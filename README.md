# Welcome to Qvet-ADMIN PROJECT

## For setup you need to ...

    - create .env name on root project and create REACT_APP_URL=https://vulcan.houseofdev.tech/qvetjob/  in .env file (for dev branch)

    - create .env.prod on root project and create REACT_APP_URL=https://qvetjobs.com/backend/ in .env file
    (for master branch)
