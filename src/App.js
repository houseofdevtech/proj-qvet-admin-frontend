import React, { Component } from 'react'
import './style/style.custom.css'

import Routes from './routes'


import Cookies from 'universal-cookie';
const cookies = new Cookies();
cookies.set('root', 'face', { path: '/',sameSite:false, secure:true});

class App extends Component {
  render() {
    return <Routes />
  }
}

export default App
