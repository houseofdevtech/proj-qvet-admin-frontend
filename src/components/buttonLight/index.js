import React, { Component } from 'react'
import styled, { css } from 'styled-components'

const StyledButton = styled.button`
  ${(props) => {
    if (props.width) {
      return css`
        width: ${props.width};
      `
    }
    return css`
      width: 100px;
    `
  }}

  background: ${(props) => props.theme.transparent};
  border: none;
  border-radius: 10px;
  border-color:transparent!important;
  outline:none;

  // :focus {
  //   background: ${(props) => props.theme.primary};
  // }
`

class ButtonLight extends Component {
  render() {
    return <StyledButton {...this.props} />
  }
}

export default ButtonLight
