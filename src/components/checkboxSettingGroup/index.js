import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

const Container = styled.div`
  margin-right: 1vw;
`

class Checkbox extends Component {
  // _isMounted = false;

  constructor(props){
    super(props);
    this.state={
      setchecked:[],
      checked: false,
    }
  }


  componentDidUpdate(){
    // this.setValueSelect()
  }
  
  // componentWillUnmount() {
  //   this._isMounted = false;
  // }

 async componentDidMount(){
    // console.log(this.props)
    this.props.checkStateCheckbox()
        this.setValueSelect()
  }

   async setValueSelect() {     
    const found = await this.props.setchecked.findIndex((o) => o === this.props.id)
    if (found > -1) await this.setState({checked: true})   
  } 

  render() {
    const { id, label, 
      // checked, 
      onChange ,
      // setchecked
    } = this.props
    // defaultChecked={checked}
    // onChange={onChange}
    return (
      <Container>
        <div className="custom-control custom-checkbox">
        <input
                type="checkbox"
                className="custom-control-input"
                checked={this.state.checked}
                id={id}
                onChange={onChange}
              />
              <label className="custom-control-label" htmlFor={id} style={{cursor: 'pointer'}}>
                {label}
              </label>
        </div>
      </Container>
    )
  }
}


const mapState = (state) => ({
    columnSettingGroup: state.settingGroup.columnSettingGroup,
    nameSettingGroup:state.settingGroup.nameSettingGroup,
  })
  
  const mapDispatch = (dispatch) => {
    return { 
      setPageTitle: dispatch.Title.setPageTitle,
      setModeSettingGroup:dispatch.settingGroup.setModeSettingGroup,
      checkStateCheckbox:dispatch.settingGroup.checkStateCheckbox
     }
  }
  
  export default connect(
    mapState,
    mapDispatch
  )(Checkbox)

// export default Checkbox
