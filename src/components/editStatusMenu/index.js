import React, { Component } from 'react'
import styled from 'styled-components'
import ErrorBoundary from '../errorBoundary'
import { post } from '../../utils/service'
import { connect } from 'react-redux'
const Container = styled.div`
  width: 150px;
  border-radius: 4px;
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.2);
  background-color: #ffffff;

  .link {
    cursor: pointer;
  }
`

const Item = styled.div`
  padding: 10px 0 10px 30px;
  display: flex;
  align-items: center;
  font-size: 14px;
  cursor: pointer;
  img {
    width: 20px;
    margin-right: 14px;
  }

  :hover {
    background-color: #f3f3f3;
  }
`
const RoleMaidClass = [
  'Top Maid',
  'Super Maid',
  'Quality Maid',
  'Normal Maid',
  'Reduced Maid',
  'Banned Maid',
]

class EditStatusMenu extends Component {
  async handleClick(text) {
    console.log(`handleClick : ${text}`)
    if (text ==='UNAPPROVED'|| text ==='APPROVED'|| text ==='BANNED'|| text ==='UNBAN') {
      console.log(`handleClick : ${this.props.IdMember}`)
      await post({status:text},`admin/maid/id/${this.props.IdMember}/update`).then(res=>{
        if(!res.error){
          this.props.satatusApprove('UpdateStatus')
        }
      })
    }
  }

  render() {
    // const { data, id, handleOpenModal } = this.props
    const { data, role } = this.props
    // console.log(`data :${JSON.stringify(this.props)}`)
    // {/* <Item onClick={() => handleOpenModal(id)}> */}
    const FilterMaidClassMenu = RoleMaidClass.filter((Allrole) => Allrole !== role)
    return (
      <ErrorBoundary>
        <Container>
          {/*  Menu for Status */}
          {/*           Approve            Ban           */}
          {/* Unapproved =====> Approved <=====> Banned  */}
          {/*                             Unban          */}

          {/* {data === 'Unapproved' && <Item>Approve</Item>} */}
          {data === 'UNAPPROVED' && <Item onClick={() => this.handleClick("APPROVED")}>APPROVED</Item>}
          {data === 'APPROVED' && <Item onClick={() => this.handleClick("BANNED")}>BANNED</Item>}
          {data === 'BANNED' && <Item onClick={() => this.handleClick("UNBAN")}>UNBAN</Item>}

          {/*  Menu for Maid Role Status */}
          {/*  Maid Class tooltip Menu is All Maid Class except their classes */}

          {role &&
            FilterMaidClassMenu.map((menu, index) => (
              <Item key={index} onClick={() => this.handleClick(menu)}>
                {menu}
              </Item>
            ))}
        </Container>
      </ErrorBoundary>
    )
  }
}



const mapState = () => ({})

const mapDispatch = (dispatch) => {
  return {
    satatusApprove:dispatch.orderManagement.satatusApprove
  }
}

export default connect(
  mapState,
  mapDispatch
)(EditStatusMenu)
