import React, { Component } from 'react'
import styled, { css } from 'styled-components'

import Chart from 'react-apexcharts'


// import ApexCharts from "apexcharts";

// const Row = styled.div`
//   display: flex;
// `

const Container = styled.div`
  display: flex;
  flex: 1;
  // background: #fff;
  // padding: 20px 10px 0px;
  // position: absolute;
  // top: 64px;
  // left: 250px;
  ${(props) => {
    if (props.width) {
      return css`
        width: ${props.width};
      `
    }
    if (props.height) {
      return css`
        height: ${props.height};
      `
    }
    return css`
      height: 100%;
      width: 100%;
    `
  }}

  @media (max-width: 768px) {
    left: 0px;
  }
`
// const Title = styled.h5`
//   align-self: center;
// `

// const GraphContainer = styled.div`
//   // background: #d3e7ee;
//   // position: relative;
//   // padding: 20px 0 0 0;
//   // width: 1fr;
// `

class Graph extends Component {
  state = {
    // option: {},
    // series:[]
  }

  componentDidMount() {
    // console.log(`this.props : ${JSON.stringify(this.props.data)}`)
    this.setState({
      options: this.props.data.options,
      series: this.props.data.series,
    })
  }

  isEmpty(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object
  }
  
  // {/* <Container width={width} height={height}> */}
  // width={gWidth}
  // height={gHeight}
  render() {
    const { 
      // title, 
      type, 
      // width, height, 
      gWidth, gHeight } = this.props
    // console.log(`this state :${JSON.stringify(this.state)}`) &&
    return (
      <Container >
          {!this.isEmpty(this.state) && (
            <Chart
                options={this.state.options}
                series={this.state.series}
                type={type}
                width={gWidth}
                height={gHeight}
                colors={this.state.colors}
              />
          )}
      </Container>
    )
  }
}

export default Graph
