import React,{useState} from 'react'
import styled from 'styled-components'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { connect } from 'react-redux'
import Divider from '../divider'
import DatePicker from 'react-datepicker'
import './graphDashboard.css'

const GraphCard =(props)=>{
        const SelectContainer =styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        `

        const Container = styled.div`
        display: flex;
        flex-direction: column;
        border-radius: 5px;
        background: #fff;
        flex: 1;
        margin-bottom: 20px;
        `

        const HeaderContainer = styled.div`
        display: flex;
        flex-direction: column;
        padding: 20px 20px 10px;
        flex: 1;

        `
        const HeaderContent = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        // justify-content: flex-start;
        `
        const GraphContainer = styled.div`
        display: block;
        padding: 0px 10px;
        // width: max-content;
        // flex: 1;

        .highcharts-container {
          position: relative;
          overflow: auto;
          width: 100%;
          height: 100%;
        }
        `
  
   const [start_datetime, setStart_datetime] = useState(props.date.start_datetime)
   const [end_datetime, setEnd_datetime] = useState(props.date.end_datetime)

    const handleChangeStartDate = (e) => {
        setStart_datetime(e)
    }
    
    const handleChangeEndDate =(e)=>{
       setEnd_datetime(e)
    }
    if(start_datetime || end_datetime){
      const data={
        start_datetime:start_datetime,
        end_datetime:end_datetime
      }
      props.setDatetime_GrapSale(data)
    }

    
  return (
    <Container>
      <HeaderContainer>
        <HeaderContent>
          <h5>{props.title}</h5>
        </HeaderContent>
        <SelectContainer>
            <div style={{paddingTop:'5px',display:'flex',paddingRight:'45px'}}>
            <h5 style={{paddingTop:'5px',float:'left',paddingRight:'15px'}}>Select Date</h5>
               <div> 
                  <DatePicker
                        showMonthDropdown
                        showYearDropdown
                        className="Datepicker-graph"
                        value={props.date.start_datetime}
                        name="start_datetime"
                        selected={props.date.start_datetime}
                        onChange={handleChangeStartDate}
                        dateFormat=" yyyy-MM-dd"
                      />
                  </div>
                   <label className="textInDatePicker">-</label>
                  <div>
                     <DatePicker 
                        showMonthDropdown
                        showYearDropdown   
                        minDate = {props.date.start_datetime}
                        className="Datepicker-graph-end"
                        value={props.date.end_datetime}
                        name="start_datetime"
                        selected={props.date.end_datetime}
                        onChange={handleChangeEndDate}
                        dateFormat=" yyyy-MM-dd"
                      />
                  </div>
            </div>
          </SelectContainer>
        <Divider />
      </HeaderContainer>
      <GraphContainer>
        <HighchartsReact highcharts={Highcharts} options={props.data} />
      </GraphContainer>
    </Container>
  )
}


const mapState = (state) => {
  return {
    dataSaleProduct:state.dashoard.dataSaleProduct,
    dateforGraph:state.dashoard.dateforGraph
  }
}

const mapDispatch = (dispatch) => {
return { 
    setPageTitle: dispatch.Title.setPageTitle,
    setDateforGraph:dispatch.dashoard.setDateforGraph,
    FaceGrapSale:dispatch.dashoard.FaceGrapSale

  }
}

export default connect(
mapState,
mapDispatch
)(GraphCard)
