
export const yearOptions = [
  { value: 2019, label: 'Year : 2019' }, 
  { value: 2020, label: 'Year : 2020' }
]

export const weekOptions = [
  { value: '2019/10/01 - 2019/10/30', label: '2019/10/01 - 2019/10/30' },
  { value: '2019/11/01 - 2019/11/30', label: '2019/11/01 - 2019/11/30'  },
  { value: '2019/12/01 - 2019/12/30', label: '2019/12/01 - 2019/12/30' },
  { value: '2020/01/01 - 2020/01/30', label: '2020/01/01 - 2020/01/30' },
  { value: '2020/02/01 - 2020/02/30', label: '2020/02/01 - 2020/02/30' },
  { value: '2020/03/01 - 2020/03/30', label: '2020/03/01 - 2020/03/30' },
  { value: '2020/04/01 - 2020/04/30', label: '2020/04/01 - 2020/04/30' },
  { value: '2020/05/01 - 2020/05/30', label: '2020/05/01 - 2020/05/30' },
  { value: '2020/06/01 - 2020/06/30', label: '2020/06/01 - 2020/06/30' },
  { value: '2020/07/01 - 2020/07/30', label: '2020/07/01 - 2020/07/30' },
  
  // { value: 'week1', label: 'Week 1: 1 Jan - 5 Jan' },
  // { value: 'week2', label: 'Week 2: 6 Jan - 12 Jan' },
  // { value: 'week3', label: 'Week 3: 13 Jan - 19 Jan' },
  // { value: 'week4', label: 'Week 4: 20 Jan - 26 Jan' },
  // { value: 'week5', label: 'Week 5: 6 Jan - 12 Jan' },
  // { value: 'week6', label: 'Week 6: 6 Jan - 12 Jan' },
  // { value: 'daily', label: 'Daily' },
  // { value: 'weekly', label: 'Weekly' },
  // { value: 'monthly', label: 'Monthly' },
  // { value: 'yearly', label: 'Yearly' },
]