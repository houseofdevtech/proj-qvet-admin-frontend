import React,{useState} from 'react'
import styled from 'styled-components'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { connect } from 'react-redux'
import Divider from '../divider'
import DatePicker from 'react-datepicker'
import './graphDashboard.css'


const SaleGarphUF =(props)=>{
        const SelectContainer =styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        `

        const Container = styled.div`
        display: flex;
        flex-direction: column;
        border-radius: 5px;
        background: #fff;
        flex: 1;
        margin-bottom: 20px;
        `

        const HeaderContainer = styled.div`
        display: flex;
        flex-direction: column;
        padding: 20px 20px 10px;
        flex: 1;

        `
        const HeaderContent = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        // justify-content: flex-start;
        `
        const GraphContainer = styled.div`
        display: block;
        padding: 0px 10px;
        // width: max-content;
        // flex: 1;

        .highcharts-container {
          position: relative;
          overflow: auto;
          width: 100%;
          height: 100%;
        }
        `
        const [Start_Date, setStartDate] = useState(props.date.start_datetimeUF)
        const [End_Date, setEndDate] = useState(props.date.end_datetimeUF)
        
        const handleChangeStartDate = (e) => {
              setStartDate(e)
        }
         
        const handleChangeEndDate =(e)=>{
             setEndDate(e)
        } 
    
        if(Start_Date || End_Date ){
               let data={
            start_datetime:Start_Date,
            end_datetime:End_Date,
          }
            props.setDatetime_BYUF(data)
    
          }

    

  return (
    <Container>
      <HeaderContainer>
        <HeaderContent>
          <h5>{props.title}</h5>
        </HeaderContent>
        <SelectContainer>
            <div style={{paddingTop:'5px',display:'flex',paddingRight:'45px'}}>
            <h5 style={{paddingTop:'5px',float:'left',paddingRight:'15px'}}>Select Date</h5>
               <div> 
                  <DatePicker
                        showMonthDropdown
                        showYearDropdown
                        className="Datepicker-graph"
                        value={props.date.start_datetimeUF}
                        name="start_datetime"
                        selected={props.date.start_datetimeUF}
                        onChange={handleChangeStartDate}
                        dateFormat=" yyyy-MM-dd"
                      />
                  </div>
                   <label className="textInDatePicker">-</label>
                  <div>
                     <DatePicker 
                        showMonthDropdown
                        showYearDropdown   
                        minDate = {props.date.start_datetimeUF}
                        className="Datepicker-graph-end"
                        value={props.date.end_datetimeUF}
                        name="start_datetime"
                        selected={props.date.end_datetimeUF}
                        onChange={handleChangeEndDate}
                        dateFormat=" yyyy-MM-dd"
                      />
                  </div>
            </div>
          </SelectContainer>
        <Divider />
      </HeaderContainer>
      <GraphContainer>
        <HighchartsReact highcharts={Highcharts} options={props.data} />
      </GraphContainer>
    </Container>
  )
}


const mapState = (state) => {
  return {
    dataSaleProduct:state.dashoard.dataSaleProduct,
    dateforGraph:state.dashoard.dateforGraph
  }
}

const mapDispatch = (dispatch) => {
return { 
    setPageTitle: dispatch.Title.setPageTitle,
    setDateforGraph:dispatch.dashoard.setDateforGraph,
    FaceGrapSale:dispatch.dashoard.FaceGrapSale

  }
}

export default connect(
mapState,
mapDispatch
)(SaleGarphUF)
