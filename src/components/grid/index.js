import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import ErrorBoundary from '../errorBoundary'

const CustomGrid = styled.div`
  // margin-top: 35px;

  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(${(props) => props.width}, 1fr));
  grid-auto-rows: ${(props) => props.height || 'auto'};
  grid-gap: ${(props) => props.gap || '30px'};

  @media (max-width: 768px) {
    margin-top: 15px;
    grid-gap: 15px;
  }
`

class Grid extends Component {
  static propTypes = {
    height: PropTypes.string,
    width: PropTypes.string.isRequired,
  }

  render() {
    const { children } = this.props
    return (
      <ErrorBoundary>
        <CustomGrid {...this.props}>{children}</CustomGrid>
      </ErrorBoundary>
    )
  }
}

export default Grid
