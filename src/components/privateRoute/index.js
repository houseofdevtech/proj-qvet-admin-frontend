import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { ACCESS_TOKEN } from '../../constants'
// import PropTypes from 'prop-types'
// import ErrorBoundary from '../errorBoundary'
function PrivateRoute({ component: Component, ...rest }) {
  const isAuthenticated = localStorage.getItem(ACCESS_TOKEN)
  // console.log(`isAuthenticated : ${isAuthenticated}`)
  // console.log(`process.env.PUBLIC_URL : ${process.env.PUBLIC_URL}`)
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/user/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  )
}
export default PrivateRoute
