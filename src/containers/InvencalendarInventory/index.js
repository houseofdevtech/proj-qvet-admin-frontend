import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
// import Select from 'react-select'
// import Divider from '../divider'
// import Button from '../buttonLight'
import request from '../../utils/request'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'

// import { yearOptions, weekOptions } from '../graphCard/options'

// import { cohortExample } from './example'

// const SelectContainer =styled.div`
//   display: flex;
//   flex-direction: column;
//   align-items: flex-end;
// `

const Container = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 5px;
  background: #fff;
  flex: 1;
  margin-bottom: 20px;
`

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px 20px 10px;
  flex: 1;
`
const HeaderContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const GraphContainer = styled.div`
  display: block;
  padding: 0px 10px;
  // width: max-content;
  // flex: 1;

  .highcharts-container {
    position: relative;
    overflow: auto;
    width: 100%;
    height: 100%;
  }
`

// React-Select Style Section 
// const customStylesYear = {
//   container: (styles) => ({
//     ...styles,
//     width: 250,
//   }),
// }

// const customStylesWeek = {
//   container: (styles) => ({
//     ...styles,
//     width: 250,
//     marginTop: "5px",
//   }),
// }

class Carlendar extends Component {
  state = {
    year: null,
    week: null,
    data:[]
    // selectedOption: null,
  }

  componentDidMount(){
    this.props.FaceserRetention()
    this.getItemFromTable()
  }
  getItemFromTable(){
    var d = new Date();
    var m = d.getMonth();
    var y = d.getFullYear();
    console.log(m+1)
    request.get(`/maid/calendar/inventory/month/${m+1}/year/${y}`).then(res=>{
      console.log(res)
       
    })
  }

  handleChange = (selectedOption, key) => {
    this.setState({ [key] : selectedOption })
  }

  

  render() {
    const {
      title,
      // data,
      // type , width ,height, gWidth, gHeight
    } = this.props

    const allColumns = [
      {
        Header: () => <h6>{''}</h6>,
        accessor: 'date',
        minWidth: 80,
      },
      {
        Header: () => <h6>6.00</h6>,
        accessor: 'col1',
        minWidth: 80,
      },
      {
        Header: () => <h6>6.30</h6>,
        accessor: 'col2',
        minWidth: 80,
      },
      {
        Header: () => <h6>7.00</h6>,
        accessor: 'col3',
        minWidth: 80,
      },
      {
        Header: () => <h6>7.30</h6>,
        accessor: 'col4',
        minWidth: 80,
      },
      {
        Header: () => <h6>8.00</h6>,
        accessor: 'col5',
        minWidth: 80,
      },
      {
        Header: () => <h6>8.30</h6>,
        accessor: 'col6',
        minWidth: 80,
      },
      {
        Header: () => <h6>9.00</h6>,
        accessor: 'col7',
        minWidth: 80,
      },
      {
        Header: () => <h6>9.30</h6>,
        accessor: 'col8',
        minWidth: 80,
      },
      {
        Header: () => <h6>10.00</h6>,
        accessor: 'col9',
        minWidth: 80,
      },
      {
        Header: () => <h6>10.30</h6>,
        accessor: 'col10',
        minWidth: 80,
      },
      {
        Header: () => <h6>11.00</h6>,
        accessor: 'col11',
        minWidth: 80,
      },
      {
        Header: () => <h6>11.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>12.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>12.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>13.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>13.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>14.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>14.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>15.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>15.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>16.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>16.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>17.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>17.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>18.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>18.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>19.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>19.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>20.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>20.30</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
      {
        Header: () => <h6>21.00</h6>,
        accessor: 'col12',
        minWidth: 80,
      },
    ]
    return (
      <Container>
        <HeaderContainer>
          <HeaderContent>
            <h5>{title}</h5>
          </HeaderContent>
          {/* <Divider /> */}
        </HeaderContainer>
        <GraphContainer>
          <ReactTable
            data={this.state.data}
            columns={allColumns}
            defaultPageSize={31}
            showPagination={false}
            className=""
          />
        </GraphContainer>
      </Container>
    )
  }
}

const mapState = (state) => {
  return {
    // dataRetentionList:state.dashoard.dataRetentionList,
  }
}

const mapDispatch = (dispatch) => {
return { 
  FaceserRetention: dispatch.dashoard.FaceserRetention

  }
}

export default connect(
mapState,
mapDispatch
)(Carlendar)
