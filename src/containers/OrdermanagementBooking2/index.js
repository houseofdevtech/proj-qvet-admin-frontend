/* eslint-disable no-labels */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import _ from 'lodash'
import { CSVLink, CSVDownload } from 'react-csv'
import moment from 'moment'
import { get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import EditOrderBooking from '../../components/editOrderBooking/index'
import Tooltip from '../../components/tooltip'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`
const TextCollum = styled.div`
  margin-bottom: 0.5rem;
  font-weight: 500;
  line-height: 1.2;
  color: inherit;
`
const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
`
const Center = styled.div`
  display: flex;
  justify-content: center;
`
class OrderManagementBooking2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      column: this.props.columnOrderManagementBooking,
      data: [],
      firstMonday: null,
      partTimeUser: [],
      start_dateOrder: '',
      end_dateOrder: '',
      start_dateOrder: moment(new Date()).day(-7).weekday(1).toDate(),
      end_dateOrder: moment(new Date()).day(-7).weekday(7).toDate(),
      paramsDate: {},
      loading: false,
      PageSize: 10,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  async componentDidMount() {
    this.props.setPageTitle('Payment')
    this.props.setOrderMode('Parttime')
    this.getDealJobList()
    // this.setDateForFectData()
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('date:', this.props.start_dateOrder)
    if (
      this.props.start_dateOrder !== undefined &&
      prevProps.start_dateOrder !== this.props.start_dateOrder
    ) {
      if (this.props.start_dateOrder !== '') {
        // this.setState({ start_dateOrder: this.props.start_dateOrder })
        this.setState({
          start_dateOrder: this.props.start_dateOrder,
          end_dateOrder: this.props.end_dateOrder,
        })
        console.log('data', this.state.partTimeUser)
        this.getDealJobList()
      }
    }
  }

  swMoneyPet = (level) => {
    switch (level) {
      case 1:
        return 1500
        break
      case 2:
        return 1500
        break
      case 3:
        return 1500
        break
      case 4:
        return 1500
        break
      case 5:
        return 550
        break
      case 6:
        return 850
        break
      default:
        break
    }
  }
  swMoney = (level) => {
    switch (level) {
      case 1:
        return 1600
        break
      case 2:
        return 1700
        break
      case 3:
        return 2000
        break
      case 4:
        return 3000
        break
      case 5:
        return 550
        break
      case 6:
        return 850
        break
      default:
        break
    }
  }

  getDeaJobPartTime = (jobPartTime) => {
    let temp = []
    jobPartTime.map((m) => {
      temp.push(m.period_job.date_start)
      temp.push(m.period_job.date_end)
    })
    return temp.join(',')
  }

  currencyFormat = (money) => {
    return Intl.NumberFormat('th-TH', { currency: 'THB' }).format(money)
  }

  sumDay = (period) => {
    let start = moment(period.date_start)
    let end = moment(period.date_end)
    console.log('uuu', period)
    // let result = period.reduce((acc, curr) => acc + curr.amount, 0)
    return end.diff(start, `days`) + 1
  }
  getDealJobList = async () => {
    const res = await get(`/api/donejob/all?page=1&limit=20`).then((res) => res)

    console.log('resx', res)
    let partTimeUser = []
    let exportCSV = []
    let EmMoney = 0
    let sumd

    // res.part_time.deal_job_part_times.map((u) => {
    //   let user = _.filter(res.part_time, { user_id: u.user_id })
    //   console.log('resxxx', user)
    // })

    // console.log(_.filter(res.part_time, (u) => {
    //   return u.deal_job_part_times.job.company.company_name ===
    // }))
    console.log('summ', sumd)
    let order = 1
    res.part_time.map((user, index) => {
      // const result = _.filter(user.deal_job_part_times, (e) => e.job.company.company_name)
      const result = _(user.deal_job_part_times)
        .groupBy('job.company.company_name')
        .map((v, i) => ({
          company_name: i,
          job: v,
        }))
        .value()

      // result.map((m) => {
      //   sumd = m.deal_job_part_times.reduce((acc, curr) => {
      //     let start = moment(curr.period_job.date_start)
      //     let end = moment(curr.period_job.date_end)
      //     return (acc += start.diff(end, 'days') + 1)
      //   }, 0)
      // })
      let userUniq = _.uniqBy(user.deal_job_part_times, 'job.company.company_name')
      let sum = []
      let score = []
      const resultScore = _(user.user_assessments)
        .groupBy('company_id')
        .map((v, i) => {
          // console.log('company info: ', v, i)
          return {
            company_id: i,
            score: v,
          }
        })
        .value()

      resultScore.map((m) => {
        score.push(
          m.score.map((x) => {
            return x.score
          })
        )
        // test note
        // note.push(
        //   m.score.map((x) => {
        //     return x.note
        //   })
        // )
      })
      result.map((m) => {
        sum.push(
          m.job.reduce((acc, curr) => {
            return (acc += curr.done_job_part_time.total_price)
          }, 0)
        )
      })
      // console.log('rexxx3', resultScore)
      // console.log('rexxx2', result)
      // console.log('unique user : ', userUniq);
      if (user !== null) {
        userUniq.map((m, i, origin) => {
          let ind_time_start = m.period_job.date_start
          let ind_time_end = m.period_job.date_end
          let timeCnodition =
            moment(ind_time_start).isSameOrAfter(moment(this.state.start_dateOrder)) &&
            moment(ind_time_end).isSameOrBefore(moment(this.state.end_dateOrder))
          if (timeCnodition) {
            console.log('users: ', user)
            partTimeUser.push({
              Order_ID: order++,
              name: user.name,
              position: user.position,
              level: m.job.level,
              EmployerName: m.job.company.company_name,
              pricing_person:
                this.currencyFormat(
                  m.job.company.position === 'Pet shop'
                    ? this.swMoneyPet(m.job.level)
                    : this.swMoney(m.job.level)
                ) + ' บาท',
              note: m.job.add_on_extra?.note,
              JobType: m.job.company.position,
              WorkTime: m.period_job.date_start + ',' + m.period_job.date_end,
              amountDay:
                m.job.company.position === 'Pet shop'
                  ? Math.floor(sum[i] / this.swMoneyPet(m.job.level))
                  : Math.floor(sum[i] / this.swMoney(m.job.level)),
              score: score[i].join(',\r\n'),
              moneyPerson: Intl.NumberFormat('th-TH', { currency: 'THB' }).format(sum[i]) + ' บาท',
              paymentStatus: m.job.payment_type,
            })
          }
          // exportCSV.push({
          //   ลำดับ: index + 1,
          //   ชื่อพนักงาน: user.name,
          //   ตำแหน่ง: user.position,
          //   level: m.job.level,
          //   อัตราจ้าง:
          //     m.job.company.position === 'Pet shop'
          //       ? this.swMoneyPet(m.job.level)
          //       : this.swMoney(m.job.level),
          //   ประเภทงาน: m.job.type,
          //   วันที่ปฏิบัติงาน: m.period_job.date_start + ',' + m.period_job.date_end,
          //   จำนวนวัน: m.period_job.amount + ' วัน',
          //   จำนวนเงิน: m.done_job_part_time.total_price + ' ' + 'บาท',
          //   สถานะการชำระเงิน:
          //     m.job.payment_type === 'success' ? 'ชำระเงินแล้ว' : 'ยังไม่ได้ชำระเงิน',
          // })
          // EmMoney += m.done_job_part_time.total_price
        })
        user.deal_job_part_times.map((m, i, origin) => {
          // partTimeUser.push({
          //   Order_ID: index + 1,
          //   name: user.name,
          //   position: user.position,
          //   level: m.job.level,
          //   EmployerName: m.job.company.company_name,
          //   pricing_person:
          //     m.job.company.position === 'Pet shop'
          //       ? this.swMoneyPet(m.job.level)
          //       : this.swMoney(m.job.level),
          //   JobType: m.job.company.position,
          //   WorkTime: m.period_job.date_start + ',' + m.period_job.date_end,
          //   amountDay: m.period_job.amount,
          //   moneyPerson: m.done_job_part_time.total_price,
          //   paymentStatus: m.job.payment_type,
          // })
          exportCSV.push({
            ลำดับ: i + 1,
            ชื่อพนักงาน: user.name,
            ตำแหน่ง: user.position,
            ชื่อผู้ประกอบการ: m.job.company.company_name,
            level: m.job.level,
            อัตราจ้าง:
              this.currencyFormat(
                m.job.company.position === 'Pet shop'
                  ? this.swMoneyPet(m.job.level)
                  : this.swMoney(m.job.level)
              ) + ' บาท',
            ประเภทงาน: m.job.company.position,
            วันที่ปฏิบัติงาน: m.period_job.date_start + ',' + m.period_job.date_end,
            จำนวนวัน: m.period_job.amount + ' วัน',
            จำนวนเงิน:
              Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                m.done_job_part_time.total_price
              ) +
              ' ' +
              'บาท',
            คะแนนประเมิน: user.user_assessments[i].score,
            สถานะการชำระเงิน:
              m.job.payment_type === 'success' ? 'ชำระเงินแล้ว' : 'ยังไม่ได้ชำระเงิน',
            หมายเหตุ: m.job.add_on_extra?.note || '-',
          })
          EmMoney += m.done_job_part_time.total_price
        })
      }
    })

    const moneyV = {
      EmMoney,
    }
    this.props.setEmployee(exportCSV)
    this.props.setEmployerTotal(moneyV)
    this.setState({ partTimeUser })
  }

  render() {
    const allColumns = [
      {
        Header: () => <TextCollum>ลำดับ </TextCollum>,
        accessor: 'Order_ID',
      },
      {
        Header: () => <TextCollum>ชื่อพนักงาน</TextCollum>,
        accessor: 'name',
      },
      {
        Header: () => <TextCollum>ตำแหน่ง</TextCollum>,
        accessor: 'position',
      },
      {
        Header: () => <TextCollum>Level</TextCollum>,
        accessor: 'level',
      },
      {
        Header: () => <TextCollum>อัตราจ้าง</TextCollum>,
        accessor: 'pricing_person',
      },
      {
        Header: () => <TextCollum>ชื่อผู้ประกอบการ</TextCollum>,
        accessor: 'EmployerName',
      },
      {
        Header: () => <TextCollum>ประเภทงาน</TextCollum>,
        accessor: 'JobType',
      },
      {
        Header: () => <TextCollum>วันที่ปฏิบัติงาน</TextCollum>,
        accessor: 'WorkTime',
        Cell: (row) => <Div>{row.original.WorkTime}</Div>,
      },
      {
        Header: () => <TextCollum>จำนวนวัน</TextCollum>,
        accessor: 'amountDay',
        Cell: (row) => <Center>{row.original.amountDay} วัน</Center>,
      },
      {
        Header: () => <TextCollum>จำนวนเงิน</TextCollum>,
        accessor: 'moneyPerson',
      },
      {
        Header: () => <TextCollum>คะแนนประเมิน</TextCollum>,
        accessor: 'score',
      },
      {
        Header: () => <TextCollum>หมายเหตุ</TextCollum>,
        accessor: 'note',
        Cell: (row) => <Center>{row.original.note ?? '-'}</Center>,
      },
      {
        Header: () => <TextCollum>สถานะรายการ</TextCollum>,
        accessor: 'listConfirm',
        Cell: (row) => (
          <Center>{row.original.listConfirm ? 'ยืนยันรายการ' : 'ยังไม่ยืนยันรายการ'}</Center>
        ),
      },
    ]

    let columnWillShow = []
    console.log('oo', this.state.partTimeUser)
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })

    return (
      <div>
        <Card>
          {this.state.partTimeUser.length >= 0 ? (
            <div style={{ width: '100%' }}>
              <ReactTable
                data={this.state.partTimeUser} //data object
                columns={columnWillShow} //column config object
                loading={false}
                //  defaultPageSize={this.state.PageSize}
                // pageSize={5}
                showPagination={true}
                pageSize={this.state.PageSize}
                // getTrGroupProps={(state, rowInfo) => {
                //   if (rowInfo !== undefined) {
                //     return {
                //       onDoubleClick: () => {
                //         if(rowInfo.original.Type_service === 'MULTI_PACKAGE'){
                //            this.props.history.replace({
                //           pathname: '/admin/order-management/orderBookingProfile',
                //           state: rowInfo.original,
                //         })
                //         }
                //
                //       },
                //     }
                //   }
                // }}
                // style={{
                //   height: '400px',
                // }}
                className="-striped -highlight"
              />
            </div>
          ) : (
            //
            <ReactTable
              data={this.state.partTimeUser}
              columns={columnWillShow}
              defaultPageSize={this.state.pageSize}
              showPagination={false}
              className=""
            />
          )}
        </Card>
        {this.props.children}
      </div>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  columnOrderManagementBooking: state.orderManagement.columnOrderManagementBooking,
  start_dateOrder: state.orderManagement.start_dateOrder,
  end_dateOrder: state.orderManagement.end_dateOrder,
  orderMode: state.orderManagement.orderMode,
})

const mapDispatch = (dispatch) => {
  return {
    setEmployee: dispatch.orderManagement.setEmployee,
    setEmployerTotal: dispatch.orderManagement.setEmployerTotal,
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    getOrderBookList: dispatch.orderManagement.getOrderBookList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(OrderManagementBooking2)
