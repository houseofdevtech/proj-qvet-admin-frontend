import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import _ from 'lodash'
import { CSVLink, CSVDownload } from 'react-csv'
import moment from 'moment'
import { get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { extend } from 'highcharts'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

const TextCollum = styled.div`
  margin-bottom: 0.5rem;
  font-weight: 500;
  line-height: 1.2;
  color: inherit;
`
const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
`
const Center = styled.div`
  display: flex;
  justify-content: center;
`

class ReceiptProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      column: [],
      company_name : this.props.match.params.id,
      partTimeUser:[],
    }
  }
  async componentDidMount() {
    this.props.setPageTitle(this.state.company_name)
    this.props.setOrderMode('Profile')
    // await this.setDateForFectData()
    this.getDealJobList()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.columnOrderManagementApprove) {
      this.setState({ column: this.props.columnOrderManagementApprove })
    }
  }

  currencyFormat = (money) => {
    return Intl.NumberFormat('th-TH', { currency: 'THB' }).format(money)
  }
  getDealJobList = async () => {
    const res = await get(`/api/dealjob/all?page=1&limit=5`).then((res) => res)
    // this.setState({partTimeUser: res.deal_job_part_time})
    // const x = _.union(res.deal_job, res.deal_job_part_time)
    console.log('resxx', res)
    let partTimeUser = []
    let EmployerData = []
    let money = 0
    let moneyVat = 0
    let moneyVatTotal = 0
    let moneyVx = 0
    // console.log('xxxb', x)
    let index = 0
    res.done_job.map((user) => {
      if (user !== null && user.company_name === this.state.company_name) {
        user.jobs.map((job) => {
          partTimeUser.push({
            Order_ID: index + 1,
            position: user.position,
            level: job.level,
            pricing_person: job.pricing_job.pricing_vat - job.pricing_job.pricing + ' บาท',
            JobType: job.type,
            WorkTime: job.date_time_job.date_start + ',\n' + job.date_time_job.date_end + ',\n',
            amountDay: job.amount,
            moneyPerson: job.pricing_job.pricing,
            EmployerName: user.company_name,
            moneyVat: job.pricing_job.pricing_vat,
          })
          EmployerData.push({
            ลำดับ: index + 1,
            ชื่อผู้ประกอบการ: user.company_name,
            จำนวนวัน: job.amount + ' วัน',
            จำนวนเงิน: this.currencyFormat(job.pricing_job.pricing) + ' บาท',
            วันที่ปฏิบัติงาน:
              job.date_time_job.date_start + ',\n' + job.date_time_job.date_end + ',\n',
            ภาษีมูลค่าเพิ่ม:
              this.currencyFormat(job.pricing_job.pricing_vat - job.pricing_job.pricing) + ' บาท',
            จำนวนเงินรวมภาษีมูลค่าเพิ่ม: this.currencyFormat(job.pricing_job.pricing_vat) + ' บาท',
          })
          money += job.pricing_job.pricing
          moneyVat += job.pricing_job.pricing_vat
          moneyVx += job.pricing_job.pricing_vat - job.pricing_job.pricing
          index++
        })
      }
    })
    console.log("resu", partTimeUser);
    this.setState({
        partTimeUser: partTimeUser
    })
  }

  render() {
    const allColumns = [
      {
        Header: () => <TextCollum>ลำดับ </TextCollum>,
        accessor: 'Order_ID',
        style: { fontSize: '13px' },
      },

      {
        Header: () => <TextCollum>เลขที่อ้างอิง</TextCollum>,
        accessor: 'EmployerName',
        style: { fontSize: '13px' },
      },
      {
        Header: () => <TextCollum>ชื่อพนักงาน</TextCollum>,
        accessor: 'WorkTime',
        style: { fontSize: '13px' },
        Cell: (row) => <Div></Div>,
      },
      {
        Header: () => <TextCollum>ตำแหน่ง</TextCollum>,
        accessor: 'amountDay',
        style: { fontSize: '13px' },
        Cell: (row) => <Center> วัน</Center>,
      },
      {
        Header: () => <TextCollum>อัตราจ้าง</TextCollum>,
        accessor: 'moneyPerson',
        style: { fontSize: '13px' },
        Cell: (row) => {
          return <Center>บาท</Center>
        },
      },

      {
        Header: () => <TextCollum>รหัสงาน</TextCollum>,
        accessor: 'moneyVat',
        style: { fontSize: '13px' },
        Cell: (row) => {
          return <Center>บาท</Center>
        },
      },
      {
        Header: () => <TextCollum>วันที่ปฏิบัติงาน</TextCollum>,
        accessor: 'moneyVatTotal',
        style: { 'white-space': 'pre', fontSize: '13px' },
        // maxWidth: 150,
        Cell: (row) => {
          let total = row.original.moneyVat
        //   this.props.setCompanyMoneyVatTotal(total)
          return <Center>บาท</Center>
        },
      },
      {
        Header: () => <TextCollum>จำนวนวัน</TextCollum>,
        accessor: 'moneyVatTotal',
        style: { fontSize: '13px' },
        Cell: (row) => {
          let total = row.original.moneyVat
        //   this.props.setCompanyMoneyVatTotal(total)
          return <Center>บาท</Center>
        },
      },
      {
        Header: () => <TextCollum>จำนวนเงิน</TextCollum>,
        accessor: 'moneyVatTotal',
        style: { fontSize: '13px' },
        Cell: (row) => {
          let total = row.original.moneyVat
        //   this.props.setCompanyMoneyVatTotal(total)
          return <Center>บาท</Center>
        },
      },
    ]
    let columnWillShow = []
    for (var obj of allColumns) {
      columnWillShow.push(obj)
    }
    // this.state.column.map((colName) => {
    //   for (var obj of allColumns) {
    //     colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
    //   }
    //   return {}
    // })
    return (
      <>
        <Card>
          <div onScroll={this.onScroll} style={{ width: '100%' }}>
            <ReactTable
              data={this.state.partTimeUser}
              columns={columnWillShow}
              loading={false}
              pageSize={5}
              showPagination={true}
            //   style={{ height: '400px' }}
              className="-striped -highlight"
            />
          </div>
        </Card>
      </>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  columnOrderManagementBooking: state.orderManagement.columnOrderManagementBooking,
  start_dateOrder: state.orderManagement.start_dateOrder,
  end_dateOrder: state.orderManagement.end_dateOrder,
  orderMode: state.orderManagement.orderMode,
})

const mapDispatch = (dispatch) => {
  return {
    setEmployee: dispatch.orderManagement.setEmployee,
    setEmployerTotal: dispatch.orderManagement.setEmployerTotal,
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    getOrderBookList: dispatch.orderManagement.getOrderBookList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(ReceiptProfile)
