import React from 'react'
import { connect } from 'react-redux'
import { ViewState, IntegratedEditing, EditingState } from '@devexpress/dx-react-scheduler'
import Button from '@material-ui/core/Button'
import Chip from '@material-ui/core/Chip'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import Collapse from '@material-ui/core/Collapse'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Box from '@material-ui/core/Box'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ListSubheader from '@material-ui/core/ListSubheader'
import { withStyles } from '@material-ui/core/styles'
import { get } from '../../utils/service'
import moment from 'moment'
import './scheduler.scss'
import styled from 'styled-components'
import _, { rest } from 'lodash'

import {
  Scheduler,
  MonthView,
  Appointments,
  WeekView,
  Toolbar,
  ViewSwitcher,
  AppointmentTooltip,
  DateNavigator,
  TodayButton,
  AppointmentForm,
  DayView,
} from '@devexpress/dx-react-scheduler-material-ui'
import { job_dashboard } from './mock-data'

const Card = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  /* flex: 1; */
  border-radius: 5px;
  border: 1px solid #f8eff6;
  background: linear-gradient(to bottom, #b585a5, #8a296d);
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`
const ListContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
`
const SPAN = styled.span`
  display: flex;
  justify-content: center;
`
const listStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(3),
    border: '1px solid #eaeaea',
  },
  subheader: {
    paddingLeft: 0,
  },
}))

const listItemStyle = makeStyles((theme) => ({
  root: {
    border: '1px solid #eaeaea',
    borderRadius: '0.2em',
    margin: '0.25em 0',
  },
}))

const P1 = styled.p`
  color: #cbcbcb !important;
  font-size: 30px;
`
const P2 = styled.p`
  color: #cbcbcb !important;
  font-size: 20px;
`
const P3 = styled.p`
  color: #cbcbcb !important;
  font-size: 18px;
`
const style = ({ palette }) => ({
  icon: {
    color: palette.action.active,
  },
  textCenter: {
    textAlign: 'center',
  },
  header: {
    height: '260px',
    backgroundSize: 'cover',
  },
  commandButton: {
    backgroundColor: 'rgba(255,255,255,0.65)',
  },
})

// style
const Appointment = ({ children, style, isShaded, ...restProps }) => {
  // console.log('propss', restProps)
  let bg_color = ''
  switch (restProps.data.title) {
    case 'คิลนิค':
      bg_color = 'transparent'
      break
    case 'พนักงานที่ปฏิบัติงาน':
      bg_color = 'transparent'
      break
    default:
      bg_color = 'transparent'
      break
  }
  // console.log('ppx1', restProps.data.data)
  let uniqData = _.uniqBy(restProps.data.data, function (e) {
    return e.user.company_id
  })
  // console.log('ppx2', uniqData)
  return (
    <Appointments.Appointment
      {...restProps}
      style={{
        ...style,
        backgroundColor: bg_color,
        color: '#282c34',
        verticalAlign: 'center',
        borderRadius: '0px',
        border: 0,
        textAlign: 'left',
      }}>
      {/* {children} */}
      <div
        className={
          restProps.data.title === 'ผู้ประกอบการ'
            ? 'font-base-line-purple'
            : restProps.data.title === 'ต้องการพนักงานเพิ่ม'
            ? 'font-base-line-orange'
            : 'font-base-line-green'
        }>
        <div className="app-title">
          {restProps.data.title} <span className="ppl-amount"> x{uniqData.length}</span>
        </div>
      </div>
    </Appointments.Appointment>
  )
}

const Content = withStyles(style, { name: 'Content' })(
  ({ children, appointmentData, ...restProps }) => {
    // console.log('appo', appointmentData)
    let is_clinic = appointmentData.title === 'ผู้ประกอบการ'
    const classes = listStyles()
    const list_classes = listItemStyle()
    const [selectedIndex, setSelectedIndex] = React.useState('')

    const handleClick = (index) => {
      if (selectedIndex === index) {
        setSelectedIndex('')
      } else {
        setSelectedIndex(index)
      }
    }
    // let appointmentDataUniq = _.uniqBy(appointmentData.data, function (e) {
    //   return e.user.company_id
    // })
    let appointmentDataUniq = appointmentData.data
    console.log('ppx', appointmentDataUniq)
    return (
      // <AppointmentTooltip.Content {...restProps} appointmentData={appointmentData}>
      <>
        {appointmentData.title === 'ต้องการพนักงานเพิ่ม' ? (
          <div className={classes.content} style={{ height: '30em' }}>
            <Container className="appointment-header">
              <p>{moment(appointmentData.startDate).format('DD MMMM YYYY')}</p>
            </Container>
            <Container>
              <List
                component="nav"
                aria-labelledby="nested-list-subheader"
                subheader={
                  <ListSubheader
                    component="div"
                    id="nested-list-subheader"
                    className={classes.subheader}>
                    {'ต้องการพนักงานเพิ่ม'}
                  </ListSubheader>
                }
                className={classes.root}>
                <>
                  {appointmentDataUniq.map((appointment, index) => (
                    <>
                      <ListItem
                        button
                        onClick={() => handleClick(index)}
                        className={list_classes.root}
                        key={index}>
                        <ListItemText primary={appointment.user.name} />
                        {index === selectedIndex ? <ExpandLess /> : <ExpandMore />}
                      </ListItem>
                      <Collapse in={index === selectedIndex} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                          <ListItem button className={classes.nested}>
                            {appointment.type === 'FullTime' ? (
                              <>
                                {appointment.user.position === 'ผู้ช่วยสัตวแพทย์' ||
                                appointment.user.position === 'พนักงานตัดขน' ? (
                                  <ListItemText
                                    primary={`ตำแหน่งงาน${appointment.user.position} (Full time)`}
                                  />
                                ) : (
                                  <ListItemText
                                    primary={`ตำแหน่งงาน${appointment.user.position}`}
                                    secondary={`Level ${appointment.user.level} (Full time)`}
                                  />
                                )}
                              </>
                            ) : (
                              <ListItemText
                                primary={`ตำแหน่งงาน${appointment.user.position} Level ${appointment.user.level}`}
                                secondary={`${appointment.time_start} - ${appointment.time_end} (Part time)`}
                              />
                            )}
                          </ListItem>
                        </List>
                      </Collapse>
                    </>
                  ))}
                </>
              </List>
            </Container>
          </div>
        ) : (
          <>
            <div className={classes.content} style={{ height: '30em' }}>
              <Container className="appointment-header">
                <p>{moment(appointmentData.startDate).format('DD MMMM YYYY')}</p>
              </Container>

              <Container>
                <List
                  component="nav"
                  aria-labelledby="nested-list-subheader"
                  subheader={
                    <ListSubheader
                      component="div"
                      id="nested-list-subheader"
                      className={classes.subheader}>
                      {is_clinic
                        ? 'ผู้ประกอบการที่ประกาศงาน'
                        : appointmentData.title === 'ต้องการพนักงานเพิ่ม'
                        ? 'ต้องการพนักงานเพิ่ม'
                        : 'พนักงานที่ปฏิบัติงาน'}
                    </ListSubheader>
                  }
                  className={classes.root}>
                  <>
                    {appointmentDataUniq.map((appointment, index) => (
                      <>
                        <ListItem
                          button
                          onClick={() => handleClick(index)}
                          className={list_classes.root}
                          key={index}>
                          {is_clinic ? (
                            <ListItemText primary={appointment.company} />
                          ) : (
                            <ListItemText
                              primary={appointment.user.title + ' ' + appointment.user.name}
                            />
                          )}
                          {index === selectedIndex ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={index === selectedIndex} timeout="auto" unmountOnExit>
                          <List component="div" disablePadding>
                            {is_clinic ? (
                              // appointment.user?.jobs?.map((m) => (
                              // <ListItemText>
                              <ListItem button className={classes.nested}>
                                <Box flexDirection="row">
                                  <ListItemText
                                    xs={12}
                                    primary={`${appointment.user.position} Level ${appointment.user.level}`}
                                  />
                                  {appointment.user.date_time_job ? (
                                    <ListItemText
                                      xs={12}
                                      secondary={`${appointment.user.date_time_job?.time_end} - ${appointment.user.date_time_job?.time_end} (${appointment.type})`}
                                    />
                                  ) : (
                                    appointment.user.period_jobs?.map((d) => (
                                      <ListItemText
                                        xs={12}
                                        secondary={`${d?.time_start} - ${d?.time_end} (${
                                          appointment.type === 'PartTime'
                                            ? 'Part time'
                                            : 'Full time'
                                        })`}
                                      />
                                    ))
                                  )}
                                </Box>
                              </ListItem>
                            ) : (
                              // </ListItemText>
                              // ))
                              <ListItem button className={classes.nested}>
                                <ListItemText
                                  primary={`ตำแหน่งงาน${appointment.user.position} `}
                                  secondary={`Level ${appointment.user.level}
                                  ${appointment.user.time_start} - ${appointment.user.time_end}`}
                                />
                              </ListItem>
                            )}
                            <ListItem button className={classes.nested}>
                              <ListItemText
                                primary={
                                  is_clinic ? (
                                    <span>{appointment.name}</span>
                                  ) : (
                                    <span>{appointment.user.company_name}</span>
                                  )
                                }
                              />
                            </ListItem>
                          </List>
                        </Collapse>
                      </>
                    ))}
                  </>
                </List>
              </Container>
            </div>
          </>
        )}
      </>
      // </AppointmentTooltip.Content>
    )
  }
)

const ExternalViewSwitcher = ({ currentViewName, onChange }) => {
  //   console.log(`External view : ${onChange}`);
  return (
    <ButtonGroup
      variant="text"
      aria-label="text primary button group"
      value={currentViewName}
      onClick={onChange}
      id="ExternalViewSwitcher">
      <Button className={currentViewName === 'Day' ? 'active' : 'inactive'} value="Day">
        Day
      </Button>
      <Button className={currentViewName === 'Week' ? 'active' : 'inactive'} value="Week">
        Week
      </Button>
      <Button className={currentViewName === 'Month' ? 'active' : 'inactive'} value="Month">
        Month
      </Button>
    </ButtonGroup>
  )
}

const BasicLayout = ({ onFieldChange, appointmentData, ...restProps }) => {
  const onCustomFieldChange = (nextValue) => {
    onFieldChange({ customField: nextValue })
  }

  return (
    <AppointmentForm.BasicLayout
      appointmentData={appointmentData}
      onFieldChange={onFieldChange}
      {...restProps}>
      <AppointmentForm.Label text="Custom Field" type="title" />
    </AppointmentForm.BasicLayout>
  )
}

class AnalysisPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
      currentViewName: 'Month',
      currentDate: moment().format('YYYY-MM-DD'),
      isShiftPressed: false,
      open: false,
      active_emp_amount: 0,
      active_announcement_amount: 0,
      active_company_amount: 0,
      more_emp_amount: 0,
      active_emp: [],
      active_company: [],
    }

    this.currentViewNameChange = (e) => {
      console.log(e.target.innerHTML)
      this.setState({ currentViewName: e.target.innerHTML })
    }
    this.currentDateChange = async (currentDate) => {
      this.setState({ currentDate })
      this.countFucnction()
      //this.getAllData()    
    }
  }

  clearState = () => {
    const initState = {
      data: [],
      currentViewName: 'Month',
      isShiftPressed: false,
      open: false,
      active_emp_amount: 0,
      active_announcement_amount: 0,
      active_company_amount: 0,
      more_emp_amount: 0,
      active_emp: [],
      active_company: [],
    }
    this.setState({ ...initState });
  };

  countFucnction = async () =>{
    const emp = await get(`/api/employee_analysis`).then((res) => res)
    const announce = await get(`/api/announce_analysis`).then((res) => res)
    // const manhour = await get(`/api/employee_analysis`).then((res) => res)
    // console.log('emp:', emp)
    // console.log('announce', announce)
    let active_emp = []
    let active_emp_amount = 0 //แสดงพนักงานที่ปฏิบัติงาน
    // คำนวณพนักงานที่ปฏิบัติงาน
    let more_emp = []
    emp.full_time.map((emp) => {
      emp.jobs.map((job) => {
        let amount_all = job.annoucement.amount_all
        job.deal_jobs.map((del_job) => {
          if ((del_job.status === 'confirm' || del_job.status === 'complete')
          &&
          moment(this.state.currentDate).format("YYYY/MM") === moment(job.date_time_job.date_start).format("YYYY/MM")
          ) {
            active_emp_amount++
            amount_all--
            active_emp.push({
              user: {
                title: del_job.user.title,
                name: del_job.user.name,
                company_id: job.company_id,
                position: del_job.user.position,
                time_start: job.date_time_job.time_start,
                time_end: job.date_time_job.time_end,
                level: job.level,
                company_name: emp.company_name,
              },
              type: 'FullTime',
              startDate: job.date_time_job.date_start,
              endDate: job.date_time_job.date_end,
              title: 'พนักงานที่ปฏิบัติงาน',
            })
          }
        })
        if (amount_all > 0) {
          more_emp.push({
            user: {
              name: emp.company_name,
              company_id: job.company_id,
              position: job.position,
              level: job.level,
            },
            type: 'FullTime',
            amount: amount_all,
            startDate: job.date_time_job.date_start,
            endDate: job.date_time_job.date_end,
            title: 'ต้องการพนักงานเพิ่ม',
          })
        }
      })
    })
    emp.part_time.map((emp) => {
      emp.jobs.map((job) => {
        let more_job_left = job.period_jobs
        let amount_all = job.annoucement.amount_all
        job.deal_job_part_times.map((del_job) => {
          if ((del_job.status === 'confirm' || del_job.status === 'complete') 
          &&
          moment(this.state.currentDate).format("YYYY/MM") === moment(del_job.period_job.date_start).format("YYYY/MM")
          ) {
            active_emp_amount++
            amount_all--
            more_job_left.map((item, index) => {
              if (item.period_job_id === del_job.period_job_id) {
                console.log(more_job_left[index])
                more_job_left[index].amount = more_job_left[index].amount - 1
              }
            })

            active_emp.push({
              user: {
                title: del_job.user.title,
                name: del_job.user.name,
                company_id: job.company_id,
                position: del_job.user.position,
                time_start: del_job.period_job.time_start,
                time_end: del_job.period_job.time_end,
                level: job.level,
                company_name: emp.company_name,
              },
              type: 'PartTime',
              startDate: del_job.period_job.date_start,
              endDate: del_job.period_job.date_end,
              title: 'พนักงานที่ปฏิบัติงาน',
            })
          }
        })
        // console.log('more job', more_job_left);
        if (amount_all > 0) {
          more_job_left.forEach((item) => {
            // console.log('need more time', item.time_start);
            if (item.amount > 0) {
              more_emp.push({
                user: {
                  name: emp.company_name,
                  company_id: job.company_id,
                  position: job.position,
                  level: job.level,
                },
                // amount: amount_all,
                type: 'PartTime',
                startDate: item.date_start,
                endDate: item.date_end,
                time_start: item.time_start,
                time_end: item.time_end,
                title: 'ต้องการพนักงานเพิ่ม',
              })
            }
          })
        }
      })
    })
    // console.log('active emp', active_emp)
    console.log('more_emp', more_emp)

    // console.log('active_emp_amount = ', active_emp_amount);
    let active_company = []
    let active_announcement_amount = 0 //งานที่ลงประกาศ
    let active_company_amount = 0
    announce.full_time.map((announce) => {
      let is_check = false
      announce.jobs.map((job) => {
        if (job.status === 'active' && job.annoucement.status === 'active' 
        && moment(this.state.currentDate).format("YYYY/MM") === moment(job.date_time_job.date_start).format("YYYY/MM")) {
          active_announcement_amount++
          active_company.push({
            user: job,
            company: announce.company_name,
            name: announce.name,
            type: 'FullTime',
            startDate: job.date_time_job.date_start,
            endDate: job.date_time_job.date_end,
            title: 'ผู้ประกอบการ',
          })
          if (is_check === false) {
            active_company_amount++
          }
          is_check = true
        }
      })
    })
    announce.part_time.map((announce) => {
      let is_check = false
      announce.jobs.map((job) => {
        if (job.status === 'active' && job.annoucement.status === 'active') {
          // active_announcement_amount++
          // if (is_check === false) {
          //   active_company_amount++
          // }
          is_check = true
          job.period_jobs.map((period_jobs) => {
            if(moment(this.state.currentDate).format("YYYY/MM") === moment(period_jobs.date_start).format("YYYY/MM")){
              active_announcement_amount++
              if (is_check === false) {
              active_company_amount++
              }
            } 
            active_company.push({
              user: job,
              name: announce.name,
              company: announce.company_name,
              type: 'PartTime',
              startDate: period_jobs.date_start,
              endDate: period_jobs.date_end,
              title: 'ผู้ประกอบการ',
            })
          })
        }
      })
    })
    console.log('active_company', active_company)
    // active_company = _.uniqBy(active_company, (e) => { return e.user.job_id })
    let data = _.union(more_emp, active_emp, active_company)
    // data = _.sortBy(data, ['title']);
    // console.log('data', data)
    // console.log('active_announcement_amount = ', active_announcement_amount);
    // console.log('active_company_amount = ', active_company_amount)
    let more_emp_amount = active_announcement_amount - active_emp_amount

    
    // console.log('Employee: ')
    // more_emp.map(item=>{
    //   console.log(item)
    // })

    this.setState({
      active_emp_amount,
      active_company_amount,
      active_announcement_amount,
      more_emp_amount
    })
  }

  getAllData = async () => {
    const emp = await get(`/api/employee_analysis`).then((res) => res)
    const announce = await get(`/api/announce_analysis`).then((res) => res)
    // const manhour = await get(`/api/employee_analysis`).then((res) => res)
    // console.log('emp:', emp)
    // console.log('announce', announce)
    let active_emp = []
    let active_emp_amount = 0 //แสดงพนักงานที่ปฏิบัติงาน
    // คำนวณพนักงานที่ปฏิบัติงาน
    let more_emp = []
    emp.full_time.map((emp) => {
      emp.jobs.map((job) => {
        let amount_all = job.annoucement.amount_all
        job.deal_jobs.map((del_job) => {
          if ((del_job.status === 'confirm' || del_job.status === 'complete')
          &&
          moment(this.state.currentDate).format("YYYY/MM") === moment(job.date_time_job.date_start).format("YYYY/MM")
          ) {
            active_emp_amount++
            amount_all--
            active_emp.push({
              user: {
                title: del_job.user.title,
                name: del_job.user.name,
                company_id: job.company_id,
                position: del_job.user.position,
                time_start: job.date_time_job.time_start,
                time_end: job.date_time_job.time_end,
                level: job.level,
                company_name: emp.company_name,
              },
              type: 'FullTime',
              startDate: job.date_time_job.date_start,
              endDate: job.date_time_job.date_end,
              title: 'พนักงานที่ปฏิบัติงาน',
            })
          }
        })
        if (amount_all > 0) {
          more_emp.push({
            user: {
              name: emp.company_name,
              company_id: job.company_id,
              position: job.position,
              level: job.level,
            },
            type: 'FullTime',
            amount: amount_all,
            startDate: job.date_time_job.date_start,
            endDate: job.date_time_job.date_end,
            title: 'ต้องการพนักงานเพิ่ม',
          })
        }
      })
    })
    emp.part_time.map((emp) => {
      emp.jobs.map((job) => {
        let more_job_left = job.period_jobs
        let amount_all = job.annoucement.amount_all
        job.deal_job_part_times.map((del_job) => {
          if ((del_job.status === 'confirm' || del_job.status === 'complete') 
          &&
          moment(this.state.currentDate).format("YYYY/MM") === moment(del_job.period_job.date_start).format("YYYY/MM")
          ) {
            active_emp_amount++
            amount_all--
            more_job_left.map((item, index) => {
              if (item.period_job_id === del_job.period_job_id) {
                console.log(more_job_left[index])
                more_job_left[index].amount = more_job_left[index].amount - 1
              }
            })

            active_emp.push({
              user: {
                title: del_job.user.title,
                name: del_job.user.name,
                company_id: job.company_id,
                position: del_job.user.position,
                time_start: del_job.period_job.time_start,
                time_end: del_job.period_job.time_end,
                level: job.level,
                company_name: emp.company_name,
              },
              type: 'PartTime',
              startDate: del_job.period_job.date_start,
              endDate: del_job.period_job.date_end,
              title: 'พนักงานที่ปฏิบัติงาน',
            })
          }
        })
        // console.log('more job', more_job_left);
        if (amount_all > 0) {
          more_job_left.forEach((item) => {
            // console.log('need more time', item.time_start);
            if (item.amount > 0) {
              more_emp.push({
                user: {
                  name: emp.company_name,
                  company_id: job.company_id,
                  position: job.position,
                  level: job.level,
                },
                // amount: amount_all,
                type: 'PartTime',
                startDate: item.date_start,
                endDate: item.date_end,
                time_start: item.time_start,
                time_end: item.time_end,
                title: 'ต้องการพนักงานเพิ่ม',
              })
            }
          })
        }
      })
    })
    // console.log('active emp', active_emp)
    console.log('more_emp', more_emp)

    // console.log('active_emp_amount = ', active_emp_amount);
    let active_company = []
    let active_announcement_amount = 0 //งานที่ลงประกาศ
    let active_company_amount = 0
    announce.full_time.map((announce) => {
      let is_check = false
      announce.jobs.map((job) => {
        if (job.status === 'active' && job.annoucement.status === 'active' 
        && moment(this.state.currentDate).format("YYYY/MM") === moment(job.date_time_job.date_start).format("YYYY/MM")) {
          active_announcement_amount++
          active_company.push({
            user: job,
            company: announce.company_name,
            name: announce.name,
            type: 'FullTime',
            startDate: job.date_time_job.date_start,
            endDate: job.date_time_job.date_end,
            title: 'ผู้ประกอบการ',
          })
          if (is_check === false) {
            active_company_amount++
          }
          is_check = true
        }
      })
    })
    announce.part_time.map((announce) => {
      let is_check = false
      announce.jobs.map((job) => {
        if (job.status === 'active' && job.annoucement.status === 'active') {
          // active_announcement_amount++
          // if (is_check === false) {
          //   active_company_amount++
          // }
          is_check = true
          job.period_jobs.map((period_jobs) => {
            if(moment(this.state.currentDate).format("YYYY/MM") === moment(period_jobs.date_start).format("YYYY/MM")){
              active_announcement_amount++
              if (is_check === false) {
              active_company_amount++
              }
            } 
            active_company.push({
              user: job,
              name: announce.name,
              company: announce.company_name,
              type: 'PartTime',
              startDate: period_jobs.date_start,
              endDate: period_jobs.date_end,
              title: 'ผู้ประกอบการ',
            })
          })
        }
      })
    })
    console.log('active_company', active_company)
    // active_company = _.uniqBy(active_company, (e) => { return e.user.job_id })
    let data = _.union(more_emp, active_emp, active_company)
    // data = _.sortBy(data, ['title']);
    // console.log('data', data)
    // console.log('active_announcement_amount = ', active_announcement_amount);
    // console.log('active_company_amount = ', active_company_amount)
    let more_emp_amount = active_announcement_amount - active_emp_amount

    
    // console.log('Employee: ')
    // more_emp.map(item=>{
    //   console.log(item)
    // })

    this.setState({
      active_emp_amount,
      active_company_amount,
      active_announcement_amount,
      more_emp_amount,
      active_company,
      active_emp,
      more_emp,
      data,
    })
  }

  async componentDidMount() {
    this.props.setPageTitle('Dashboard')
    await this.getAllData()

    console.log('Check')
    this.cleanData(this.state.data)
    setTimeout(() => {
      let target = [...document.querySelectorAll(`[class*="HorizontalAppointment-title"]`)]
      target.map((e) => {
        // console.log(e.innerHTML)
        if (e.innerHTML === 'ผู้ประกอบการ') {
          e.classList.add('font-base-line-purple')
        } else if (e.innerHTML === 'พนักงานที่ปฏิบัติงาน') {
          e.classList.add('font-base-line-green')
        }
      })
      // console.log('target', target)
    }, 1000)
  }

  handleOpen = () => {
    this.setState({
      open: !this.state.open,
    })
  }

  cleanData = (data) => {
    // console.log('...', data)

    const is_exist = (data, array) => {
      // console.log('-------')
      // console.log(data)
      return array.findIndex((member) => {
        return (
          moment(member.startDate).format('YYYY-MM-DD') ===
            moment(data.startDate).format('YYYY-MM-DD') && member.title === data.title
        )
      })
    }

    let newdata = []
    data.forEach((item) => {
      if (newdata.length === 0) {
        newdata.push({
          startDate: item.startDate,
          title: item.title,
          data: [item],
          endDate: moment(item.startDate).add(1, 'hours'),
        })
      } else {
        let con = is_exist(item, newdata)
        // console.log('res', con);
        if (con !== -1) {
          newdata[con].data.push(item)
        } else {
          newdata.push({
            startDate: item.startDate,
            title: item.title,
            data: [item],
            endDate: moment(item.startDate).add(1, 'hours'),
          })
        }
      }
      // console.log('newdata', newdata);
    })
    console.log(newdata)
    this.setState({
      data: newdata,
    })
  }

  render() {
    const { data, currentDate, currentViewName } = this.state

    return (
      <div style={{ marginBottom: '40vh' }}>
        <Paper>
          <Card>
            <div
              className="container"
              style={{
                color: 'black',
              }}>
              <P2>ผู้ประกอบการที่ประกาศงาน</P2>

              <P3 style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
                จำนวน <P1> {this.state.active_company_amount}</P1> คลีนิค
              </P3>
            </div>
            <div className="container side-border" style={{ color: 'white' }}>
              <P2>งานที่ลงประกาศ</P2>
              <P3 style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
                จำนวน <P1> {this.state.active_announcement_amount} </P1> งาน
              </P3>
            </div>
            <div className="container side-border" style={{ color: 'white' }}>
              <P2>พนักงานที่ปฏิบัติงาน</P2>
              <P3 style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
                จำนวน <P1> {this.state.active_emp_amount} </P1> คน
              </P3>
            </div>
            <div className="container side-border" style={{ color: 'white' }}>
              <P2>พนักงานที่ต้องการเพิ่ม</P2>
              <P3 style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
                จำนวน <P1> {this.state.more_emp_amount} </P1> คน
              </P3>
            </div>
          </Card>
          {/* <ExternalViewSwitcher
            currentViewName={currentViewName}
            onChange={this.currentViewNameChange}
          /> */}
          <Scheduler data={data}>
            <ViewState
              currentDate={currentDate}
              currentViewName={currentViewName}
              onCurrentDateChange={this.currentDateChange}
            />
            <EditingState />

            <MonthView />
            <WeekView startDayHour={10} endDayHour={24} />
            <DayView startDayHour={9.5} endDayHour={13.5} />

            <Toolbar />
            {/* <ViewSwitcher /> */}
            <DateNavigator />
            <TodayButton />
            <IntegratedEditing />
            <Appointments appointmentComponent={Appointment} />
            <AppointmentTooltip contentComponent={Content} />
            {/* <AppointmentForm
            // basicLayoutComponent={BasicLayout}
          /> */}
          </Scheduler>
        </Paper>
      </div>
    )
  }
}

const mapState = (state) => ({
  startDateOrder: state.orderManagement.startDateOrder,
  endDateOrder: state.orderManagement.endDateOrder,
  columnOrderManagementApprove: state.orderManagement.columnOrderManagementApprove,
  updateSatatusApprove: state.orderManagement.updateSatatusApprove,
})

const mapDispatch = (dispatch) => {
  return {
    setEmployee: dispatch.orderManagement.setEmployee,
    setCompanyMoneyVatTotal: dispatch.orderManagement.setCompanyMoneyVatTotal,
    setCompanyMoney: dispatch.orderManagement.setCompanyMoney,
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    satatusApprove: dispatch.orderManagement.satatusApprove,
    getOrderApproveList: dispatch.orderManagement.getOrderApproveList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}
export default connect(mapState, mapDispatch)(AnalysisPage)
