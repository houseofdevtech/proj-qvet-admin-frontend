import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import moment from 'moment'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import MoreIcon from '../../images/more.svg'
import EditStatusBroadcasError from '../../components/editsatatusBroadcasError/index'
import Tooltip from '../../components/tooltip'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex:1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  position:relative;
  top: -1px;
  padding: 20px 40px;
  align-items: flex-start;
`

const Icon = styled.div`
  align-self: center;
  img {
  }
  `
class BroadCasError extends Component {

  constructor(props) {
    super(props);
    this.state = {
      column: this.props.customerColumn,
      data: [],
      loading: false,
      pages: null,
      broadcastMode: this.props.broadcastMode,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.broadcastMode !== this.props.broadcastMode && this.state.broadcastMode !== undefined) {
      this.setState({ broadcastMode: this.props.broadcastMode })
      this.getAnnouncement()
    }
    if (this.props.ModeView !== undefined && this.props.ModeView.Type === 'error') {
      if (this.props.ModeView.Type !== '' && this.props.history.location.state === 'error') {
        this.props.checkModeModal()
        this.props.getInAppMessageFailed()
        this.getAnnouncement()
        this.setState({ broadcastMode: this.props.broadcastMode })
      }
    }
  }

  async componentDidMount() {
    await this.getAnnouncement()
    this.props.setBroadcastMode(`${this.props.broadcastMode}`)

  }
  async getAnnouncement() {
    let resp = []
    let resAnnouncement = []
    if (this.state.broadcastMode === 'InAppMessage') {
      resp = await get('admin/broadcast/inappmessage/failed/list')
      for (let i = 0; i < resp.in_app_message.length; i++) {
        this.setState({ loading: false, totalPages: resp.page_count, totaldata: resp.item_count })
        resAnnouncement.push({
          name: resp.in_app_message[i].name,
          id: resp.in_app_message[i].id,
          type: resp.in_app_message[i].type,
          start_datetime: moment(resp.in_app_message[i].start_datetime).format('YYYY-MM-DD'),
          end_datetime: moment(resp.in_app_message[i].end_datetime).format('YYYY-MM-DD'),
        })
      }
    }
    if (this.state.broadcastMode === 'InAppMessage') {
      this.setState({ data: resAnnouncement })
    } else {
      this.setState({ data: [] })
    }
  }
  onScroll = (e) => {
    if (e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight && e.target.scrollTop) {
      if (this.state.totalPages > 1 && this.state.data.length < this.state.totaldata) {
        this.setState({ numberPage: this.state.numberPage + 1, loading: true })
        setTimeout(() => {
          if (this.state.broadcastMode === 'InAppMessage') {
            get(`admin/broadcast/inappmessage/failed/list?page=${this.state.numberPage}&limit=10`).then(resp => {
              if (resp.in_app_message.length > 0) {
                for (let i = 0; i < resp.in_app_message.length; i++) {
                  this.state.data.push({
                    name: resp.in_app_message[i].name,
                    id: resp.in_app_message[i].id,
                    start_datetime: moment(resp.in_app_message[i].start_datetime).format('YYYY-MM-DD'),
                    end_datetime: moment(resp.in_app_message[i].end_datetime).format('YYYY-MM-DD'),
                    text: resp.in_app_message[i].text
                  })
                }
                this.setState({
                  loading: false,
                  PageSize: this.state.data.length
                })
              }
            })
          }


        }, 100);
      }
    }
  }

  render() {
    const allColumns = [
      {
        Header: "",
        accessor: "edit",
        Cell: (row) => (
          <div style={{ width: '100%', cursor: 'pointer' }}>
            <Tooltip
              placement="bottom"
              trigger="click"
              tooltip={<EditStatusBroadcasError TypeStatus={row.original.id} props={this.props} />}>
              <Icon ><img style={{ width: "5px" }} alt="moreIcon" src={MoreIcon} /></Icon>
            </Tooltip>
          </div>
        ),
        minWidth: 30,
      },
      {
        Header: () => <h6>Template Name</h6>,
        accessor: 'name', // String-based value accessors!
        minWidth: 120,
      },
      {
        Header: () => <h6>Id</h6>,
        accessor: 'id',
        minWidth: 80,
      },
      {
        Header: () => <h6>Type</h6>,
        accessor: 'type',
        minWidth: 80
      },
      {
        Header: () => <h6>Start Date</h6>,
        accessor: 'start_datetime',
        minWidth: 100,
      },
      {
        Header: () => <h6>End Date</h6>,
        accessor: 'end_datetime',
        minWidth: 100,
      },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      if (this.state.broadcastMode === 'InAppMessage' && colName.accessor === "type") {
        colName.isShow = false
      }
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor &&
          columnWillShow.push(obj)
      }
      return {}
    })
    return (
      <div >
        <Card className="col-md-9">
          {this.state.data.length >= 0 ?
            <div onScroll={this.onScroll} style={{ width: '100%' }}>
              <ReactTable
                data={this.state.data} //data object
                columns={columnWillShow}  //column config object
                loading={this.state.loading}
                pageSize={this.state.PageSize}
                showPagination={false}
                style={{
                  height: "400px"
                }}
                className="-striped -highlight"
              />
            </div>
            :
            null
          }
        </Card>
      </div>
    )
  }
}

const mapState = (state) => ({
  broadcastMode: state.broadcasting.broadcastMode,
  customerColumn: state.broadcasting.columnBroadcast,
  dataInAppMessageFailedList: state.broadcasting.dataInAppMessageFailedList,
  ModeView: state.broadcasting.ModeView,

})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    setBroadcastMode: dispatch.broadcasting.setBroadcastMode,
    getInAppMessageFailedList: dispatch.broadcasting.getInAppMessageFailedList,
    checkModeModal: dispatch.broadcasting.checkModeModal,
    getInAppMessageFailed: dispatch.broadcasting.getInAppMessageFailed

  }
}
export default connect(
  mapState,
  mapDispatch
)(BroadCasError)

