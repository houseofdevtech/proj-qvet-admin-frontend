import React, { useState } from 'react'
import { connect } from 'react-redux'
import ChatLeft from './chatLeft'
import ChatRight from './chatRight'
// import messageApi from '../../utils/sendbird/messageApi'

const ChatBoxPage = (props) => {
    const [curChannel, setChannel] = useState()

    if (Object.keys(props.sbUser).length  === 0) {
        const fetchData = async () => {
            props.setPageTitle('Chat Box')
            await props.sbConnect()
            await props.sbGetGroupChannelList()
        }

        fetchData()
    }

    const onClickGroupChannel = (channel) => {
        if (!curChannel || curChannel.url !== channel.url) {
            setChannel(channel)
            props.sbGetMessageList(channel.url)
            // props.sbGetMessageList({ channelUrl: channel.url })
        }
    }

    const onInputFocus = () => {
       props.sbMarkRead(curChannel)
    }

    const onSendMessage = (text) => {
        const data = {
            message: text,
            channelUrl: curChannel.url
        }
        props.sbSendMessage(data)
    }

    const onSendFile = (file) => {
        file.channelUrl = curChannel.url
        props.sbSendFile(file)
    }

    const filterChannel = (filter) => {
        props.sbGetGroupChannelList(filter)
    }

    // const loadChannelList = (limit) => {
    //     props.sbGetGroupChannelList({ limit })
    // }

    // const loadMessageList = (limit) => {
    //     const payload = {
    //         limit,
    //         channelUrl: curChannel.url
    //     }
    //     props.sbGetMessageList(payload)
    // }

    return (
        <div>
            <ChatLeft channelList={props.sbChannelList}
                      onClickGroupChannel={onClickGroupChannel}
                      channel={curChannel}
                      filterChannel={filterChannel}
            />
            <ChatRight messageList={props.sbMessageList}
                       userId={props.sbUser.userId}
                       onInputFocus={onInputFocus}
                       onSendMessage={onSendMessage}
                       onSendFile={onSendFile}
                       channel={curChannel}
            />
            ChatBoxPage
        </div>
    )
}

const mapState = (state) => {
    return {
        sbUser: state.sbUser.sbUser,
        sbChannelList: state.sbChannel.sbChannelList,
        sbMessageList: state.sbMessage.sbMessageList
    }
}

const mapDispatch = (dispatch) => {
  return { 
      setPageTitle: dispatch.Title.setPageTitle,
      sbConnect: dispatch.sbUser.sbConnect,
      sbGetGroupChannelList: dispatch.sbChannel.sbGetGroupChannelList,
    //   getGroupChannel: dispatch.sbChannel.getGroupChannel,
      sbGetMessageList: dispatch.sbMessage.sbGetMessageList,
      sbSendMessage: dispatch.sbMessage.sbSendMessage,
      sbSendFile: dispatch.sbMessage.sbSendFile,
      sbMarkRead: dispatch.sbMessage.sbMarkRead
    }
}

export default connect(
  mapState,
  mapDispatch
)(ChatBoxPage)
