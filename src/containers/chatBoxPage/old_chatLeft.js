import React, { Component } from 'react'

export default class ChatLeft extends Component {
  render() {
    return (
      <div>
  <div className="left-part bg-light fixed-left-part  border-top">
  {/* Mobile toggle button */}
  {/* Wit :: Change template from a -> div for fix the warning  */}
  <div className="ti-menu ti-close btn btn-success show-left-part d-block d-md-none" href="/#" />
  {/* Mobile toggle button */}
  <div className="p-15">
    <h4>Chat Sidebar</h4>
  </div>
  <div className="scrollable position-relative" style={{height: '100%'}}>
    <div className="p-15">
      <h5 className="card-title">Search Contact</h5>
      <form>
        <input className="form-control" type="text" placeholder="Search Contact" />
      </form>
    </div>
    <hr />
    <ul className="mailbox list-style-none">
      <li>
        <div className="message-center chat-scroll">
          <a href="/#" className="message-item" id="chat_user_1" data-user-id={1}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/1.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status online pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Pavan kumar</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:30 AM</span>
            </div>
          </a>
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_2" data-user-id={2}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/2.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status busy pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Sonu Nigam</h5>
              <span className="mail-desc">I've sung a song! See you at</span>
              <span className="time">9:10 AM</span>
            </div>
          </a>
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_3" data-user-id={3}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/3.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status away pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Arijit Sinh</h5>
              <span className="mail-desc">I am a singer!</span>
              <span className="time">9:08 AM</span>
            </div>
          </a>
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_4" data-user-id={4}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/4.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status offline pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Nirav Joshi</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:02 AM</span>
            </div>
          </a>
          {/* Message */}
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_5" data-user-id={5}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/5.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status offline pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Sunil Joshi</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:02 AM</span>
            </div>
          </a>
          {/* Message */}
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_6" data-user-id={6}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/6.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status offline pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Akshay Kumar</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:02 AM</span>
            </div>
          </a>
          {/* Message */}
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_7" data-user-id={7}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/7.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status offline pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Pavan kumar</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:02 AM</span>
            </div>
          </a>
          {/* Message */}
          {/* Message */}
          <a href="/#" className="message-item" id="chat_user_8" data-user-id={8}>
            <span className="user-img">
              <img src={`${process.env.PUBLIC_URL}/assets/images/users/8.jpg`} alt="user" className="rounded-circle" />
              <span className="profile-status offline pull-right" />
            </span>
            <div className="mail-contnet">
              <h5 className="message-title">Varun Dhavan</h5>
              <span className="mail-desc">Just see the my admin!</span>
              <span className="time">9:02 AM</span>
            </div>
          </a>
          {/* Message */}
        </div>
      </li>
    </ul>
  </div>
</div>

      </div>
    )
  }
}
