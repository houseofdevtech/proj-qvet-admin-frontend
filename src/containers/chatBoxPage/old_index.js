import React, { Component } from 'react'
import { connect } from 'react-redux'

import ChatLeft from './chatLeft'
import ChatRight from './chatRight'

class ChatBoxPage extends Component {
  componentDidMount() {
    this.props.setPageTitle('Chat Box')
  }

  render() {
    return (
      <div>
        <ChatLeft />
        <ChatRight />
        ChatBoxPage
      </div>
    )
  }
}

const mapState = () => ({})

const mapDispatch = (dispatch) => {
  return { setPageTitle: dispatch.Title.setPageTitle }
}

export default connect(
  mapState,
  mapDispatch
)(ChatBoxPage)
