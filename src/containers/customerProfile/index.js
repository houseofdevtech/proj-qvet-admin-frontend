import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import 'moment/locale/th'
import Checkbox from 'rc-checkbox'
import moment from 'moment'
import styled from 'styled-components'
import Rightbar from '../../layouts/orderManagementLayout/rightbar'
import { get, put } from '../../utils/service'
import './orderManagefilterBy.css'
import '../../style/form-custom.scss'
import Swal from 'sweetalert2'

moment.locale('th')
const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

const Container = styled.div`
  padding: 20px;
`

// const Footer = styled.div`
//   position: absolute;
//   left: 10px;
//   bottom: 50px;
// `
// const FooterPicContainer = styled.div`
//   display: flex;
//   padding: 0 0 0 30px;
//   img {
//     margin-right: 10px;
//   }
// `

const Skill = styled.div`
  display: flex;
  flex-direction: row;
`
const UTEXT = styled.h5`
  color: #3e5569;
  text-decoration: underline;
`
const Div = styled.div`
  display: flex;
  flex-direction: column;
`
const Card = styled.div`
  float: left;
  width: 33.333%;
  padding: 0.75rem;
  margin-bottom: 2rem;
  border: 0;
`

const Card2 = styled.div`
  float: left;
  width: 95%;
  padding: 0.75rem;
  margin-bottom: 0rem;
  border: 0;
`

class customerProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      id: '',
      bank: {},
      edu: [],
      skill: {},
      license: {},
      idCard: {},
      workZone: {},
      isApproved: false,
      workexp: [],
      column: this.props.columnMaidProfile,
      showRightbar: false,
      dataProfile: [],
      salary: '',
      start_datetime: '',
      end_datetime: '',
      listWeekPayment: [],
      Tempweek: '',
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
      yearList: [],
      yaerValue: '',
    }
  }

  componentDidMount() {
    // console.log('pxxx', this.props.match.params.id)
    this.setState({ id: this.props.match.params.id })
    this.props.showBackIcon(true)
    this.getDataProfile()
    // this.getDataCalendarTable()
    this.setDateForBooking()
    this.props.setStartdateOrder('')
    this.props.setEndateOrder('')
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.columnMaidProfile) {
      this.setState({ column: this.props.columnMaidProfile })
    }
  }

  handleChangeStartDate = (e) => {
    this.setState({ start_datetime: e }, () => this.getDataCalendarTable())
  }
  handleChangeEndDate = (e) => {
    this.setState({ end_datetime: e }, () => this.getDataCalendarTable())
  }

  async handleChangeYear(year) {
    this._isMounted = true
    if (this._isMounted) {
      var d = new Date()
      var y = d.getFullYear()
      let Tempyaer = 0
      if (year !== undefined) {
        Tempyaer = year
      } else {
        Tempyaer = y
      }
      let array = []
      let temparray = []
      let temMonth = Tempyaer % 4
      if (Tempyaer) {
        for (let i = 0; i < 12; i++) {
          if (i < 9) {
            if (i === 1) {
              if (temMonth === 0) {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-29`,
                )
              } else {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-28`,
                )
              }
            } else if (i === 0 || i === 2 || i === 4 || i === 6 || i === 7 || i === 9) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-31`,
              )
            } else if (i === 3 || i === 5 || i === 8) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-30`,
              )
            }
          } else {
            if (i === 9 || i === 11) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-31`,
              )
            } else if (i === 10) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-30`,
              )
            }
          }
        }
        if (array.length >= 12) {
          for (let i = 0; i < array.length; i++) {
            temparray.push(...array[i].split(','))
          }
        }
      }
      this.setState({ listWeekPayment: temparray, yaerValue: year })
    }
  }

  async SelectDatePyment(el) {
    this.setState({ Tempweek: el })
    let setdate = el.split(' - ')
    let sDate = setdate[0]
    let eDate = setdate[1]
    await this.getDataCalendarTable(sDate, eDate)
  }

  async setDateForBooking() {
    const d = new Date()
    const n = d.getDate()
    //yaerList
    var nYear = d.getFullYear()
    var num = nYear - 5
    this.setState({ yaerValue: nYear })
    if (num) {
      for (var i = num; i <= nYear; i++) {
        this.state.yearList.push(i)
      }
    }
    if (n <= 15) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 1).format()),
        end_datetime: new Date(moment().set('date', 15).format()),
      })
    } else if (n >= 16) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 16).format()),
        end_datetime: new Date(moment(d).endOf('month').format()),
      })
    }
    await this.getDataCalendarTable()
    this.handleChangeYear()
    if (this.props.history.location.state !== undefined) {
      this.setState({
        Tempweek: `${moment(this.props.history.location.state.start_datetime, 'YYYY-MM-DD').format(
          'YYYY-MM-DD',
        )} - ${moment(this.props.history.location.state.end_datetime, 'YYYY-MM-DD').format(
          'YYYY-MM-DD',
        )}`,
      })
    }
  }

  async getDataProfile() {
    const id = this.props.match.params.id
    const all = await get('api/allcompany')
    const single = _.filter(all, (e) => {
      return e.member === this.props.match.params.id.toString()
    })
    const resp = await get(`api/profile/${id}`)
    const compDetail = await get(`api/details/${single[0].company_id}`)
    const images = await get(`api/image/profile/${id}`)
    const license = await get(`api/image/company/${id}`)
    const imageCom = await get(`/api/image/place/${single[0].company_id}`)
    const idCard = await get(`api/image/idcard/${id}`)
    // const license = await get(`api/image/license/${id}`)
    const bank = await get(`api/image/bank/${id}`)

    this.props.setPageTitle(single[0].name)
    // console.log('single', all)
    let edu = await get(`api/education/${single[0].company_id}`)
    var workexp = await get(`api/experience/${single[0].company_id}`)
    var skill = await get(`/api/skill/${single[0].company_id}`)

    if(single[0]?.approve_status === 'approved') {
      this.setState({isApproved: true})
    }
    this.setState({
      dataProfile: {
        comId: single[0].company_id,
        picture: images.link ? images.link : ' ',
        name: resp.profile.nickname ? resp.profile.nickname : ' ',
        nameCom: single[0].company_name ? single[0].company_name : ' ',
        namepic: single[0].name ? single[0].name : ' ',
        imageCom: imageCom.link ?? '',
        bday: resp.profile.birth_day ? resp.profile.birth_day : ' ',
        age: resp.profile.age ? resp.profile.age : ' ',
        line_id: resp.profile.line_id ? resp.profile.line_id : ' ',
        idcard: resp.profile.idcard ? resp.profile.idcard : ' ',
        email: single[0].email ? single[0].email : ' ',
        phone: single[0].phone ? single[0].phone : ' ',
        toolCom: compDetail?.tools_com ?? ' ',
        timeCom: compDetail?.time_com ? compDetail.time_com : ' ',
        costCom: compDetail?.cost_com ? compDetail.cost_com : ' ',
        comPhone: compDetail?.phone ? compDetail.phone : ' ',
        comAddr:
          compDetail ? (
            compDetail?.no + ' ' +
            `${compDetail?.zone}` +
            ' ' +
            `อำเภอ ${compDetail?.district}` +
            ' ' +
            `จังหวัด ${compDetail?.province}` +
            ' ' +
            `รหัสไปรษณ์ ${compDetail?.postcode}`) : '-',
        realAddr: resp.address?.no
          + ` ` +
          resp.address?.district +
          ' ' +
          resp.address?.zone +
          ' ' +
          resp.address?.province +
          ' ' +
          `รหัสไปรษณ์ ${resp.address?.postcode}`
        ,
        address: resp.addressNow?.no_now
          + ' ' +
          resp.addressNow?.district_now +
          ' ' +
          resp.addressNow?.zone_now +
          ' ' +
          resp.addressNow?.province_now +
          ' ' +
          `รหัสไปรษณ์ ${resp.addressNow?.postcode_now}`,
      },
      workexp: workexp ?? ' ',
      workZone: skill?.work ?? ' ',
      edu: edu ?? ' ',
      idCard: idCard ?? ' ',
      license: license ?? ' ',
      bank: bank ?? ' ',
      skill: skill.skill ?? ' ',
    })
  }

  async getDataCalendarTable(start_date, end_date) {
    let { startDate, endDate } = this.state
    if (start_date && end_date) {
      startDate = moment(start_date, 'YYYY-MM-DD').format('YYYY-MM-DD')
      endDate = moment(end_date, 'YYYY-MM-DD').format('YYYY-MM-DD')
    } else {
      startDate = moment(this.state.start_datetime).format('YYYY-MM-DD')
      endDate = moment(this.state.end_datetime).format('YYYY-MM-DD')
    }
    this.setState({ loading: true })
    if (startDate && endDate) {
      if (this.props.location.state !== undefined) {
        const id = this.props.location.state.Maid_ID
        const resp = await get(`admin/maid/id/${id}/details`)
        let tempDate = []
        console.log(resp)
        if (resp.length <= 10) {
          this.setState({ loading: false, PageSize: 10 })
          for (let i = 0; i < resp.length; i++) {
            tempDate.push({
              Mounth: resp[i].title,
              Work_completed: resp[i].no_of_work_completed,
              Hrs_completed: resp[i].no_of_hours_completed,
              salary: resp[i].salary,
              star_bonus: resp[i].five_star_bonus,
              Monthly_bonus: resp[i].monthly_bonus,
              Milestone_bonus: resp[i].milestone_bonus,
            })
          }
        } else {
          for (let i = 0; i < resp.length; i++) {
            this.setState({ loading: false, PageSize: resp.length })
            tempDate.push({
              Mounth: resp[i].title,
              Work_completed: resp[i].no_of_work_completed,
              Hrs_completed: resp[i].no_of_hours_completed,
              salary: resp[i].salary,
              star_bonus: resp[i].five_star_bonus,
              Monthly_bonus: resp[i].monthly_bonus,
              Milestone_bonus: resp[i].milestone_bonus,
            })
          }
        }
        this.setState({ data: tempDate, salary: resp.salary })
      }
    }
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }
  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    const { history } = this.props
    history.push(this.props.history.location.pathname)
  }
  approveHandler = async (cId) => {
    console.log('cId', cId)

    const approveResult = await put('api/approved/company/', { company_id: cId })
    if (approveResult?.message === 'Approve Complete') {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'success',
        text: 'ทำการอนุมัติ เรียบร้อย',
        showConfirmButton: false,
        animation: false,
      })

      this.setState({ isApproved: true })
    } else {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'error',
        text: 'ระบบไม่สามารถทำการตรวจสอบได้ในขณะนี้ โปรดติดต่อผู้ดูแล',
        showConfirmButton: false,
        animation: false,
      })
    }
  }
  inValidHandler = async(cId) => {
    const disApproveResult = await put('api/disapproved/company/', { company_id: cId })
    if (disApproveResult?.message === 'Dispprove Complete') {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'success',
        text: 'ไม่อนุมัติผู้ประกอบการ เรียบร้อย',
        showConfirmButton: false,
        animation: false,
      })

      this.setState({ isApproved: false })
    } else {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'error',
        text: 'ระบบไม่สามารถทำการตรวจสอบได้ในขณะนี้ โปรดติดต่อผู้ดูแล',
        showConfirmButton: false,
        animation: false,
      })
    }
}

  render() {
    const currentLocation = this.props.history.location.pathname
    const { dataProfile, edu, workexp, idCard, skill, bank, license, workZone } = this.state
    console.log('jjj', license)
    return (
      <Container>
        <div className="container-fluid">
          {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')}/>}
          {/* Row */}
          <div>
            <div className="row ">
              {/* Column */}
              <Card>
                <div
                  className="card h-vh-80 col-md-12"
                  style={{
                    width: '330px',
                    height: 'auto',
                    borderRadius: '6px',
                    border: ' solid 1px #324755',
                    backgroundColor: '#ffffff',
                  }}>
                  <div className="card-body">
                    <center className="m-t-30">
                      <img
                        src={`${dataProfile?.picture}`}
                        alt="profile"
                        className="rounded-circle"
                        width={150}
                      />

                      <h4 className="card-title m-t-10">{dataProfile?.namepic}</h4>
                    </center>
                    <small className="text-muted">ชื่อสภานประกอบการ </small>
                    <h6>{dataProfile?.nameCom}</h6>
                    <small className="text-muted">ชื่อเล่น </small>
                    <h6>{dataProfile?.name}</h6>
                    <small className="text-muted">เลขบัตรประชาชน </small>
                    <h6>{dataProfile?.idcard}</h6>
                    <small className="text-muted">วันเกิด </small>
                    <h6>{moment(dataProfile?.bday).format('LL')}</h6>
                    <small className="text-muted">อายุ</small>
                    <h6>{dataProfile?.email}</h6>
                    <small className="text-muted p-t-30 db">เบอร์โทร</small>
                    <h6>{dataProfile?.phone}</h6>
                    <small className="text-muted p-t-30 db">Line</small>
                    <h6>{dataProfile?.line_id}</h6>
                    <small className="text-muted p-t-30 db">ที่อยู่</small>
                    <h6>{dataProfile?.address}</h6>
                  </div>
                </div>
              </Card>
              <div className="col">
                <Card2>
                  <div
                    className="card "
                    style={{
                      width: '100%',
                      height: 'auto',
                      borderRadius: '6px',
                      border: ' solid 1px #324755',
                      backgroundColor: '#ffffff',
                    }}>
                    <div className="card-body">
                      <div className="d-flex">
                        <figure className="figure">
                          <img
                            width={200}
                            src={idCard?.link}
                            className="figure-img  rounded img-thumbnail thumb-post"
                            data-toggle="modal"
                            data-target="#employerModalCenter1"
                          />
                          <div
                            class="modal fade"
                            id="employerModalCenter1"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <img src={idCard?.link} className="img-fluid rounded"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <figcaption className="font-12 figure-caption">
                            รูปบัตรประจำตัวประชาชน
                          </figcaption>
                        </figure>

                        <figure className="figure">
                          <img
                            width={200}
                            src={license?.link}
                            className="figure-img rounded img-thumbnail thumb-post"
                            data-toggle="modal"
                            data-target="#employerModalCenter2"
                          />
                          <div
                            class="modal fade"
                            id="employerModalCenter2"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <img src={license?.link} className="img-fluid rounded"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <figcaption className="font-12 figure-caption">
                            ใบอนุญาติประกอบวิชาชีพ
                          </figcaption>
                        </figure>
                      </div>
                      <UTEXT>ที่อยู่ตามบัตรประชาชน </UTEXT>
                      <h6>{dataProfile?.realAddr}</h6>
                      <br/>
                      <UTEXT>สถานประกอบการ </UTEXT>
                      <p>{dataProfile?.nameCom}</p>
                      <figure className="figure">
                        <img
                          width={200}
                          src={dataProfile?.imageCom}
                          className="figure-img rounded img-thumbnail thumb-post"
                          data-toggle="modal"
                          data-target="#employerModalCenter3"
                        />
                        <div
                          class="modal fade"
                          id="employerModalCenter3"
                          tabindex="-1"
                          role="dialog"
                          aria-labelledby="exampleModalCenterTitle"
                          aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <img src={dataProfile?.imageCom} className="img-fluid rounded"/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <figcaption className="font-12 figure-caption">
                          รูปภาพสภานที่ประกอบการ
                        </figcaption>
                      </figure>

                      <UTEXT>ที่อยู่สถานประกอบการ </UTEXT>
                      <h6>{dataProfile?.comAddr}</h6>
                      <UTEXT>ที่อยู่จากแผนที่ </UTEXT>
                      <br/>
                      <UTEXT>เบอร์โทร </UTEXT>
                      <h6>{dataProfile?.comPhone}</h6>
                      <UTEXT>เครื่องมือที่ทางโรงพยาบาลสัตว์ของท่านมี </UTEXT>
                      <div className="d-flex justify-content-around">
                        <div>
                          <Checkbox checked={dataProfile?.toolCom?.x_ray}/>
                          <h6>X-Ray</h6>
                        </div>
                        <div>
                          <Checkbox checked={dataProfile?.toolCom?.untrasound}/>
                          <h6>Untrasound</h6>
                        </div>
                        <div>
                          <Checkbox checked={dataProfile?.toolCom?.cbc}/>
                          <h6>CBC</h6>
                        </div>
                        <div>
                          <Checkbox checked={dataProfile?.toolCom?.blood}/>
                          <h6>Blood</h6>
                        </div>
                        <div>
                          <Checkbox checked={dataProfile?.toolCom?.anesthesia}/>
                          <h6>Anesthesia</h6>
                        </div>
                      </div>
                      <br/>
                      <UTEXT>จำนวนคุณหมอประจำคลีนิกของท่านในแต่ละวัน </UTEXT>
                      <h6>2</h6>
                      <UTEXT>ราคาคิดเฉลี่ยนต่อเคส</UTEXT>
                      <h6>
                        ต่ำสุดโดยเฉลี่ย {dataProfile?.costCom?.min_cost} สูงสุดโดยเฉลี่ย{' '}
                        {dataProfile?.costCom?.max_cost}
                      </h6>
                      <UTEXT>เวลาเปิด - ปิด คลีนิกของท่าน</UTEXT>
                      <h6>
                        {dataProfile?.timeCom?.start} - {dataProfile?.timeCom?.end}{' '}
                      </h6>
                    </div>
                  </div>
                </Card2>
                <div className="container btn-float-right">
                  {this.state.isApproved ?
                    <div className="btn green-highlight  btn-margin-lr"
                         onClick={() => this.approveHandler(dataProfile.comId)}>✅ ตรวจสอบแล้ว</div>
                    : <div className="btn purple-highlight  btn-margin-lr"
                           onClick={() => this.approveHandler(dataProfile.comId)}>ตรวจสอบแล้ว</div>
                  }

                  <div className="btn purple-highlight btn-margin-lr" onClick={() => this.inValidHandler(dataProfile.comId)}>ข้อมูลไม่ถูกต้อง</div>
                </div>
              </div>
            </div>
          </div>
          <Rightbar data={this.state} currentLocation={currentLocation}/>
          {this.props.children}
        </div>
      </Container>
    )
  }
}

const mapState = (state) => ({
  columnMaidProfile: state.maid.columnMaidProfile,
  selectedCustomer: state.customer.selectedCustomer,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(customerProfile)
