export const Graph1 = {
  options: {
    chart: {
      width: "100%",
      height: 400,
      id: 'apexchart-example',
    },
    xaxis: {
      categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999],
      labels: {
        formatter: function(value) {
          return value
        },
      },
      title: {
        text: 'Years',
      },
    },
    yaxis: {
      labels: {
        formatter: function(value) {
          return value + '$'
        },
      },
      title: {
        text: 'US Dollar',
      },
    },
    legend: {
      position: "right",
      verticalAlign: "top",
      containerMargin: {
        left: 35,
        right: 60
      }
    },
    colors: ['#fea735', '#0077ff', '#fe7235'],
    responsive: [
      {
        breakpoint: 1000,
        options: {
          width: "400px",
          height: "200px",
          legend: {
            position: 'bottom',
          },
        },
      },
    ],
  },
  series: [
    {
      name: 'Total sales',
      data: [30, 40, 35, 50, 49, 60, 70, 91, 125],
    },
    {
      name: 'Maid Wages',
      data: [50, 60, 85, 90, 129, 150, 160, 191, 210],
    },
  ],
}
