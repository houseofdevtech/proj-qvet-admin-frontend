
 const ovi01_1 = {
  title: {
    text: null,
  },

  xAxis:{
    categories: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  },

  yAxis: {
    title: {
      text: 'Price (THB)',
    },
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
      type: 'line',
    },
  },

  series: [
    {
      name: 'Total Sales',
      data: [100000, 200000, 250000, 500000, 550000, 520000, 600000],
    },
    {
      name: 'Maid’s wages',
      data: [60000, 80000, 100000, 120000, 210000, 250000, 300000],
    },

  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 700,
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
          },
        },
      },
    ],
  },
}

export default ovi01_1