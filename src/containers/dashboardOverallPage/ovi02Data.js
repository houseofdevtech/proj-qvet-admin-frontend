 const ovi02_1 = {
  title: {
    text: null,
  },

  xAxis:{
    categories: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  },

  yAxis: {
    title: {
      text: 'Number of Services / Hours',
    },
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
      type: 'line',
    },
  },

  series: [
    {
      name: 'Number of Service',
      data: [5, 7, 10, 20, 25, 27, 30],
    },
    {
      name: 'Number of Average Hours per service',
      data: [2, 2.5, 1, 2, 2.5, 2.6, 2.7],
    },

  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 700,
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
          },
        },
      },
    ],
  },
}
export default ovi02_1
