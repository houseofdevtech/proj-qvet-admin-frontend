export const ovi03_1 = {
  title: {
    text: null,
  },

  xAxis:{
    categories: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  },

  yAxis: {
    title: {
      text: 'Number of Bookings',
    },
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
      type: 'line',
    },
  },

  series: [
    {
      name: 'Number of one-time book',
      data: [2, 5, 6, 7, 8, 10, 14],
    },
    {
      name: 'Number of multi-package',
      data: [1, 2, 4, 3, 6, 8, 9],
    },

  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 700,
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
          },
        },
      },
    ],
  },
}
