export const ovi0506_1 = {
  title: {
    text: null,
  },

  xAxis:{
    categories: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  },

  yAxis: {
    title: {
      text: 'Number of Bookings',
    },
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
      type: 'line',
    },
  },

  series: [
    {
      name: 'Number of new login users',
      data: [3, 10, 15, 20, 18, 10, 20],
    },
    {
      name: 'Number of new service users',
      data: [1, 5, 8, 15, 14, 15, 16],
    },

  ],

  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 700,
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
          },
        },
      },
    ],
  },
}
