import React, { Component } from 'react'

import TestContent from '../testPage/'

class HomePage extends Component {
  render() {
    return (
      <div>
        <TestContent />
      </div>
    )
  }
}

export default HomePage
