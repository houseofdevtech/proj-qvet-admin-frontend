import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import 'moment/locale/th'
import moment from 'moment'
import styled from 'styled-components'
import Rightbar from '../../layouts/orderManagementLayout/rightbar'
import { get, put } from '../../utils/service'
import './orderManagefilterBy.css'
import '../../style/form-custom.scss'
import Swal from "sweetalert2"

moment.locale('th')
const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

const Container = styled.div`
  padding: 20px;
`

// const Footer = styled.div`
//   position: absolute;
//   left: 10px;
//   bottom: 50px;
// `
// const FooterPicContainer = styled.div`
//   display: flex;
//   padding: 0 0 0 30px;
//   img {
//     margin-right: 10px;
//   }
// `

const Skill = styled.div`
  display: flex;
  flex-direction: row;
`
const UTEXT = styled.h5`
  color: #3e5569;
  text-decoration: underline;
`
const Div = styled.div`
  display: flex;
  flex-direction: column;
`
const Card = styled.div`
  float: left;
  width: 33.333%;
  padding: 0.75rem;
  margin-bottom: 2rem;
  border: 0;
`

const Card2 = styled.div`
  float: left;
  width: 95%;
  padding: 0.75rem;
  margin-bottom: 2rem;
  border: 0;
`

class maidProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      id: '',
      bank: {},
      edu: [],
      skill: {},
      license: {},
      idCard: {},
      workZone: {},
      workexp: [],
      isApproved: false,
      column: this.props.columnMaidProfile,
      showRightbar: false,
      dataProfile: [],
      salary: '',
      start_datetime: '',
      end_datetime: '',
      listWeekPayment: [],
      Tempweek: '',
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
      yearList: [],
      yaerValue: '',
    }
  }

  componentDidMount() {
    // console.log('pxxx', this.props.match.params.id)
    this.setState({ id: this.props.match.params.id })
    this.props.showBackIcon(true)
    this.getDataProfile()
    // this.getDataCalendarTable()
    this.setDateForBooking()
    this.props.setStartdateOrder('')
    this.props.setEndateOrder('')
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.columnMaidProfile) {
      this.setState({ column: this.props.columnMaidProfile })
    }
  }

  handleChangeStartDate = (e) => {
    this.setState({ start_datetime: e }, () => this.getDataCalendarTable())
  }
  handleChangeEndDate = (e) => {
    this.setState({ end_datetime: e }, () => this.getDataCalendarTable())
  }

  async handleChangeYear(year) {
    this._isMounted = true
    if (this._isMounted) {
      var d = new Date()
      var y = d.getFullYear()
      let Tempyaer = 0
      if (year !== undefined) {
        Tempyaer = year
      } else {
        Tempyaer = y
      }
      let array = []
      let temparray = []
      let temMonth = Tempyaer % 4
      if (Tempyaer) {
        for (let i = 0; i < 12; i++) {
          if (i < 9) {
            if (i === 1) {
              if (temMonth === 0) {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-29`
                )
              } else {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-28`
                )
              }
            } else if (i === 0 || i === 2 || i === 4 || i === 6 || i === 7 || i === 9) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-31`
              )
            } else if (i === 3 || i === 5 || i === 8) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-30`
              )
            }
          } else {
            if (i === 9 || i === 11) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-31`
              )
            } else if (i === 10) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-30`
              )
            }
          }
        }
        if (array.length >= 12) {
          for (let i = 0; i < array.length; i++) {
            temparray.push(...array[i].split(','))
          }
        }
      }
      this.setState({ listWeekPayment: temparray, yaerValue: year })
    }
  }

  async SelectDatePyment(el) {
    this.setState({ Tempweek: el })
    let setdate = el.split(' - ')
    let sDate = setdate[0]
    let eDate = setdate[1]
    await this.getDataCalendarTable(sDate, eDate)
  }

  async setDateForBooking() {
    const d = new Date()
    const n = d.getDate()
    //yaerList
    var nYear = d.getFullYear()
    var num = nYear - 5
    this.setState({ yaerValue: nYear })
    if (num) {
      for (var i = num; i <= nYear; i++) {
        this.state.yearList.push(i)
      }
    }
    if (n <= 15) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 1).format()),
        end_datetime: new Date(moment().set('date', 15).format()),
      })
    } else if (n >= 16) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 16).format()),
        end_datetime: new Date(moment(d).endOf('month').format()),
      })
    }
    await this.getDataCalendarTable()
    this.handleChangeYear()
    if (this.props.history.location.state !== undefined) {
      this.setState({
        Tempweek: `${moment(this.props.history.location.state.start_datetime, 'YYYY-MM-DD').format(
          'YYYY-MM-DD'
        )} - ${moment(this.props.history.location.state.end_datetime, 'YYYY-MM-DD').format(
          'YYYY-MM-DD'
        )}`,
      })
    }
  }

  async getDataProfile() {
    const id = this.props.match.params.id
    const uId =this.props.match.params.uId
    const all = await get('api/alluser')
    const single = _.filter(all, (e) => {
      return e.member === this.props.match.params.id.toString()
    })
    const resp = await get(`api/profile/${id}`)
    const images = await get(`api/image/profile/${id}`)
    const idCard = await get(`api/image/idcard/${id}`)
    const license = await get(`api/image/license/${id}`)
    const bank = await get(`api/image/bank/${id}`)

    this.props.setPageTitle(single[0].name)
    let edu = await get(`api/education/${uId}`)
    let workexp = await get(`api/experience/${uId}`)
    let skill = await get(`/api/skill/${uId}`)
    let ansQuestion = await get(`/api/question/${uId}`)
    console.log('ssx',single)
  if(single[0].approve_status === 'approved') {
    this.setState({isApproved: true})
  }
    console.log('imagee', skill)
    this.setState({
      dataProfile: {
        uId,
        picture: images.link,
        name: resp.profile?.nickname ?? ' ',
        namepic: single[0].name,
        bday: resp.profile === null ? ' - ' : resp.profile?.birth_day ?? ' ',
        age: resp.profile?.age ?? ' ',
        line_id: resp.profile?.line_id ?? ' ',
        email: single[0].email,
        phone: single[0].phone,
        realAddr:
          resp.address === null
            ? ' - '
            : resp.address?.no +
              ' ' +
              `\n` +
              resp.address?.district +
              ' ' +
              ' ' +
              resp.address?.zone +
              ' ' +
              ' ' +
              resp.address?.province +
              ' ' +
              ' ' +
              `รหัสไบรษณ์ ${resp.address?.postcode}`,
        address:
          resp.addressNow === null
            ? ' - '
            : resp.addressNow?.no_now +
              ' ' +
              resp.addressNow?.district_now +
              ' ' +
              resp.addressNow?.zone_now +
              ' ' +
              resp.addressNow?.province_now +
              ' ' +
              `รหัสไบรษณ์ ${resp.addressNow?.postcode_now}`,
      },
      ansQuestion,
      workLevel: skill.work?.level,
      workexp: workexp,
      workZone: single[0].work,
      edu: edu,
      holidayWork: skill.work?.holiday,
      idCard: idCard,
      license: license,
      bank: bank,
      skill: skill.skill,
    })
  }

  async getDataCalendarTable(start_date, end_date) {
    let { startDate, endDate } = this.state
    if (start_date && end_date) {
      startDate = moment(start_date, 'YYYY-MM-DD').format('YYYY-MM-DD')
      endDate = moment(end_date, 'YYYY-MM-DD').format('YYYY-MM-DD')
    } else {
      startDate = moment(this.state.start_datetime).format('YYYY-MM-DD')
      endDate = moment(this.state.end_datetime).format('YYYY-MM-DD')
    }
    this.setState({ loading: true })
    if (startDate && endDate) {
      if (this.props.location.state !== undefined) {
        const id = this.props.location.state.Maid_ID
        const resp = await get(`admin/maid/id/${id}/details`)
        let tempDate = []
        console.log(resp)
        if (resp.length <= 10) {
          this.setState({ loading: false, PageSize: 10 })
          for (let i = 0; i < resp.length; i++) {
            tempDate.push({
              Mounth: resp[i].title,
              Work_completed: resp[i].no_of_work_completed,
              Hrs_completed: resp[i].no_of_hours_completed,
              salary: resp[i].salary,
              star_bonus: resp[i].five_star_bonus,
              Monthly_bonus: resp[i].monthly_bonus,
              Milestone_bonus: resp[i].milestone_bonus,
            })
          }
        } else {
          for (let i = 0; i < resp.length; i++) {
            this.setState({ loading: false, PageSize: resp.length })
            tempDate.push({
              Mounth: resp[i].title,
              Work_completed: resp[i].no_of_work_completed,
              Hrs_completed: resp[i].no_of_hours_completed,
              salary: resp[i].salary,
              star_bonus: resp[i].five_star_bonus,
              Monthly_bonus: resp[i].monthly_bonus,
              Milestone_bonus: resp[i].milestone_bonus,
            })
          }
        }
        this.setState({ data: tempDate, salary: resp.salary })
      }
    }
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }
  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    const { history } = this.props
    history.push(this.props.history.location.pathname)
  }
  approveHandler = async (cId) => {
    console.log('cId', cId)

    const approveResult = await put('api/approved/user/', { user_id: cId })
    if (approveResult?.message === 'Approve Complete') {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'success',
        text: 'ทำการอนุมัติพนักงาน เรียบร้อย',
        showConfirmButton: false,
        animation: false,
      })

      this.setState({ isApproved: true })
    } else {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'error',
        text: 'ระบบไม่สามารถทำการตรวจสอบได้ในขณะนี้ โปรดติดต่อผู้ดูแล',
        showConfirmButton: false,
        animation: false,
      })
    }
  }
  inValidHandler = async(cId) => {
    const disApproveResult = await put('api/disapproved/user/', { user_id: cId })
    if (disApproveResult?.message === 'Dispprove Complete') {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'success',
        text: 'ไม่อนุมัติผู้พนักงาน เรียบร้อย',
        showConfirmButton: false,
        animation: false,
      })

      this.setState({ isApproved: false })
    } else {
      Swal.fire({
        // title: "ระบบได้ทำการสร้างานใหม่ และส่งข้อมูลถึงคลินิกเรียบร้อยแล้ว",
        icon: 'error',
        text: 'ระบบไม่สามารถทำการตรวจสอบได้ในขณะนี้ โปรดติดต่อผู้ดูแล',
        showConfirmButton: false,
        animation: false,
      })
    }
  }

  render() {
    const currentLocation = this.props.history.location.pathname
    const {
      dataProfile,
      edu,
      workexp,
      idCard,
      skill,
      bank,
      license,
      workZone,
      workLevel,
      ansQuestion,
      holidayWork,
    } = this.state
    return (
      <Container>
        <div className="container-fluid">
          {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
          {/* Row */}
          <div>
            <div className="row ">
              {/* Column */}
              <Card>
                <div
                  className="card h-vh-80 col-md-12"
                  style={{
                    width: '330px',
                    height: 'auto',
                    borderRadius: '6px',
                    border: ' solid 1px #324755',
                    backgroundColor: '#ffffff',
                  }}>
                  <div className="card-body">
                    <center className="m-t-30">
                      <img
                        src={`${dataProfile?.picture}`}
                        alt="profile"
                        className="rounded-circle"
                        width={150}
                      />

                      <h4 className="card-title m-t-10">{dataProfile?.namepic}</h4>
                    </center>
                    <small className="text-muted">ชื่อเล่น </small>
                    <h6>{dataProfile?.name}</h6>
                    <small className="text-muted">อายุ </small>
                    <h6>{dataProfile?.age}</h6>
                    <small className="text-muted">วันเกิด </small>
                    <h6>
                      {dataProfile.bday === ' - ' ? '-' : moment(dataProfile?.bday).format('LL')}
                    </h6>
                    <small className="text-muted">อีเมล์</small>
                    <h6>{dataProfile?.email ? dataProfile?.email : ' '}</h6>
                    <small className="text-muted p-t-30 db">เบอร์โทร</small>
                    <h6>{dataProfile?.phone ?? ' '}</h6>
                    <small className="text-muted p-t-30 db">Line</small>
                    <h6>{dataProfile?.line_id ?? ' '}</h6>
                    <small className="text-muted p-t-30 db">ที่อยู่</small>
                    <h6>{dataProfile.address !== undefined ? dataProfile.address : '-'}</h6>
                  </div>
                </div>
              </Card>
              <div className="col">
                <Card2>
                  <div
                    className="card "
                    style={{
                      width: '100%',
                      height: 'auto',
                      borderRadius: '6px',
                      border: ' solid 1px #324755',
                      backgroundColor: '#ffffff',
                    }}>
                    <div className="card-body">
                      <div className="d-flex">
                        <figure className="figure">
                          <img
                            src={idCard?.link}
                            className="figure-img w-75 img-fluid rounded thumb-post"
                            data-toggle="modal"
                            data-target="#exampleModalCenter1"
                          />
                          <div
                            class="modal fade"
                            id="exampleModalCenter1"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <img src={idCard?.link} className="img-fluid rounded" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <figcaption className="font-12 figure-caption">
                            ใบอนุญาติประกอบวิชาชีพ
                          </figcaption>
                        </figure>
                        <figure className="figure">
                          <img
                            src={license?.link}
                            className="figure-img w-75 img-fluid rounded thumb-post"
                            data-toggle="modal"
                            data-target="#exampleModalCenter2"
                          />
                          <div
                            class="modal fade"
                            id="exampleModalCenter2"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <img src={license?.link} className="img-fluid rounded" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <figcaption className="font-12 figure-caption">
                            ใบอนุญาติประกอบวิชาชีพ
                          </figcaption>
                        </figure>

                        <figure className="figure">
                          <img
                            src={bank?.link}
                            className="figure-img w-75 img-fluid rounded thumb-post"
                            data-toggle="modal"
                            data-target="#exampleModalCenter3"
                          />
                          <div
                            class="modal fade"
                            id="exampleModalCenter3"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <img src={bank?.link} className="img-fluid rounded" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <figcaption className="font-12 figure-caption">
                            หน้าสมุดธนาคารรับเงินค่าจ้าง
                          </figcaption>
                        </figure>
                      </div>
                      {workLevel !== undefined ? (
                        <select disabled className="dropdown-selected" value={workLevel}>
                          <option value="1">Level 1</option>
                          <option value="2">Level 2</option>
                          <option value="3">Level 3</option>
                          <option value="4">Level 4</option>
                        </select>
                      ) : (
                        ''
                      )}

                      <UTEXT>ที่อยู่ตามบัตรประชาชน </UTEXT>
                      <h6>{dataProfile?.realAddr}</h6>
                      <br />
                      <UTEXT>ประวัติการศึกษา </UTEXT>

                      {edu.length !== 0 ? (
                        edu.map((edu) => {
                          return (
                            <div key={edu?.education_id}>
                              <h6>ระดับการศึกษา : {edu?.level_education}</h6>
                              <h6>ชื่อสถาบัน : {edu?.university}</h6>
                              <h6>สาขาวิชา : {edu?.branch}</h6>
                              <h6>ปีที่จบการศึกษา : {edu?.graduation}</h6>
                              <h6>สถานะการศึกษา : {edu?.education_status}</h6>
                              <h6>เกรดเฉลี่ยน : {edu?.gpa}</h6>
                              <br />
                            </div>
                          )
                        })
                      ) : (
                        <div>
                          <h6>ระดับการศึกษา : </h6>
                          <h6>ชื่อสถาบัน :</h6>
                          <h6>สาขาวิชา : </h6>
                          <h6>ปีที่จบการศึกษา : </h6>
                          <h6>สถานะการศึกษา : </h6>
                          <h6>เกรดเฉลี่ยน : </h6>
                          <br />
                        </div>
                      )}
                      <UTEXT>ประสบการทำงาน </UTEXT>
                      {workexp.length !== 0 ? (
                        workexp?.map((workexp) => {
                          return (
                            <div>
                              <h6>ตำแหน่งงาน : {workexp?.old_position}</h6>
                              <h6>ชื่อสถานที่ : {workexp?.company}</h6>
                              <h6>จังหวัด : {workexp?.province}</h6>
                              <h6>วันเริ่มการทำงาน : {moment(workexp?.start_day).format('LL')}</h6>
                              <h6>
                                วันสิ้นสุดการทำงาน : {moment(workexp?.finish_day).format('LL')}
                              </h6>
                              <br />
                            </div>
                          )
                        })
                      ) : (
                        <div>
                          <h6>ตำแหน่งงาน :</h6>
                          <h6>ชื่อสถานที่ :</h6>
                          <h6>จังหวัด :</h6>
                          <h6>วันเริ่มการทำงาน :</h6>
                          <h6>วันสิ้นสุดการทำงาน :</h6>
                          <br />
                        </div>
                      )}
                      {/*<UTEXT>ทักษะเฉพาะทาง หรือความสามารถพิเศษ </UTEXT>*/}
                      {/*<p>-</p>*/}

                      <UTEXT>ทักษะพิเศษของท่าน (5 = มาก , 1 = น้อย) </UTEXT>
                      <Skill>
                        <Div>
                          <h6>มนุษย์สัมพันธ์ดี</h6>
                          <h6>สื่อสารดี, ใช้คำพูดเป็น </h6>
                          <h6>การใช้ภาษาอังกฤษ </h6>
                          <h6>แก้ปัญหาเฉพาะหน้าได้ </h6>
                          <h6>ความรู้วิชาการ</h6>
                          <h6>ละเอียดรอบคอบ </h6>
                        </Div>
                        <Div>
                          <h6 className="ml-lg-4">{skill?.humanReletion}</h6>
                          <h6 className="ml-lg-4">{skill?.communicate}</h6>
                          <h6 className="ml-lg-4">{skill?.languageEng}</h6>
                          <h6 className="ml-lg-4">{skill?.problemSolve}</h6>
                          <h6 className="ml-lg-4">{skill?.knowledge}</h6>
                          <h6 className="ml-lg-4">{skill?.prudence}</h6>
                        </Div>
                      </Skill>
                      <br />
                      <UTEXT>ท่านสนใจทำงานบริเวณใด</UTEXT>
                      <div>
                        <h6>{workZone?.work_area}</h6>
                      </div>
                      <UTEXT>วันหยุดพิเศษ / เทศกาล ท่านสามารถทำงานได้หรือไม่</UTEXT>
                      <h6>
                        ถ้าท่านทำได้ โอกาสได้งานจากคลินิกและโรงพยาบาลสัตว์จะเพิ่มขึ้น
                        เนื่องจากเป็นช่วงที่ต้องการบุคลากรเยอะ
                      </h6>
                      <label>ได้</label>
                      <input type="radio" checked={holidayWork !== 'ไม่ได้'} />
                      <span className="ml-2">
                        <label>ไม่ได้</label>
                        <input type="radio" checked={holidayWork === 'ไม่ได้'} />
                      </span>
                      <UTEXT>
                        สมมติ มีเคสเข้ามารักษาและเกิดเสียชีวิต ท่านจะแจ้งเจ้าของเคสว่าอย่างไร
                        ถ้าเจ้าของเคสเป็นคนค่อนข้างละเอียดและมีอิทธิพลต่อสังคม{' '}
                      </UTEXT>
                      <div>
                        <h6>
                          สมมติ สาเหตุการเสียชีวิตได้ตามประสบการณ์หรือความถนัดในการรักษาได้เลยค่ะ
                          ตอบได้ตั้งแต่ระยะก่อนรักษา ระหว่างรักษา และหลังจากเสียชีวิตแล้ว
                          ว่าควรแจ้งอย่างไร
                        </h6>
                      </div>
                      <div>
                        <h6>- {ansQuestion?.ansQ}</h6>
                      </div>
                      <div>
                        รายละเอียดอื่นๆเพิ่มเติม (ถ้ามี)
                        <div>- {ansQuestion?.noteQ}</div>
                      </div>
                      <div>
                        ข้อเสนอแนะเพิ่มเติม
                        <div>- {ansQuestion?.commentQ}</div>
                      </div>
                    </div>
                  </div>
                </Card2>
                <div className="container btn-float-right">
                  {this.state.isApproved ?
                    <div className="btn green-highlight  btn-margin-lr"
                         onClick={() => this.approveHandler(dataProfile.uId)}>✅ ตรวจสอบแล้ว</div>
                    : <div className="btn purple-highlight  btn-margin-lr"
                           onClick={() => this.approveHandler(dataProfile.uId)}>ตรวจสอบแล้ว</div>
                  }

                  <div className="btn purple-highlight btn-margin-lr" onClick={() => this.inValidHandler(dataProfile.uId)}>ข้อมูลไม่ถูกต้อง</div>
                </div>
              </div>
            </div>
          </div>
          <Rightbar data={this.state} currentLocation={currentLocation} />
          {this.props.children}
        </div>
      </Container>
    )
  }
}

const mapState = (state) => ({
  columnMaidProfile: state.maid.columnMaidProfile,
  selectedCustomer: state.customer.selectedCustomer,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(maidProfile)
