import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import moment from 'moment'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
// import { data } from './CustomerData'
// import EditStatusMenu from '../../components/editStatusMenu'
// import Tooltip from '../../components/tooltip'

import { withCookies } from 'react-cookie'
import { Link } from 'react-router-dom'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`
// const access_token = 'EAAES8YEy1TMBAEeZB8ZA2QfxfobkOgMcwOEX06U9NZANiE5C4soO8DTpvkQA3bcWtid8aiFzCKt8QDpU0qbqovFStHgaJr1hkEcV74oNbmGHXAKlHbfw8ZBzSSCLZAPoaCyQagIRFaWRymximvuMaT9PCf4PH2fJJsAQU0pcJe6PMG5r0X4hQyfVTsugY0mMZD'
// const access_token = 'faskfjsld'
// &access_token=EAAES8YEy1TMBAEeZB8ZA2QfxfobkOgMcwOEX06U9NZANiE5C4soO8DTpvkQA3bcWtid8aiFzCKt8QDpU0qbqovFStHgaJr1hkEcV74oNbmGHXAKlHbfw8ZBzSSCLZAPoaCyQagIRFaWRymximvuMaT9PCf4PH2fJJsAQU0pcJe6PMG5r0X4hQyfVTsugY0mMZD

class MemberCustomerPage extends Component {
  state = {
    column: this.props.customers,
    resAnnouncement: [],
    personal: [],
    line_array: [],
    member_array: [],
    id_array: [],
    level_array: [],
    data: [],
    PageSize: 10,
    numberPage: 1,
    totalPages: 0,
    totaldata: 0,
    Detail: '',
    dataCome: false,
    level_update: false,
  }
  componentDidMount() {
    this.props.setPageTitle('Member management')
    this.handleSetCookies(this.props.customers)

    this.getAnnouncement()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.customerColumn) {
      // console.log('update')

      this.setState({ column: this.props.customerColumn })
    }

    console.log('oolo', this.props.customerColumn)
    // this.handleSetCookies(this.props.customers)
  }
  async getAnnouncement() {
    try {
      let resp = []
      let result = await get('api/alluser')
      console.log('resp', resp)
      result.map((m) => {
        resp.push(m)
      })
      this.setState({
        resAnnouncement: result,
      })
      // }
      this.props.setCustomer(resp)

      console.log('1')
      console.log('data', this.state.resAnnouncement)

      this.getLevel()
      console.log('2')

      // this.getPersonal()
    } catch (error) {
      console.log(error)
    }
  }

  async getLevel() {
    let level = []
    let id_arr = this.state.id_array
    console.log('levelings', this.state.id_array)
    console.log('add level', this.state.resAnnouncement)
    // }
    this.setState({ data: this.state.resAnnouncement })
    console.log('data', this.state.data)

    // let level_arr = this.state.level_array

    console.log('level', this.state.level_array)
  }

  onScroll = (e) => {
    if (e.target.scrollTop) {
      let temph = e.target.scrollHeight
      let tempt = e.target.scrollTop
      let calHT = temph - tempt
      let tempc = e.target.clientHeight
      if (calHT === tempc) {
        this.onFaceDataOnScroll(calHT, tempc)
      }
    }
  }
  async onFaceDataOnScroll(calHT, tempc) {
    if (this.state.numberPage === this.state.totalPages) {
      this.setState({ numberPage: this.state.numberPage + 1 })
    }
    if (calHT === tempc) {
      if (this.state.totalPages > 1 && this.state.data.length < this.state.totaldata) {
        if (this.state.totalPages >= this.state.numberPage) {
          await this.setState({ numberPage: this.state.numberPage + 1, loading: true })
          try {
            get(`admin/customer/list?page=${this.state.numberPage}&limit=10`).then((resp) => {
              if (resp.length >= 0) {
                for (let i = 0; i < resp.length; i++) {
                  this.state.data.push({
                    id: resp[i].id,
                    member: resp[i].member,
                    name: resp[i].name,
                    title: resp[i].title,
                    email: resp[i].email,
                    phone: resp[i].phone,
                    position: resp[i].position,
                    idLine: resp[i].idLine,
                    updatedAt: resp[i].updatedAt,
                  })
                }
                this.setState({
                  loading: false,
                  PageSize: this.state.data.length,
                })
              }
            })
          } catch (error) {
            console.log(error)
          }
        }
      }
    }
  }
  handleSetCookies(list) {
    const { cookies } = this.props
    let result = []
    list.map((obj) => result.push(obj.image))
    cookies.set('picture', result, { path: '/', sameSite: false, secure: true })
    // console.log(cookies.get('picture'))
  }

  render() {
    const allColumns = [
      {
        Header: () => <h6>{''}</h6>,
        accessor: 'picture', // String-based value accessors!
        minWidth: 80,
        Cell: (row) => (
          <div style={{ width: '100%' }}>
            <img
              src={`${row.original.image}`}
              alt="profile3"
              className="rounded-circle"
              width={35}
            />
          </div>
        ),
      },
      {
        Header: () => <h4>ลำดับ</h4>,
        accessor: 'member',
        minWidth: 80,
      },
      {
        Header: () => <h4>ตำแหน่งงาน</h4>,
        accessor: 'title',
        minWidth: 80,
      },
      {
        Header: () => <h4>ชื่อพนักงาน</h4>,
        accessor: 'name',
      },
      {
        Header: () => <h4>อีเมล</h4>,
        accessor: 'email',
        minWidth: 80,
      },
      {
        Header: () => <h4>เบอร์โทร</h4>,
        accessor: 'phone',
        minWidth: 80,
      },
      {
        Header: () => <h4>Level All</h4>,
        accessor: 'level',
        minWidth: 50,
        className: 'levelAll',
        Cell: ({ original }) => {
          // console.log('levels', original.work.level)
          if (original.work !== null) {
            return original.work?.level
          } else {
            return ''
          }
        },
      },
      {
        Header: () => <h4>สถานะการตรวจสอบ<br/>ประวัติและข้อมูลส่วนตัว</h4>,
        accessor: 'approve_status',
        className: 'centerx',
        Cell: ({original}) => {
          let status;
          if(original.approve_status === 'waiting') {
            return <div style={{textAlign: 'center',color: '#e0ab1d',fontWeight:'bold'}}>รอตรวจสอบข้อมูล</div>
          } else if(original.approve_status === 'approved') {
            return <div style={{textAlign: 'center',color: '#347c00', fontWeight:'bold'}}>ตรวจสอบแล้ว</div>
          }
          return <div style={{textAlign: 'center',color: '#bc0000', fontWeight:'bold'}}>ข้อมูลไม่ถูกต้อง</div>
        }

      },
      {
        Header: () => <h4>วัน และ เวลาที่อัพเดตข้อมูลล่าสุด</h4>,
        minWidth: 80,
      },
      {
        Header: () => <h4>ดูรายละเอียด</h4>,
        accessor: 'createdAt',
        width: 200,
        Cell: ({ original }) => {
          this.props.setSelectedCustomer(original)
          // console.log('original', original)
          return (
            <Link to={`/admin/customer-management/profile/employee/${original.member}/${original.user_id}`}>
              ดูรายละเอียด
            </Link>
          )
        },
      },
      // {
      //   Header: () => <h4>ลบผู้ใช้งาน</h4>,
      //   accessor: 'createdAt',
      //   Cell: ({ original }) => (
      //     <Link to={`/profile/${original.user_id}`} >
      //       ลบ
      //     </Link>
      //   )
      // },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })

    return (
      <div>
        <Card>
          <div onScroll={this.onScroll} style={{ width: '100%' }}>
            <ReactTable
              data={this.props.getM} //data object
              columns={columnWillShow} //column config object
              loading={this.state.loading}
              pageSize={this.state.PageSize}
              // minRows={20}
              showPagination={true}
              style={{
                width: 'auto',
                height: '400px',
              }}
              className="-striped -highlight"
            />
          </div>
        </Card>
      </div>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  customerColumn: state.customer.column,
  customerList: state.customers,
  getM: state.Title.maidList
})

const mapDispatch = (dispatch) => {
  return {
    setSelectedCustomer: dispatch.customer.setSelectedCustomer,
    setPageTitle: dispatch.Title.setPageTitle,
    setCustomer: dispatch.Title.setM,
  }
}

export default connect(mapState, mapDispatch)(withCookies(MemberCustomerPage))
