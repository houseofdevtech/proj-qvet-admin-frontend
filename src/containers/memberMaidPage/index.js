import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import moment from 'moment'
// import { data } from './MaidData'
// import EditStatusMenu from '../../components/editStatusMenu'
// import Tooltip from '../../components/tooltip'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

class MemberMaidPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      column: this.props.maidColumn,
      resAnnouncement: [],
      data: [],
      PageSize: 10,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  componentDidMount() {
    this.props.setPageTitle('Member management')
    this.getAnnouncement()
    this.props.showBackIcon(true)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.maidColumn) {
      // console.log('update')
      this.setState({ column: this.props.maidColumn })
    }
    if (this.props.updateSatatusApprove !== undefined || this.props.updateSatatusApprove !== null) {
      if (this.props.updateSatatusApprove !== '') {
        this.props.satatusApprove('')
        this.getAnnouncement()
        this.setState({
          resAnnouncement: [],
          PageSize: 11,
          numberPage: 1,
          totalPages: 0,
          totaldata: 0,
        })
      }
    }
  }
  // src={`${process.env.PUBLIC_URL}/assets/images/users/7.jpg`}
  async getAnnouncement() {
    try {
      let resp = []
      resp = await get('api/allcompany')
      console.log('re', resp)

      this.setState({ data: resp })
      this.props.setCustomerList(resp)
    } catch (error) {
      console.log(error)
    }
  }

  onScroll = (e) => {
    if (e.target.scrollTop) {
      let temph = 0
      let tempt = 0
      temph = e.target.scrollHeight
      tempt = e.target.scrollTop
      let calHT = temph - tempt
      let tempc = e.target.clientHeight
    }
  }
  render() {
    const allColumns = [
      {
        Header: () => <h6>ลำดับ</h6>,
        accessor: 'member',
      },
      {
        Header: () => <h6>ประเภทสถานประกอบการ</h6>,
        accessor: 'position', // String-based value accessors!
        minWidth: 150,
      },
      {
        Header: () => <h6>ชื่อสถานประกอบการ</h6>,
        accessor: 'company_name',
      },
      {
        Header: () => <h6>อีเมล์</h6>,
        accessor: 'email',
      },
      {
        Header: () => <h6>เบอร์โทร</h6>,
        accessor: 'phone',
      },
      {
        Header: () => <h6>วันที่อัพเดตข้อมูลล่าสุด</h6>,
        accessor: 'updatedAt',
        Cell: ({ original }) => {
          return moment(original.updatedAt).format('ll')
        },
      },
      {
        Header: () => (
          <h6>
            สถานะการตรวจสอบ
            <br />
            ประวัติและข้อมูลส่วนตัว
          </h6>
        ),
        accessor: 'approve_status',
        className: 'centerx',
        Cell: ({ original }) => {
          let status
          if (original.approve_status === 'waiting') {
            return (
              <div style={{ textAlign: 'center', color: '#e0ab1d', fontWeight: 'bold' }}>
                รอตรวจสอบข้อมูล
              </div>
            )
          } else if (original.approve_status === 'approved') {
            return (
              <div style={{ textAlign: 'center', color: '#347c00', fontWeight: 'bold' }}>
                ตรวจสอบแล้ว
              </div>
            )
          }
          return (
            <div style={{ textAlign: 'center', color: '#bc0000', fontWeight: 'bold' }}>
              ข้อมูลไม่ถูกต้อง
            </div>
          )
        },
      },

      {
        Header: () => <h6>เวลาที่อัพเดตข้อมูลล่าสุด</h6>,
        accessor: 'Repeat_Rate',
        Cell: ({ original }) => {
          return moment(original.updatedAt).format('LTS') + ' น.'
        },
      },
      {
        Header: () => <h4>ดูรายละเอียด</h4>,
        accessor: 'Start_Date',
        width: 200,
        Cell: ({ original }) => {
          // this.props.setSelectedCustomer(original)
          // window.localStorage.removeItem('seeId')
          // window.localStorage.setItem('seeId',original.member)
          return (
            <Link to={`/admin/customer-management/profile/company/${original.member}`}>
              ดูรายละเอียด
            </Link>
          )
        },
      },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })
    // console.log(`Page Column state: ${JSON.stringify(this.state.column)}`)
    return (
      <div>
        <Card>
          <div onScroll={this.onScroll} style={{ width: '100%' }}>
            <ReactTable
              data={this.props.companyList} //data object
              columns={columnWillShow} //column config object
              loading={this.state.loading}
              pageSize={this.state.PageSize}
              showPagination={true}
              getTrGroupProps={(state, rowInfo) => {
                if (rowInfo !== undefined) {
                  return {
                    onDoubleClick: () => {
                      this.props.history.replace({
                        pathname: '/admin/maid-management/maidProfile',
                        state: rowInfo.original,
                      })
                    },
                  }
                }
              }}
              style={{
                height: '400px',
              }}
              className="-striped -highlight"
            />
          </div>
        </Card>
      </div>
    )
  }
}

const mapState = (state) => ({
  maidColumn: state.maid.column,
  companyList: state.Title.customerList,
  updateSatatusApprove: state.orderManagement.updateSatatusApprove,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    setCustomerList: dispatch.Title.setC,
    satatusApprove: dispatch.orderManagement.satatusApprove,
  }
}

export default connect(mapState, mapDispatch)(MemberMaidPage)
