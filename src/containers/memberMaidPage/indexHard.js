import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'

import { data } from './MaidData'
import EditStatusMenu from '../../components/editStatusMenu'
import Tooltip from '../../components/tooltip'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

class MemberMaidPage extends Component {
  state = {
    column: this.props.maidColumn,
  }

  componentDidMount() {
    this.props.setPageTitle('Member List')
  }

  componentDidUpdate(prevProps,prevState) {
    if (prevState.column !== this.props.maidColumn) {
      console.log('update')
      this.setState({ column: this.props.maidColumn })
    }
  }

  render() {
    const allColumns = [
      {
        Header: () => <h6>{''}</h6>,
        accessor: 'picture', // String-based value accessors!
        minWidth: 50,
        Cell: (row) => (
          <div style={{ width: '100%' }}>
            <img
              src={`${process.env.PUBLIC_URL}/assets/images/users/7.jpg`}
              alt="profile3"
              className="rounded-circle"
              width={35}
            />
          </div>
        ),
      },
      {
        Header: () => <h6>Name</h6>,
        accessor: 'name', // String-based value accessors!
        minWidth: 200,
      },
      {
        Header: () => <h6>Email</h6>,
        accessor: 'email',
        minWidth: 200,
      },
      {
        Header: () => <h6>Phone</h6>,
        accessor: 'phone',
      },
      {
        Header: () => <h6>Status</h6>,
        accessor: 'status',
        Cell: (row) => (
          <div style={{ width: '100%', cursor: 'pointer' }}>
            <Tooltip
              placement="bottom"
              trigger="click"
              tooltip={<EditStatusMenu data={row.original.status} />}>
              {row.original.status}
            </Tooltip>
          </div>
        ),
      },
      {
        Header: () => <h6>Role</h6>,
        accessor: 'role',
        Cell: (row) => (
          <div style={{ width: '100%', cursor: 'pointer' }}>
            <Tooltip
              placement="bottom"
              trigger="click"
              tooltip={<EditStatusMenu role={row.original.role} />}>
              {row.original.role}
            </Tooltip>
          </div>
        ),
      },

      {
        Header: () => <h6>Joining date</h6>,
        accessor: 'joinDate',
      },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && 
        columnWillShow.push(obj)
      }
      return {}
    })
    console.log(`Page Column state: ${JSON.stringify(this.state.column)}`)
    return (
      <div>
        <Card>
          {/* member Maid Page */}
          {/* data={this.props.maids} */}
          <ReactTable
            data={data}
            columns={columnWillShow}
            defaultPageSize={10}
            showPagination={false}
            className=""
          />
        </Card>
      </div>
    )
  }
}

const mapState = (state) => ({
  maids: state.maid.maids,
  maidColumn: state.maid.column,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
  }
}

export default connect(
  mapState,
  mapDispatch
)(MemberMaidPage)
