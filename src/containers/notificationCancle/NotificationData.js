export const headers = [
  'company_name',
  'work_id',
  'date',
  'position',
  'level',
  'workplace',
  'work_time',
]

export const data = [
  {
    company_name: 'บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)',
    work_id: '00100-20010001',
    date: '2020/12/10',
    position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
    level: 'Level 1',
    workplace: ['บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)'],
    work_time: '8.30 - 16.00 น.',
  },
  {
    company_name: 'โรงพยาบาลสัตว์ ท่าพระ',
    work_id: '12401-20010001',
    date: '2020/12/10',
    position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
    level: 'Level 2',
    workplace: ['โรงพยาบาลสัตว์ ท่าพระ'],
    work_time: '8.30 - 16.00 น.',
  },
  {
    company_name: 'บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)',
    work_id: '00103-20010001',
    date: '2020/12/10',
    position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
    level: 'Level 4',
    workplace: ['บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)'],
    work_time: '8.30 - 16.00 น.',
  },
]

export const employee = [
  {
    id: 'MQ-0010',
    job: 'สัตวแพทย์',
    levle: 1,
    emp_id: '1569',
    name: 'น.ส. สมปอง รักสัตว์',
    age: 27,
    email: 'sompong@mail.com',
    tel: '081 234 5678',
    img:
      'https://cdn.zeplin.io/5e6b0d4f596188134eef044a/assets/D5577984-A560-4F1F-9A41-4862E86F80D7.png',
  },
  {
    id: 'MQ-0011',
    job: 'สัตวแพทย์',
    levle: 1,
    emp_id: '1569',
    name: 'ปรมิน เลิศสถิตย์พงษ์',
    age: 30,
    email: 'paramin@mail.com',
    tel: '081 234 5678',
    img:
      'https://cdn.zeplin.io/5e6b0d4f596188134eef044a/assets/C7CD3843-DC33-400E-8F78-CB4E25BD3296.png',
  },
  {
    id: 'MQ-0012',
    job: 'สัตวแพทย์',
    levle: 1,
    emp_id: '1569',
    name: 'ชนาพร ลีลาขจรกิจ',
    age: 29,
    email: 'chanapon@mail.com',
    tel: '081 234 5678',
    img:
      'https://cdn.zeplin.io/5e6b0d4f596188134eef044a/assets/0B7AAD3C-E4B4-4313-8778-30D2A57FD618.png',
  },
]
