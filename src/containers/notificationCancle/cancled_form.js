import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import moment from 'moment'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import { withCookies } from 'react-cookie'
import { Link } from 'react-router-dom'
import '../../style/form-custom.scss'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

// const

class JobNotificationForm extends React.Component {
  state = {
    // column: this.props.customers,
    resAnnouncement: [],
    personal: [],
    line_array: [],
    member_array: [],
    id_array: [],
    level_array: [],
    PageSize: 11,
    numberPage: 1,
    totalPages: 0,
    totaldata: 0,
    Detail: '',
    dataCome: false,
    level_update: false,

    // new state
    id: 'MQ-0010',
    job: 'สัตวแพทย์',
    levle: 1,
    emp_id: '1569',
    name: 'น.ส. สมปอง รักสัตว์',
    age: 27,
    email: 'sompong@mail.com',
    tel: '081 234 5678',
    img:
      'https://cdn.zeplin.io/5e6b0d4f596188134eef044a/assets/D5577984-A560-4F1F-9A41-4862E86F80D7.png',
    data: [],
    work_time: '',
    startDate: new Date(),
    personnel: 0,
    special_price: 0,
    overtime_price: 0,
    other_expenses_price: 0,
    vat_price: 0,
    total: 0,
    current_data: {
      company_name: '',
      work_id: '',
      date: '',
      position: [],
      level: '',
      workplace: [],
      work_time: '',
    },
  }
  componentDidMount() {
    // this.props.setPageTitle('Notification')
    // this.handleSetCookies(this.props.customers)
    // this.getAnnouncement()
    console.log('Form data')
    console.log(this.props.data)
    let data = this.props.data

    this.getNotificationData(data)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.data !== this.props.data) {
      this.getNotificationData(this.props.data)
    }
    // let data = this.props.data

    // this.getNotificationData(data)
    console.log('componentDidUpdate: ', prevProps, prevState, snapshot)
  }

  getNotificationData = (data) => {
    this.setState({
      current_data: data,
      //startDate: moment(data.date).toDate(),
      level: data.level,
      work_time: data.work_time,
    })
  }

  handleChange = (date) => {
    this.setState({
      startDate: date,
    })
  }

  onChange = (e) => {
    const { name, value } = e.target

    this.setState({
      [name]: value,
    })
  }

  changeTopicHandle = (work) => {
    this.setState({
      current_data: work,
      startDate: moment(work.date).toDate(),
      level: work.level,
      work_time: work.work_time,
    })
  }

  render() {
    return (
      <>
        <div
          className="noti-actice row"
          style={{ margin: '0 0 -15px 0', padding: '1em', fontSize: '1rem' }}>
          <div className="col">
            <p>งานด่วน {this.state.id}EX</p>
          </div>
        </div>
        <Card>
          <form action="" class="full-form">
            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  สถานะการประกาศงาน
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value="-"
                  // onChange={this.onChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  รหัสงานใหม่
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value="-"
                  // onChange={this.onChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  ID พนักงานใหม่ที่รับงาน
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value="-"
                  // onChange={this.onChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  ชื่อพนักงานใหม่ที่รับงาน
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value="-"
                  // onChange={this.onChange}
                />
              </div>
            </div>

            <hr />
            <p>ข้อมูลงานที่พนักงานยกเลิก</p>
            <div class="form-group row">
              <div className="col-4">
                <label for="position" className="col-form-label">
                  ตำแหน่งงานที่ต้องการสมัคร
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value={this.state.job}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  ระดับความสามารถ
                </label>
              </div>
              <div className="col">
                <input
                  type="text"
                  id="level"
                  className="form-control"
                  name="level"
                  value={this.state.level}
                  onChange={this.onChange}
                />
              </div>
            </div>

            <div class="form-group row">
              <div className="col-4">
                <label for="location" className="col-form-label">
                  สถานที่ปฏิบัติงาน
                </label>
              </div>
              <div className="col">
                <select class="form-control" id="location" name="location">
                  {this.state.current_data.workplace.map((wplace) => {
                    return <option value={wplace}> {wplace} </option>
                  })}
                </select>
              </div>
            </div>

            <div className="row form-group">
              <div className="col-4"></div>
              <div className="col">
                <Card>
                  <div className="inner-form">
                    <div className="form-group row">
                      <div className="col-5">
                        <label htmlFor="zip-code" className="col-form-label">
                          รหัสไปรษณีย์
                        </label>
                      </div>
                      <div className="col">
                        <select class="form-control" id="zip-code" name="zip-code">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-5">
                        <label htmlFor="province" className="col-form-label">
                          จังหวัด
                        </label>
                      </div>
                      <div className="col">
                        <select class="form-control" id="province" name="province">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-5">
                        <label htmlFor="district" className="col-form-label">
                          อำเภอ
                        </label>
                      </div>
                      <div className="col">
                        <select class="form-control" id="district" name="district">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-5">
                        <label htmlFor="sub-district" className="col-form-label">
                          ตำบล
                        </label>
                      </div>
                      <div className="col">
                        <select class="form-control" id="sub-district" name="sub-district">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </Card>
              </div>
            </div>
            {/* end work place form */}

            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  วันปฏิบัติงาน
                </label>
              </div>
              <div className="col customDatePickerWidth input-group">
                <DatePicker
                  selected={this.state.startDate}
                  onChange={this.handleChange}
                  dateFormat="yyyy-MM-dd"
                  className="form-control"
                  style={{ width: '100%' }}
                />
                <div className="icon-container">
                  <i
                    className="form-control-feedback far fa-calendar-alt input-icon"
                    style={{ color: '#6F8BA1' }}
                  />
                </div>
              </div>
            </div>

            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="work_time" className="col-form-label">
                  เวลาในการทำงาน
                </label>
              </div>
              <div className="col input-group has-feedback">
                <input
                  type="text"
                  id="work_time"
                  className="form-control"
                  name="work_time"
                  value={this.state.work_time}
                  onChange={this.onChange}
                  disabled
                />
                <div className="icon-container">
                  <i className="form-control-feedback fas fa-clock input-icon" />
                </div>
              </div>
            </div>
            {/* end time */}

            <div className="row form-group">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  ข้อมูลงานล่วงเวลา (ถ้ามี)
                </label>
              </div>
              <div className="col">
                <Card>
                  <div className="inner-form">
                    <div className="form-group row">
                      <div className="col-4">
                        <label htmlFor="special" className="col-form-label">
                          ค่าเฉพาะทาง
                        </label>
                      </div>
                      <div className="col">
                        <div className="row mesure-inline">
                          <div className="col">
                            <input
                              type="text"
                              id="special-percent"
                              className="form-control"
                              name="special-percent"
                            />
                          </div>
                          <div className="col-2">
                            <p> % </p>
                          </div>
                        </div>
                        <div className="row mesure-inline">
                          <div className="col">
                            <input
                              type="text"
                              id="special_price"
                              className="form-control"
                              name="special_price"
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="col-2">
                            <p>บาท</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end specialist */}

                    <hr />

                    <div className="form-group row">
                      <div className="col-4">
                        <label htmlFor="overtime" className="col-form-label">
                          OT
                        </label>
                      </div>
                      <div className="col">
                        <div className="row mesure-inline">
                          <div className="col">
                            <input
                              type="text"
                              id="overtime-percent"
                              className="form-control"
                              name="overtime-percent"
                            />
                          </div>
                          <div className="col-2">
                            <p> % </p>
                          </div>
                        </div>
                        <div className="row mesure-inline">
                          <div className="col">
                            <input
                              type="text"
                              id="overtime_price"
                              className="form-control"
                              name="overtime_price"
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="col-2">
                            <p>บาท</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end OT */}
                    <hr />

                    <div className="form-group row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          อื่นๆ ระบุ
                        </label>
                      </div>
                      <div className="col row">
                        <div className="col">
                          <textarea
                            class="form-control"
                            id="ect"
                            name="ect"
                            placeholder="เช่น ค่าเดินทาง"
                          />
                        </div>
                        <div className="col-2"></div>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          จำนวนเงิน
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <input
                            type="text"
                            id="ect-price"
                            className="form-control"
                            name="ect-price"
                          />
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>
                    {/* end ect */}
                  </div>
                </Card>
              </div>
            </div>
            {/* end optional work information */}

            <div className="form-group row">
              <div className="col-4">
                <label htmlFor="personnel" className="col-form-label">
                  จำนวนบุคลากร
                </label>
              </div>

              <div className="col">
                <input
                  type="number"
                  name="personnel"
                  id="personnel"
                  className="form-control"
                  value="1"
                  style={{
                    border: '1px solid #3e5569 !important',
                    textAlign: 'center',
                  }}
                  disabled
                />
              </div>
            </div>
            {/* end personnel */}

            <div className="row form-group">
              <div className="col-4">
                <label htmlFor="level" className="col-form-label">
                  อัตราค่าบริการ / คน
                </label>
              </div>
              <div className="col">
                <Card>
                  <div className="full-form">
                    <div className="row" style={{ marginBottom: '1em' }}>
                      <Card>
                        {' '}
                        <span style={{ textAlign: 'center', width: '100%' }}>2000</span>{' '}
                      </Card>
                    </div>
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          ราคารวม
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>2000</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          ค่าเฉพาะทาง
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>{this.state.special_price}</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          OT
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>{this.state.overtime_price}</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          อื่นๆ
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>{this.state.other_expenses_price}</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          ภาษี 7%
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>{this.state.vat_price}</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>
                    {/* end show all price */}
                    <hr />

                    <div className="form-group row">
                      <div className="col-4">
                        <label htmlFor="ect" className="col-form-label">
                          ราคาสุทธิ
                        </label>
                      </div>
                      <div className="col row mesure-inline">
                        <div className="col">
                          <p>2,140</p>
                        </div>
                        <div className="col-2">
                          <p>บาท</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* end totle price row */}
                </Card>
              </div>
            </div>
            <button className="btn purple-highlight float-right">คัดลองลิงค์งาน</button>
          </form>
          {/* end form */}
        </Card>
        {/* end main right card */}
      </>
    )
  }
}

// const mapState = (state) => ({
//     customers: state.customer.customers,
//     customerColumn: state.customer.column,
//     customerList: state.customers
//   })

//   const mapDispatch = (dispatch) => {
//     return {
//       setSelectedCustomer: dispatch.customer.setSelectedCustomer,
//       setPageTitle: dispatch.Title.setPageTitle,
//       setCustomer: dispatch.customer.setCustomerList
//     }
//   }

// export default connect(mapState, mapDispatch)(withCookies(NotificationPage))
export default JobNotificationForm
