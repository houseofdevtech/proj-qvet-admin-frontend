import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import moment from 'moment'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import { withCookies } from 'react-cookie'
import { Link } from 'react-router-dom'
import '../../style/form-custom.scss'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import { data, employee } from './NotificationData'
import { extend } from 'highcharts'
import JobNotificationForm from './cancled_form'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

class NotificationCancle extends React.Component {
  state = {
    current_data: {
      company_name: '',
      work_id: '',
      date: '',
      position: [],
      level: '',
      workplace: [],
      work_time: '',
    },
    data: [],
    level: '',
    work_time: '',
    startDate: new Date(),
    personnel: 0,
    special_price: 0,
    overtime_price: 0,
    total: 0,
    current_emp: '',
    employee: [],
  }

  componentDidMount() {
    // this.props.setPageTitle('Notification')
    // this.handleSetCookies(this.props.customers)
    // this.getAnnouncement()
    this.getNotificationData()
  }

  getNotificationData = () => {
    this.setState({
      data: data,
      employee: employee,
      current_emp: employee[0].id,
      current_data: data[0],
    })
  }

  handleChange = (date) => {
    this.setState({
      startDate: date,
    })
  }

  onChange = (e) => {
    const { name, value } = e.target

    this.setState({
      [name]: value,
    })
  }

  changeTopicHandle = (work) => {
    // this.setState({
    //   current_data: work,
    //   startDate: moment(work.date).toDate(),
    //   level: work.level,
    //   work_time: work.work_time,
    // })
    this.setState({
      current_emp: work.id,
    })
  }

  personnelHandel = (e) => {
    e.preventDefault()
    let action = e.target.innerText
    switch (action) {
      case '+':
        this.setState({
          personnel: this.state.personnel + 1,
        })
        break
      case '-':
        this.setState({
          personnel: this.state.personnel - 1,
        })
        break
      default:
        break
    }
  }

  render() {
    return (
      <>
        <Card>
          <div onScroll={this.onScroll} style={{ width: '100%', padding: '1em' }}>
            <div className="row">
              <div className="col-4">
                <div className="list-group list-group-card" style={{ padding: 0 }}>
                  {this.state.employee.map((emp) => {
                    return (
                      <button
                        onClick={() => this.changeTopicHandle(emp)}
                        className={`list-group-item list-group-item-action ${
                          emp.id == this.state.current_emp ? 'noti-actice' : ''
                        }`}>
                        <div className="row">
                          <div className="col-4">
                            <img src={emp.img} alt="" style={{ width: '100%' }} />
                          </div>
                          <div className="col">
                            <p style={{ marginTop: 0 }}>รหัส {emp.id}</p>
                            <p>
                              {emp.job} Level {emp.level}
                            </p>
                            <p>{emp.name}</p>
                            <p>อายุ {emp.age}</p>
                            <p>อีเมลล์ {emp.email}</p>
                            <p>เบอร์โทร {emp.tel}</p>
                          </div>
                        </div>
                      </button>
                    )
                  })}
                </div>
              </div>
              <div className="col">
                <JobNotificationForm data={this.state.current_data} />
              </div>
            </div>
          </div>
        </Card>
      </>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  customerColumn: state.customer.column,
  customerList: state.customers,
})
const mapDispatch = (dispatch) => {
  return {
    setSelectedCustomer: dispatch.customer.setSelectedCustomer,
    setPageTitle: dispatch.Title.setPageTitle,
    setCustomer: dispatch.customer.setCustomerList,
  }
}

export default connect(mapState, mapDispatch)(withCookies(NotificationCancle))

// export default NotificationCancle
