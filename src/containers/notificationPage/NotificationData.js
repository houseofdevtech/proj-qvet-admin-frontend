export const headers = [
  'company_name',
  'work_id',
  'date',
  'position',
  'level',
  'workplace',
  'work_time',
]

export const data = [
  // {
  //   company_name: 'บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)',
  //   work_id: '00100-20010001',
  //   date: '2020/12/10',
  //   position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
  //   level: 'Level 1',
  //   workplace: ['บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)'],
  //   work_time: '8.30 - 16.00 น.',
  // },
  // {
  //   company_name: 'โรงพยาบาลสัตว์ ท่าพระ',
  //   work_id: '12401-20010001',
  //   date: '2020/12/10',
  //   position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
  //   level: 'Level 2',
  //   workplace: ['โรงพยาบาลสัตว์ ท่าพระ'],
  //   work_time: '8.30 - 16.00 น.',
  // },
  // {
  //   company_name: 'บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)',
  //   work_id: '00103-20010001',
  //   date: '2020/12/10',
  //   position: ['สัตวแพทย์รุ่นใหม่', 'สัตวแพทย์รุ่นใหม่กว่า', 'สัตวแพทย์รุ่นใหม่ที่สุด'],
  //   level: 'Level 4',
  //   workplace: ['บริษัท มนูญเพ็ทช็อป จำกัด (สำนักงานใหญ่)'],
  //   work_time: '8.30 - 16.00 น.',
  // },
]
