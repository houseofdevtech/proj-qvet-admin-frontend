import React from 'react'
import _ from 'lodash'
import axios from 'axios'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import moment from 'moment'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import { withCookies } from 'react-cookie'
import { Link } from 'react-router-dom'
import '../../style/form-custom.scss'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import JobNotificationForm from './form'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

// const

class NotificationPage extends React.Component {
  constructor(props) {
    super(props)
  }
  state = {
    // column: this.props.customers,
    resAnnouncement: [],
    personal: [],
    line_array: [],
    member_array: [],
    id_array: [],
    level_array: [],
    PageSize: 11,
    numberPage: 1,
    totalPages: 0,
    totaldata: 0,
    Detail: '',
    dataCome: false,
    level_update: false,

    // new state
    current_data: {
      company_name: '',
      work_id: '',
      date: '',
      position: [],
      level: '',
      workplace: [],
      work_time: '',
    },
    emPloyeeByReport: [],
    emPloyeeByReportBackup: [],
    data: [],
    checkCollector: [],
    level: '',
    isCheck: false,
    work_time: '',
    startDate: new Date(),
    personnel: 0,
    special_price: 0,
    overtime_price: 0,
    total: 0,
    searchEmp: '',
  }
  componentDidMount() {
    this.props.setPageTitle('Notification')
    // this.handleSetCookies(this.props.customers)
    // this.getAnnouncement()
    this.getNotificationData()
  }

  getImage = async (id) => {
    let image = await get(`/api/image/profile/${id}`)
    console.log('image', image)
    return image
  }

  getNotificationData = async () => {
    const reportData = await get(`/api/report`).then((res) => res)
    const workType = this.typeCheck(reportData[0].type)
    const getEmployeeByReport = await get(
      `/api/search/employee/all?type=${workType}&date_start=${reportData[0].date_deal_report}`
    )

    // consgetEmployeeByReportt emImage = await get(`/api/image/profile/${}`)
    const x = await Promise.all(
      getEmployeeByReport.map(async (m, i) => {
        // console.log('im', m.member)
        return await get(`/api/image/profile/${m.member}`).then((res) => res)
      })
    )

    x.map((x, i) => (getEmployeeByReport[i]['link'] = x.link))
    this.setState({
      emPloyeeByReport: getEmployeeByReport,
      emPloyeeByReportBackup: getEmployeeByReport,
      data: reportData,
      current_data: reportData[0],
    })

    // console.log(
    //   'emPloyeeByReport',

    //   getEmployeeByReport
    // )
  }

  handleChange = (date) => {
    this.setState({
      startDate: date,
    })
  }

  onChange = (e) => {
    const { name, value } = e.target

    this.setState({
      [name]: value,
    })
  }
  typeCheck = (type) => {
    return type === 'Full time' ? 'full_time' : 'part_time'
  }
  changeTopicHandle = async (work) => {
    const workType = this.typeCheck(work.type)
    const getEmployeeByReport = await get(
      `/api/search/employee/all?type=${workType}&date_start=${work.date_deal_report}`
    )
    const x = await Promise.all(
      getEmployeeByReport.map(async (m, i) => {
        // console.log('im', m.member)
        return await get(`/api/image/profile/${m.member}`).then((res) => res)
      })
    )

    x.map((x, i) => (getEmployeeByReport[i]['link'] = x.link))
    console.log('getEmployeeByReport', getEmployeeByReport)
    this.setState({
      emPloyeeByReport: getEmployeeByReport,
      emPloyeeByReportBackup: getEmployeeByReport,
      current_data: work,
      startDate: moment(work.date).toDate(),
      level: work.level,
      work_time: work.work_time,
    })
  }
  reportStatusHandler = async (e) => {
    e.preventDefault()
    console.log('checkCollector', this.state.checkCollector)
    var data = JSON.stringify({
      user_id: 1,
      job_id: 3,
      date_start: '2020-10-11',
      date_end: '2020-10-11',
      time_start: '08:00',
      time_end: '10:00',
    })

    var config = {
      method: 'post',
      url: 'https://vulcan.houseofdev.tech/qvetjob/api/company/select/job',
      headers: {
        'Content-Type': 'application/json',
        Cookie: '__cfduid=d164bf9d0b36e40c8cbb26d9b5b63d5731596684288',
      },
      data: data,
    }

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data))
      })
      .catch(function (error) {
        console.log(error)
      })

    // console.log('res', res.data)
    const reportData = await get(`/api/report`).then((res) => res)
    // reportData.map((report => {
    //   console.log('date-compare: ', report.date_deal_report, moment(new(Date)));
    // }))
    console.log('this.state.data', this.state.data)
    const newData = _.filter(this.state.data, (el) => {
      return el.status !== 'active'
    })
    this.setState({
      data: newData,
      isCheck: false,
    })
  }

  personnelHandel = (e) => {
    e.preventDefault()
    let action = e.target.innerText
    switch (action) {
      case '+':
        this.setState({
          personnel: this.state.personnel + 1,
        })
        break
      case '-':
        this.setState({
          personnel: this.state.personnel - 1,
        })
        break
      default:
        break
    }
  }

  handleCheck = async (e, em) => {
    const { date_deal_report, job_id } = this.state.current_data
    if (e.target.checked) {
      this.setState((state) => {
        const result = state.checkCollector.concat({
          user_id: em.user_id,
          job_id: job_id,
          date_start: date_deal_report,
          date_end: date_deal_report,
        })
        return {
          checkCollector: result,
          isCheck: true,
        }
      })
    } else {
      const collectorFilter = _.filter(this.state.checkCollector, (el) => {
        return el.user_id !== em.user_id
      })
      this.setState({
        checkCollector: collectorFilter,
        isCheck: false,
      })
    }
  }
  searchEmployeeHandle = (e) => {
    console.log('emp handle : ', e.target.value)
    let searchEmp = e.target.value
    let emPloyeeByReport = this.state.emPloyeeByReportBackup
    console.log('item.includes', emPloyeeByReport)
    emPloyeeByReport = emPloyeeByReport.filter(
      (e) => e.name.includes(searchEmp) || e.member.includes(searchEmp.toUpperCase())
    )
    console.log('item.includes', emPloyeeByReport)
    this.setState({
      searchEmp,
      emPloyeeByReport,
    })
  }

  render() {
    return (
      <div>
        <Card>
          <div onScroll={this.onScroll} style={{ width: '100%', padding: '1em' }}>
            <div className="row">
              <div className="col-4">
                <div className="list-group list-group-card" style={{ padding: 0 }}>
                  {this.state.data.map((work) => {
                    // console.log('subwork', work);
                    return (
                      <button
                        onClick={() => this.changeTopicHandle(work)}
                        className={`list-group-item list-group-item-action ${
                          work.report_job_id === this.state.current_data.report_job_id
                            ? 'noti-actice'
                            : ''
                        }`}>
                        <p>{work.address_com?.company_name}</p>
                        {/* <p>รหัสงาน {work.work_id}</p> */}
                        <p>วันที่ {work.date_deal_report}</p>
                      </button>
                    )
                  })}
                </div>
              </div>
              <div className="col">
                {/* <JobNotificationForm data={this.state.current_data}/> */}
                <Card>
                  <div className="container">
                    <div class="input-group mb-3">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="ค้นหาพนักงาน"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        value={this.state.searchEmp}
                        onChange={this.searchEmployeeHandle}
                        // disabled
                      />
                      {/* <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">
                          Button
                        </button>
                      </div> */}
                    </div>
                  </div>
                  <div className="container" style={{ color: 'black' }}>
                    <form class="full-form">
                      {this.state.emPloyeeByReport.length > 0 &&
                        this.state.emPloyeeByReport.map((em) => (
                          <div
                            class="form-check"
                            style={{ display: 'flex', alignItems: 'center', marginBottom: '1em' }}>
                            <input
                              onChange={(e) => this.handleCheck(e, em)}
                              class="form-check-input"
                              type="checkbox"
                              // checked={this.state.isCheck}
                              id="profile1"
                            />
                            <label
                              class="form-check-label"
                              for="profile1"
                              style={{ width: '100%' }}>
                              <div class="card" style={{ marginBottom: 0 }}>
                                <div class="card-body" style={{ padding: '.5em' }}>
                                  <div className="row">
                                    <div className="col-2">
                                      <img
                                        src={em.link}
                                        alt="profile picture"
                                        className="rounded float-left"
                                        width="100px"
                                      />
                                    </div>
                                    <div className="col" style={{ textAlign: 'left' }}>
                                      <p>รหัส {em.member}</p>
                                      <p>{em.title + ' ' + em.name}</p>
                                      <p sltye={{ marginBottom: '0' }}>
                                        level {em.work.level} : {em.position}
                                      </p>
                                    </div>

                                    <div
                                      className="col"
                                      style={{
                                        textAlign: 'right',
                                        position: 'absolute',
                                        bottom: '1.5em',
                                        left: '-1em',
                                      }}>
                                      <a href="#">ดูรายละเอียด</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </label>
                          </div>
                        ))}
                      <div className="d-flex justify-content-end">
                        <button
                          onClick={(e) => this.reportStatusHandler(e)}
                          class="align-center btn btn-float-right purple-highlight">
                          ส่งถึงพนักงาน
                        </button>
                      </div>
                    </form>
                  </div>
                </Card>
              </div>
            </div>
          </div>
        </Card>
        {/* end big card */}
      </div>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  customerColumn: state.customer.column,
  customerList: state.customers,
})

const mapDispatch = (dispatch) => {
  return {
    setSelectedCustomer: dispatch.customer.setSelectedCustomer,
    setPageTitle: dispatch.Title.setPageTitle,
    setCustomer: dispatch.customer.setCustomerList,
  }
}

export default connect(mapState, mapDispatch)(withCookies(NotificationPage))
// export default NotificationPage
