import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Button from '../../components/button'
import ReactTable from 'react-table'
import Rightbar from '../../layouts/orderManagementLayout/rightbar'
import './orderBookingProfile.css'
import request from '../../utils/request'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

const Container = styled.div`
  padding: 20px;
`

// const Footer = styled.div`
//   position: absolute;
//   left: 10px;
//   bottom: 50px;
// `
// const FooterPicContainer = styled.div`
//   display: flex;
//   padding: 0 0 0 30px;
//   img {
//     margin-right: 10px;
//   }
// `


const Card = styled.div`
      float: left;
      width: 33.333%;
      padding: .75rem;
      margin-bottom: 2rem;
      border: 0;
      `

class ordeBookingPorfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      column: this.props.columnBookingProfile,
      showRightbar: false,
      dataProfile: [],
      maidName: '',
      quotaMaid: '',
      Tempweek: '',
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  componentDidMount() {
    if (this.props.location.state) {
      this.props.setPageTitle(`Order ID #${this.props.location.state.User_ID}`)
    } else {
      this.props.setPageTitle(`Order ID`)
    }
    this.props.showBackIcon(true)
    this.getDataProfile()
    this.getDataDetailTable()
  }

  async getDataProfile() {
    if (this.props.location.state !== undefined) {
      const id = this.props.location.state.User_ID
      try {
        request.get(`/customer/id/${id}`).then(resp => {
            if(resp.data){
              this.setState({
                dataProfile: {
                  picture: resp.data.image_url,
                  name: resp.data.first_name + ' ' + resp.data.last_name,
                  role: resp.data.role,
                  email: resp.data.email,
                  phone: resp.data.phone,
                  userId: resp.data.id,
                  defaultlocation1: resp.data.user_locations?resp.data.user_locations.length > 0 ? resp.data.user_locations[0].secondary_text : '-': '-',
                  defaultlocation2: resp.data.user_locations?resp.data.user_locations.length > 0 ? resp.data.user_locations[1].secondary_text : '-': '-',
                  defaultlocation3: resp.data.user_locations?resp.data.user_locations.length > 2 ? resp.data.user_locations[2].secondary_text : '-': '-',
                },
                quotaMaid: resp.data.change_maid_quota
              })
            }
        })
      } catch (error) {
        console.log(error)
      }
    }
  }

  async getDataDetailTable() {
    if (this.props.location.state !== undefined) {
      console.log(this.props.location.state)
      const id = this.props.location.state.User_ID
      try {
        request.get(`/booking/id/${id}`).then(resp => {
          console.log(resp.data)
              if(resp.data.length>0){
                let tempDate = []
                let tempName = ''
                  let arr = resp.data
                  for (let i = 0; i < arr.length; i++) {
                    tempDate.push({
                    Maid_change :arr[i].maid_change,
                    Maid_ID:arr[i].maid? arr[i].maid.id:null,
                    Maid_Name:arr[i].maid?arr[i].maid.user.first_name+'-'+ arr[i].maid.user.last_name:null,
                    Maid_Phone:arr[i].maid?arr[i].maid.user.phone:null,
                    Credit:arr[i].number,
                    Book_Date:arr[i].start_datetime,
                    Start_Time:arr[i].start_datetime,
                    Credit_Status:arr[i].status,
                    Purchase_Date:arr[i].payment?arr[i].payment.create_datetime:null,
                    Expiration_Date:arr[i].expired_datetime
                   })
                    
                  }
                  tempName = arr[arr.length - 1].maid? arr[arr.length - 1].maid.user.first_name+' '+arr[arr.length - 1].maid.user.last_name:"-"
                  this.setState({ data: tempDate,maidName:tempName})
              }
        })
      } catch (error) {
        console.log(error)
      }
    }
  }


  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }
  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    const { history } = this.props
    history.push(this.props.history.location.pathname)
  }

  render() {
    const allColumns = [
      {
        Header: () => <h6>'Maid Change</h6>,
        accessor: 'Maid_change', // String-based value accessors!
        minWidth: 100,
        paddingLeft: 10
      },
      {
        Header: () => <h6>Maid ID</h6>,
        accessor: 'Maid_ID',
        minWidth: 100,
      },
      {
        Header: () => <h6>Maid Name</h6>,
        accessor: 'Maid_Name',
        minWidth: 100,
      },
      {
        Header: () => <h6>Maid Phone</h6>,
        accessor: 'Maid_Phone',
        minWidth: 100,
      },
      {
        Header: () => <h6>Credit</h6>,
        accessor: 'Credit',
        minWidth: 100,
      },
      {
        Header: () => <h6>Book Date</h6>,
        accessor: 'Book_Date',
        minWidth: 100,
      },
      {
        Header: () => <h6>Start Time</h6>,
        accessor: 'Start_Time',
        minWidth: 100,
      },
      {
        Header: () => <h6>Credit Status</h6>,
        accessor: 'Credit_Status',
        minWidth: 100,
      },
      {
        Header: () => <h6>Purchase Date</h6>,
        accessor: 'Purchase_Date',
        minWidth: 100,
      },
      {
        Header: () => <h6>Expiration Date</h6>,
        accessor: 'Expiration_Date',
        minWidth: 100,
      }
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor &&
          columnWillShow.push(obj)
      }
      return {}
    })
    const currentLocation = this.props.history.location.pathname
    const { dataProfile, maidName, quotaMaid } = this.state
    return (
      <Container>
        <div className="container-fluid">
          {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
          {/* Row */}
          <div >
            <div className="row ">
              {/* Column */}
              <Card>
                <div className="card h-vh-80 col-md-12" style={{ width: '330px', height: '620px', borderRadius: '6px', border: ' solid 1px #324755', backgroundColor: '#ffffff' }}>
                  <div className="card-body">
                    <center className="m-t-30">
                      <img
                        src={`${dataProfile.picture}`}
                        alt="profile"
                        className="rounded-circle"
                        width={150}
                      />

                      <h4 className="card-title m-t-10">{dataProfile.picnameture}</h4>
                      <h6>{dataProfile.name}</h6>
                    </center>
                    <div className="row">
                      <label><strong className="text-muted">User Id : </strong> {dataProfile.userId}</label>
                    </div>
                    <div className="row">
                      <label><strong className="text-muted">Role : </strong> {dataProfile.role}</label>
                    </div>
                    <div className="row">
                      <label><strong className="text-muted">FB Email : </strong> {dataProfile.email}</label>
                    </div>
                    <div className="row">
                      <label><strong className="text-muted">Phone : </strong> {dataProfile.phone}</label>
                    </div>
                    <div className="row">
                      <label>  <strong className="row text-muted p-t-30 db">Default location 1 :</strong> {dataProfile.defaultlocation1}</label>
                    </div>
                    <div className="row">
                      <label>  <strong className="row text-muted p-t-30 db">Default location 2 :</strong> {dataProfile.defaultlocation2}</label>
                    </div>
                    <div className="row">
                      <label>  <strong className="row text-muted p-t-30 db">Default location 3 :</strong> {dataProfile.defaultlocation3}</label>
                    </div>
                  </div>
                </div>
              </Card>
              <div className="col-md-8">
                {/*<div className="btn-container">*/}
                {/*  <Button className="btn" style={{ borderRadius: "11px" }} label="Column setting" onClick={() => this.handleColumn()} />*/}
                {/*  <Button className="btn" style={{ borderRadius: "11px" }} label="Filter by" onClick={() => this.handleFilter()} />*/}
                {/*</div>*/}
                <div className="col-xs-12 col-sm-10 col-md-10 col-xl-8" style={{ lineHeight: "60px" }}>
                  <div className="row form-group">
                    <div className="col-md-2" >
                      <label style={{ fontSize: "18px", fontWeight: "500px" }}>MAID</label>
                    </div>
                    <div className="col-md-8">
                      <input className="OrderFilterInputMaid"
                        type="text" name="maidName" value={maidName} disabled />
                    </div>
                  </div>
                </div>
                <div className="col-xs-12 col-sm-10 col-md-10 col-xl-8" style={{ lineHeight: "40px" }}>
                  <div className="row form-group">
                    <div className="col-md-4" >
                      <label style={{ fontSize: "18px", fontWeight: "500px" }}>Quota of change maid</label>
                    </div>
                    <div className="col-md-6">
                      <input className="OrderFilterInput"
                        type="text" name="quotaMaid" value={quotaMaid} disabled />
                    </div>
                  </div>

                </div>
                <div className="card h-vh-80 OderReactTable">
                  <ReactTable
                    data={this.state.data} //data object
                    columns={columnWillShow}  //column config object
                    loading={this.state.loading}
                    pageSize={this.state.PageSize}
                    showPagination={false}
                    style={{
                      height: "400px"
                    }}
                    className="-striped -highlight"
                  />

                </div>
              </div>
            </div>
          </div>
          <Rightbar data={this.state} currentLocation={currentLocation} />
          {this.props.children}
        </div>
      </Container>
    )
  }
}

const mapState = (state) => ({
  columnBookingProfile: state.orderManagement.columnBookingProfile,

})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(
  mapState,
  mapDispatch
)(ordeBookingPorfile)
