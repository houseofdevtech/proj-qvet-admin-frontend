// export const headers = ['Production Name', 'Id', 'Code', 'Create BY', 'Start Date', 'End Date']

export const data = [
  {
    id:1,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Approved',
    value: 100,
  },
  {
    id:2,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Unapproved',
    value: 100,
  },
  {
    id:3,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Unapproved',
    value: 100,
  },
  {
    id:4,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Banned',
    value: 100,
  },
  {
    id:5,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Approved',
    value: 50,
  },
  {
    id:6,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Approved',
    value: 50,
  },
  {
    id:7,
    Name_maid: 'Name of maid',
    Phone_number: '+123 456 789',
    Register_date: '06/09/2007',
    Training_com_date: '06/09/2007',
    Status:'Approved',
    value: 50,
  },
]
