import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import moment from 'moment'
import _ from 'lodash'
import { Link } from 'react-router-dom'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

// const Icon = styled.div`
//   align-self: center;
//   img {
//   }
// `
const TextCollum = styled.div`
  margin-bottom: 0.5rem;
  font-weight: 500;
  line-height: 1.2;
  color: inherit;
`
const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
`
const Center = styled.div`
  display: flex;
  justify-content: center;
`

class OrderManagementApprove extends Component {
  _isMounted = false

  constructor(props) {
    super(props)
    this.state = {
      column: this.props.columnOrderManagementApprove,
      data: [],
      partTimeUser: [],
      originalPartTime: [],
      start_dateOrder: '',
      end_dateOrder: '',
      error: false,
      paramsDate: {},
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.start_dateOrder !== undefined &&
      prevProps.start_dateOrder !== this.props.start_dateOrder
    ) {
      if (this.props.start_dateOrder !== '') {
        // this.setState({ start_dateOrder: this.props.start_dateOrder })
        this.getDataActive()
        let old_data = this.state.originalPartTime
        let comparator = (this.props.start_dateOrder + '').split('-') //yyyy-MM-DD
        let new_data = old_data.filter((data) => {
          if (data.WorkTime.split(',')[0].split('-')[1] === comparator[1]) {
            return data
          }
        })
        this.setState({
          partTimeUser: new_data,
        })
        // console.log('data', new_data)
      }
    }
    if (
      this.props.end_dateOrder !== undefined &&
      prevProps.end_dateOrder !== this.props.end_dateOrder
    ) {
      if (this.props.end_dateOrder !== '') {
        // this.setState({ end_dateOrder: this.props.end_dateOrder })
        this.getDataActive()
      }
    }
    if (this.props.updateSatatusApprove !== undefined || this.props.updateSatatusApprove !== null) {
      if (this.props.updateSatatusApprove !== '') {
        this.props.satatusApprove('')
        this.getDataActive()
        this.setState({
          PageSize: 11,
          numberPage: 1,
          totalPages: 0,
          totaldata: 0,
        })
      }
    }

    if (prevState.column !== this.props.columnOrderManagementApprove) {
      this.setState({ column: this.props.columnOrderManagementApprove })
    }
  }

  async componentDidMount() {
    this.props.setPageTitle('Receipt')
    // await this.setDateForFectData()
    this.getDealJobList()
    this.props.setOrderMode('Approve')
  }
  currencyFormat = (money) => {
    return Intl.NumberFormat('th-TH', { currency: 'THB' }).format(money)
  }

  getDealJobList = async () => {
    const res = await get(`/api/dealjob/all?page=1&limit=5`).then((res) => res)
    // this.setState({partTimeUser: res.deal_job_part_time})
    const x = _.union(res.deal_job, res.deal_job_part_time)
    console.log('resxx', res)
    let partTimeUser = []
    let EmployerData = []
    let money = 0
    let moneyVat = 0
    let moneyVatTotal = 0
    let moneyVx = 0
    // console.log('xxxb', x)
    let index = 0
    res.done_job.map((user) => {
      if (user !== null) {
        user.jobs.map((job) => {
          partTimeUser.push({
            Order_ID: index + 1,
            position: user.position,
            level: job.level,
            pricing_person: job.pricing_job.pricing_vat - job.pricing_job.pricing + ' บาท',
            JobType: job.type,
            WorkTime:
              moment(job.date_time_job.date_start).format('YYYY/MM/DD') +
              ' - ' +
              moment(job.date_time_job.date_end).format('YYYY/MM/DD'),
            amountDay: 1 + ' เดือน',
            emp_amount: job.amount + ' คน',
            moneyPerson: job.pricing_job.pricing,
            EmployerName: user.company_name,
            moneyVat: job.pricing_job.pricing_vat,
          })
          EmployerData.push({
            ลำดับ: index + 1,
            ชื่อผู้ประกอบการ: user.company_name,
            จำนวนวัน: job.amount + ' เดือน',
            จำนวนเงิน: this.currencyFormat(job.pricing_job.pricing) + ' บาท',
            วันที่ปฏิบัติงาน:
              moment(job.date_time_job.date_start).format('YYYY/MM/DD') +
              ' - ' +
              moment(job.date_time_job.date_end).format('YYYY/MM/DD'),
            จำนวนผู้ปฏิบัติงาน: job.amount + ' คน',
            ภาษีมูลค่าเพิ่ม:
              this.currencyFormat(job.pricing_job.pricing_vat - job.pricing_job.pricing) + ' บาท',
            จำนวนเงินรวมภาษีมูลค่าเพิ่ม: this.currencyFormat(job.pricing_job.pricing_vat) + ' บาท',
          })
          money += job.pricing_job.pricing
          moneyVat += job.pricing_job.pricing_vat
          moneyVx += job.pricing_job.pricing_vat - job.pricing_job.pricing
          index++
        })
      }
    })

    res.done_job_part_time.map((user) => {
      if (user !== null) {
        user.jobs.map((job) => {
          partTimeUser.push({
            Order_ID: index + 1,
            position: user.position,
            level: job.level,
            pricing_person: job.pricing_job.pricing_vat - job.pricing_job.pricing + ' บาท',
            JobType: job.type,
            WorkTime: job.period_jobs.reduce((worktime, p_job) => {
              return (worktime += moment(p_job.date_start).format('YYYY/MM/DD') + ',\n')
            }, ''),
            emp_amount: job.amount + ' คน',
            amountDay: job.period_jobs.length + ' วัน',
            moneyPerson: job.pricing_job.pricing,
            EmployerName: user.company_name,
            moneyVat: job.pricing_job.pricing_vat,
          })
          EmployerData.push({
            ลำดับ: index + 1,
            ชื่อผู้ประกอบการ: user.company_name,
            จำนวนวัน: job.amount + ' วัน',
            จำนวนเงิน: this.currencyFormat(job.pricing_job.pricing) + ' บาท',
            ภาษีมูลค่าเพิ่ม:
              this.currencyFormat(job.pricing_job.pricing_vat - job.pricing_job.pricing) + ' บาท',
            วันที่ปฏิบัติงาน: job.period_jobs.reduce((worktime, p_job) => {
              return (worktime += p_job.date_start + ',\n')
            }, ''),
            จำนวนผู้ปฏิบัติงาน: job.amount + ' คน',
            จำนวนเงินรวมภาษีมูลค่าเพิ่ม: this.currencyFormat(job.pricing_job.pricing_vat) + ' บาท',
          })
          money += job.pricing_job.pricing
          moneyVat += job.pricing_job.pricing_vat
          moneyVx += job.pricing_job.pricing_vat - job.pricing_job.pricing
          index++
        })
      }
    })

    const moneyV = {
      money,
      moneyVat,
      moneyVx,
    }
    // partTimeUser = _.orderBy(partTimeUser, ['EmployerName'])
    this.props.setEmployee(EmployerData)
    this.props.setCompanyMoney(moneyV)
    this.setState({ partTimeUser, originalPartTime: partTimeUser })

    console.log('xxr', partTimeUser)
  }

  async setDateForFectData() {
    const d = new Date()
    const n = d.getDate()
    if (n <= 15) {
      this.state.paramsDate = await {
        start_date: moment().set('date', 1).format('YYYY-MM-DD'),
        end_date: moment().set('date', 15).format('YYYY-MM-DD'),
      }
    } else if (n >= 16) {
      this.state.paramsDate = await {
        start_date: moment().set('date', 16).format('YYYY-MM-DD'),
        end_date: moment(d).endOf('month').format('YYYY-MM-DD'),
      }
    }
    if (this.state.paramsDate.start_date && this.state.paramsDate.end_date) {
      await this.props.setStartdateOrder('')
      await this.props.setEndateOrder('')
      await this.getDataActive()
    }
  }

  async getDataActive() {
    this._isMounted = true
    if (this._isMounted === true) {
      if (this.props.start_dateOrder !== '' || this.props.end_dateOrder !== '') {
        if (this.props.start_dateOrder !== undefined || this.props.end_dateOrder !== undefined) {
          const params = {
            start_date: this.props.start_dateOrder
              ? moment(this.props.start_dateOrder).format('YYYY-MM-DD')
              : moment(this.state.paramsDate.start_date).format('YYYY-MM-DD'),
            end_date: this.props.end_dateOrder
              ? moment(this.props.end_dateOrder).format('YYYY-MM-DD')
              : moment(this.state.paramsDate.end_date).format('YYYY-MM-DD'),
          }
          this.setState({ loading: true })
          this.props.setStartdateOrder(params.start_date)
          this.props.setEndateOrder(params.end_date)
          if (params) {
            await this.props.getOrderApproveList({
              start_date: params.start_date,
              end_date: params.end_date,
            })
            try {
              await get(
                `admin/maid/list?start_date=${params.start_date}&end_date=${params.end_date}`
              ).then((resp) => {
                let tempData = []
                if (resp.maids.length > 0) {
                  for (let i = 0; i < resp.maids.length; i++) {
                    tempData.push({
                      picture: resp.maids[i].user.image_url,
                      Name_maid: resp.maids[i].user.first_name + ' ' + resp.maids[i].user.last_name,
                      id: resp.maids[i].id,
                      Phone_number: resp.maids[i].user.phone,
                      Register_date: resp.maids[i].create_datetime,
                      Training_com_date: resp.maids[i].traning_completed_datetime,
                      Status: resp.maids[i].status,
                    })
                  }
                  this.setState({
                    data: tempData,
                    loading: false,
                    totalPages: resp.page_count,
                    totaldata: resp.item_count,
                  })
                } else {
                  this.setState({ loading: false, PageSize: this.partTimeUser.data.length })
                }
              })
            } catch (error) {
              console.log(error)
            }
          }
        }
      } else {
        this.getDefaultDate()
      }
    }
  }

  async getDefaultDate() {
    try {
      await get(
        `admin/maid/list?start_date=${this.state.paramsDate.start_date}&end_date=${this.state.paramsDate.end_date}`
      ).then((resp) => {
        let tempData = []
        if (resp.maids.length > 0) {
          for (let i = 0; i < resp.maids.length; i++) {
            tempData.push({
              picture: resp.maids[i].user.image_url,
              Name_maid: resp.maids[i].user.first_name + ' ' + resp.maids[i].user.last_name,
              id: resp.maids[i].id,
              Phone_number: resp.maids[i].user.phone,
              Register_date: resp.maids[i].create_datetime,
              Training_com_date: resp.maids[i].traning_completed_datetime,
              Status: resp.maids[i].status,
            })
          }
          this.setState({
            data: tempData,
            loading: false,
            totalPages: resp.page_count,
            totaldata: resp.item_count,
          })
        } else {
          this.setState({ loading: false, PageSize: this.state.data.length })
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  onScroll = (e) => {
    if (this.state.numberPage === this.state.totalPages) {
      this.setState({ numberPage: this.state.numberPage + 1 })
    }
    if (this.state.numberPage <= this.state.totalPages) {
      if (
        e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight &&
        e.target.scrollTop
      ) {
        if (this.state.totalPages > 1) {
          let num = this.state.numberPage
          this.setState({ numberPage: num + 1 })
          if (this.state.data.length < this.state.totaldata && this.state.numberPage > 1) {
            this.setState({ loading: true })
            if (
              this.props.start_dateOrder !== '' &&
              this.props.end_dateOrder !== '' &&
              this.props.start_dateOrder !== undefined &&
              this.props.end_dateOrder !== undefined
            ) {
              const params = {
                start_date: moment(this.props.start_dateOrder).format('YYYY-MM-DD'),
                end_date: moment(this.props.end_dateOrder).format('YYYY-MM-DD'),
              }
              if (params) {
                this.props.getOrderApproveList({
                  start_date: params.start_date,
                  end_date: params.end_date,
                })
                try {
                  get(
                    `admin/maid/list?start_date=${params.start_date}&end_date=${params.end_date}&page=${this.state.numberPage}&limit=10`
                  ).then((resp) => {
                    if (resp.maids.length > 0) {
                      for (let i = 0; i < resp.maids.length; i++) {
                        this.state.data.push({
                          picture: resp.maids[i].user.image_url,
                          Name_maid:
                            resp.maids[i].user.first_name + ' ' + resp.maids[i].user.last_name,
                          id: resp.maids[i].id,
                          Phone_number: resp.maids[i].user.phone,
                          Register_date: resp.maids[i].create_datetime,
                          Training_com_date: resp.maids[i].traning_completed_datetime,
                          Status: resp.maids[i].status,
                        })
                      }
                      this.setState({
                        loading: false,
                        PageSize: this.state.data.length,
                      })
                    } else {
                      this.setState({
                        loading: false,
                        PageSize: this.state.data.length,
                      })
                    }
                  })
                } catch (error) {
                  console.log(error)
                }
              }
            } else {
              try {
                get(
                  `admin/maid/list?start_date=${this.state.paramsDate.start_date}&end_date=${this.state.paramsDate.end_date}&page=${this.state.numberPage}&limit=10`
                ).then((resp) => {
                  if (resp.maids.length > 0) {
                    for (let i = 0; i < resp.maids.length; i++) {
                      this.state.data.push({
                        picture: resp.maids[i].user.image_url,
                        Name_maid:
                          resp.maids[i].user.first_name + ' ' + resp.maids[i].user.last_name,
                        id: resp.maids[i].id,
                        Phone_number: resp.maids[i].user.phone,
                        Register_date: resp.maids[i].create_datetime,
                        Training_com_date: resp.maids[i].traning_completed_datetime,
                        Status: resp.maids[i].status,
                      })
                    }
                    this.setState({
                      loading: false,
                      PageSize: this.state.data.length,
                    })
                  } else {
                    this.setState({
                      loading: false,
                      PageSize: this.state.data.length,
                    })
                  }
                })
              } catch (error) {
                console.log(error)
              }
            }
          }
        }
      }
    }
  }

  render() {
    const allColumns = [
      {
        Header: () => <TextCollum>ลำดับ </TextCollum>,
        accessor: 'Order_ID',
      },
      // {
      //   Header: () => <TextCollum>เลขอ้างอิง</TextCollum>,
      //   accessor: 'Payment_date',
      // },
      // {
      //   Header: () => <TextCollum>รหัสงาน</TextCollum>,
      //   accessor: 'jobCode',
      // },
      {
        Header: () => <TextCollum>ชื่อผู้ประกอบการ</TextCollum>,
        accessor: 'EmployerName',
      },
      {
        Header: () => <TextCollum>วันที่ปฏิบัติงาน</TextCollum>,
        accessor: 'WorkTime',
        style: { 'white-space': 'pre' },
        maxWidth: 150,
        Cell: (row) => <Div>{row.original.WorkTime}</Div>,
      },
      {
        Header: () => <TextCollum>จำนวนวัน</TextCollum>,
        accessor: 'amountDay',
        Cell: (row) => <Center>{row.original.amountDay}</Center>,
      },
      {
        Header: () => <TextCollum>จำนวนผู้ปฏิบัติงาน</TextCollum>,
        accessor: 'empAmount',
        Cell: (row) => <Center>{row.original.emp_amount}</Center>,
      },
      {
        Header: () => <TextCollum>จำนวนเงิน</TextCollum>,
        accessor: 'moneyPerson',
        Cell: (row) => {
          return (
            <Center>
              {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(row.original.moneyPerson)} บาท
            </Center>
          )
        },
      },

      {
        Header: () => <TextCollum>ภาษีมูลค่าเพิ่ม 7%</TextCollum>,
        accessor: 'moneyVat',
        Cell: (row) => {
          return (
            <Center>
              {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                (row.original.moneyPerson * 7) / 100
              )}{' '}
              บาท
            </Center>
          )
        },
      },
      {
        Header: () => <TextCollum>จำนวนเงินรวมภาษีมูลค่าเพิ่ม 7%</TextCollum>,
        accessor: 'moneyVatTotal',
        Cell: (row) => {
          console.log('row ', row.original)
          let total = row.original.moneyVat
          this.props.setCompanyMoneyVatTotal(total)
          return (
            <Center>{Intl.NumberFormat('th-TH', { currency: 'THB' }).format(total)} บาท</Center>
          )
        },
      },

      // {
      //   Header: () => <TextCollum>สถานะเอกสาร</TextCollum>,
      //   accessor: 'listConfirm',
      //   Cell: (row) => (
      //     <Center>
      //       {row.original.listConfirm ?
      //         <button className="btn border border-dark text-success dropdown-toggle" type="button"
      //                 id="dropdownMenuButton"
      //                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      //           วางบิลแล้ว
      //         </button> : <button className="btn border border-dark text-danger dropdown-toggle" type="button"
      //                             id="dropdownMenuButton"
      //                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      //           รอดำเนินการ
      //         </button>}
      //     </Center>
      //   ),
      // },
      {
        Header: () => <TextCollum>ดูรายละเอียด</TextCollum>,
        accessor: 'Details',
        Cell: (row) => (
          <Center>
            {/* <Link to={`/admin/order-management/list/profile/${row.original.EmployerName}`}> */}
            <svg
              width="1em"
              height="1em"
              viewBox="0 0 16 16"
              className="bi bi-person-fill"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fill-rule="evenodd"
                d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
              />
            </svg>
            {/* </Link> */}
          </Center>
        ),
      },
      // {
      //   Header: () => <TextCollum>สถานะการชำระเงิน</TextCollum>,
      //   accessor: 'paymentStatus',
      //   minWidth: 150,
      //   Cell: (row) => (
      //     <div className="mr-4 dropdown">
      //       {row.original.paymentStatus ?
      //         <button className="btn border border-dark text-success dropdown-toggle" type="button"
      //                 id="dropdownMenuButton"
      //                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      //           ชำระเงินแล้ว
      //         </button> : <button className="btn border border-dark text-danger dropdown-toggle" type="button"
      //                             id="dropdownMenuButton"
      //                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      //           ยังไม่ได้ชำระเงิน
      //         </button>}

      //     </div>
      //   ),
      // },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })

    return (
      <div>
        <Card>
          {this.state.partTimeUser.length >= 0 ? (
            <div onScroll={this.onScroll} style={{ width: '100%' }}>
              <ReactTable
                data={this.state.partTimeUser}
                columns={columnWillShow}
                loading={false}
                pageSize={10}
                showPagination={true}
                // style={{ height: '400px' }}
                className="-striped -highlight"
              />
            </div>
          ) : null}
        </Card>
        {this.props.children}
      </div>
    )
  }
}

const mapState = (state) => ({
  start_dateOrder: state.orderManagement.start_dateOrder,
  end_dateOrder: state.orderManagement.end_dateOrder,
  columnOrderManagementApprove: state.orderManagement.columnOrderManagementApprove,
  updateSatatusApprove: state.orderManagement.updateSatatusApprove,
})

const mapDispatch = (dispatch) => {
  return {
    setEmployee: dispatch.orderManagement.setEmployee,
    setCompanyMoneyVatTotal: dispatch.orderManagement.setCompanyMoneyVatTotal,
    setCompanyMoney: dispatch.orderManagement.setCompanyMoney,
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    satatusApprove: dispatch.orderManagement.satatusApprove,
    getOrderApproveList: dispatch.orderManagement.getOrderApproveList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(OrderManagementApprove)
