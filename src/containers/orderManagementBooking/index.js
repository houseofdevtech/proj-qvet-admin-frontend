import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import _ from 'lodash'
import { CSVLink, CSVDownload } from 'react-csv'
import moment from 'moment'
import { get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import EditOrderBooking from '../../components/editOrderBooking/index'
import Tooltip from '../../components/tooltip'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`
const TextCollum = styled.div`
  margin-bottom: 0.5rem;
  font-weight: 500;
  line-height: 1.2;
  color: inherit;
`
const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
`
const Center = styled.div`
  display: flex;
  justify-content: center;
`
class OrderManagementBooking extends Component {
  constructor(props) {
    super(props)
    this.state = {
      column: this.props.columnOrderManagementBooking,
      data: [],
      firstMonday: null,
      partTimeUser: [],
      start_dateOrder: '',
      end_dateOrder: '',
      start_dateOrder: moment(new Date()).day(-7).weekday(1).toDate(),
      end_dateOrder: moment(new Date()).day(-7).weekday(7).toDate(),
      paramsDate: {},
      diffDay: 0,
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  async componentDidMount() {
    this.props.setPageTitle('Payment')
    this.props.setOrderMode('Payment')
    this.getDealJobList()
  }
  componentDidUpdate(prevProps, prevState) {
    // console.log('date:', this.props.start_dateOrder)
    if (
      this.props.start_dateOrder !== undefined &&
      prevProps.start_dateOrder !== this.props.start_dateOrder
    ) {
      if (this.props.start_dateOrder !== '') {
        // this.setState({ start_dateOrder: this.props.start_dateOrder })
        this.setState({
          start_dateOrder: this.props.start_dateOrder,
          end_dateOrder: this.props.end_dateOrder,
        })
        console.log('data', this.state.partTimeUser)
        this.getDealJobList()
      }
    }
  }

  swMoneyPet = (level) => {
    switch (level) {
      case 1:
        return 25000
        break
      case 2:
        return 25000
        break
      case 3:
        return 25000
        break
      case 4:
        return 25000
        break
      case 5:
        return 12000
        break
      case 6:
        return 14000
        break
      default:
        break
    }
  }

  swMoney = (level) => {
    switch (level) {
      case 1:
        return 23000
        break
      case 2:
        return 28000
        break
      case 3:
        return 33000
        break
      case 4:
        return 38000
        break
      case 5:
        return 12000
        break
      case 6:
        return 14000
        break
      default:
        break
    }
  }

  currencyFormat = (money) => {
    return Intl.NumberFormat('th-TH', { currency: 'THB' }).format(money)
  }

  getDeaJobFullTime = (jobFullTime) => {
    let temp = []
    jobFullTime.map((m) => {
      temp.push(m.job.date_time_job.date_start)
      temp.push(m.job.date_time_job.date_end)
    })
    let a = moment(temp[0])
    console.log('xx', a)
    let b = moment(temp[1])
    console.log('xx', b.diff(a, 'days'))
    this.setState({ diffDay: b.diff(a, 'days') + 1 })
    return temp.join(',')
  }

  getDealJobList = async () => {
    const res = await get(`/api/donejob/all?page=1&limit=10`).then((res) => res)
    // this.setState({partTimeUser: res.deal_job_part_time})
    console.log('resx', res)
    let partTimeUser = []
    let exportCSV = []
    let EmMoney = 0
    let score = []

    res.full_time.map((user, index, origin) => {
      const resultScore = _(user.user_assessments)
        .groupBy('company_id')
        .map((v, i) => ({
          company_id: i,
          score: v,
        }))
        .value()

      resultScore.map((m) => {
        score.push(
          m.score.map((x) => {
            return x.score
          })
        )
      })

      if (user !== null) {
        let timeIndicator = this.getDeaJobFullTime(user.deal_jobs).split(',')
        // console.log();
        let timeCondition = moment(timeIndicator[0]).isSameOrAfter(moment(this.state.start_dateOrder)) &&
          moment(timeIndicator[1]).isSameOrBefore(moment(this.state.end_dateOrder))
        if (timeCondition) {
          partTimeUser.push({
            Order_ID: index + 1,
            name: user.name,
            position: user.deal_jobs[0].job.position,
            level: user.deal_jobs[0].job.level,
            EmployerName: user.deal_jobs[0].job.company?.company_name,
            pricing_person:
              this.currencyFormat(
                user.deal_jobs[0].job.company.position === 'Pet shop'
                  ? this.swMoneyPet(user.deal_jobs[0].job.level)
                  : this.swMoney(user.deal_jobs[0].job.level)
              ) + ' บาท',
            JobType: user.deal_jobs[0].job.company.position,
            WorkTime: this.getDeaJobFullTime(user.deal_jobs),
            amountDay: this.state.diffDay,
            score: score[index],
            note: user.deal_jobs[0].job.add_on_extra?.note,
            moneyPerson: this.currencyFormat(user.deal_jobs[0].done_job.total_price) + ' บาท',
            paymentStatus: user.deal_jobs[0].job.payment_type,
          })
        }

        exportCSV.push({
          ลำดับ: index + 1,
          ชื่อพนักงาน: user.name,
          ตำแหน่ง: user.deal_jobs[0].job.position,
          level: user.deal_jobs[0].job.level,
          อัตราจ้าง:
            this.currencyFormat(
              user.deal_jobs[0].job.company.position === 'Pet shop'
                ? this.swMoneyPet(user.deal_jobs[0].job.level)
                : this.swMoney(user.deal_jobs[0].job.level)
            ) + ' บาท',
          ประเภทงาน: user.deal_jobs[0].job.company.position,
          ชื่อผู้ประกอบการ: user.deal_jobs[0].job.company?.company_name,
          WorkTime: this.getDeaJobFullTime(user.deal_jobs),
          จำนวนวัน: user.deal_jobs[0].job.amount + ' วัน',
          คะแนนประเมิน: score[index],
          จำนวนเงิน: this.currencyFormat(user.deal_jobs[0].done_job.total_price) + ' ' + 'บาท',
          สถานะรายการ: user.status ? 'ยืนยันรายการ' : 'ยังไม่ยืนยันรายการ',
          สถานะการชำระเงิน:
            user.deal_jobs[0].job.payment_type === 'success' ? 'ชำระเงินแล้ว' : 'ยังไม่ได้ชำระเงิน',
          หมายเหตุ: user.deal_jobs[0].job.add_on_extra?.note ?? '-',
        })
        EmMoney += user.deal_jobs[0].done_job.total_price
      }
    })

    const moneyV = {
      EmMoney,
    }
    this.props.setEmployee(exportCSV)
    this.props.setEmployerTotal(moneyV)
    this.setState({ partTimeUser })
  }

  render() {
    const allColumns = [
      {
        Header: () => <TextCollum>ลำดับ </TextCollum>,
        accessor: 'Order_ID',
      },
      {
        Header: () => <TextCollum>ชื่อพนักงาน</TextCollum>,
        accessor: 'name',
      },
      {
        Header: () => <TextCollum>ตำแหน่ง</TextCollum>,
        accessor: 'position',
      },
      {
        Header: () => <TextCollum>Level</TextCollum>,
        accessor: 'level',
      },
      {
        Header: () => <TextCollum>อัตราจ้าง</TextCollum>,
        accessor: 'pricing_person',
      },
      {
        Header: () => <TextCollum>ชื่อผู้ประกอบการ</TextCollum>,
        accessor: 'EmployerName',
      },
      {
        Header: () => <TextCollum>ประเภทงาน</TextCollum>,
        accessor: 'JobType',
      },
      {
        Header: () => <TextCollum>วันที่ปฏิบัติงาน</TextCollum>,
        accessor: 'WorkTime',
        maxWidth: 50,
        Cell: (row) => <Div>{row.original.WorkTime}</Div>,
      },
      {
        Header: () => <TextCollum>จำนวนวัน</TextCollum>,
        accessor: 'amountDay',
        Cell: (row) => <Center>{row.original.amountDay} วัน</Center>,
      },
      {
        Header: () => <TextCollum>จำนวนเงิน</TextCollum>,
        accessor: 'moneyPerson',
      },
      {
        Header: () => <TextCollum>คะแนนประเมิน</TextCollum>,
        accessor: 'score',
      },
      {
        Header: () => <TextCollum>หมายเหตุ</TextCollum>,
        accessor: 'note',
        Cell: (row) => <Center>{row.original.note ?? '-'}</Center>,
      },
      {
        Header: () => <TextCollum>สถานะรายการ</TextCollum>,
        accessor: 'listConfirm',
        Cell: (row) => (
          <Center>{row.original.listConfirm ? 'ยืนยันรายการ' : 'ยังไม่ยืนยันรายการ'}</Center>
        ),
      },
    ]

    let columnWillShow = []
    console.log('oo', this.state.partTimeUser)
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })

    return (
      <div>
        <Card>
          {this.state.partTimeUser.length >= 0 ? (
            <div style={{ width: '100%' }}>
              <ReactTable
                data={this.state.partTimeUser} //data object
                columns={columnWillShow} //column config object
                loading={false}
                //  defaultPageSize={this.state.PageSize}
                pageSize={10}
                showPagination={true}
                className="-striped -highlight"
              />
            </div>
          ) : (
            //
            <ReactTable
              data={this.state.partTimeUser}
              columns={columnWillShow}
              defaultPageSize={10}
              showPagination={true}
              className=""
            />
          )}
        </Card>
        {this.props.children}
      </div>
    )
  }
}

const mapState = (state) => ({
  customers: state.customer.customers,
  columnOrderManagementBooking: state.orderManagement.columnOrderManagementBooking,
  start_dateOrder: state.orderManagement.start_dateOrder,
  end_dateOrder: state.orderManagement.end_dateOrder,
  orderMode: state.orderManagement.orderMode,
})

const mapDispatch = (dispatch) => {
  return {
    setEmployee: dispatch.orderManagement.setEmployee,
    setEmployerTotal: dispatch.orderManagement.setEmployerTotal,
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    getOrderBookList: dispatch.orderManagement.getOrderBookList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(mapState, mapDispatch)(OrderManagementBooking)
