import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Button from '../../components/button'
import ReactTable from 'react-table'
import Rightbar from '../../layouts/orderManagementLayout/rightbar'
import { get } from '../../utils/service'
import moment from 'moment'
import './orderManagefilterBy.css'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

const Container = styled.div`
  padding: 20px;
`

// const Footer = styled.div`
//   position: absolute;
//   left: 10px;
//   bottom: 50px;
// `
// const FooterPicContainer = styled.div`
//   display: flex;
//   padding: 0 0 0 30px;
//   img {
//     margin-right: 10px;
//   }
// `


const Card = styled.div`
      float: left;
      width: 33.333%;
      padding: .75rem;
      margin-bottom: 2rem;
      border: 0;
      `

class orderManagementFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      column: this.props.columnOrderManagementNamelist,
      showRightbar: false,
      dataProfile: [],
      salary: '',
      start_datetime: '',
      end_datetime: '',
      listWeekPayment: [],
      Tempweek: '',
      loading: false,
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
      yearList: [],
      yaerValue: ''
    }
  }

  componentDidMount() {
    this.props.setPageTitle('Profile')
    this.props.showBackIcon(true)
    this.getDataProfile()
    // this.getDataCalendarTable()
    this.setDateForBooking()
    this.props.setStartdateOrder('')
    this.props.setEndateOrder('')
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.column !== this.props.columnOrderManagementNamelist) {
      this.setState({ column: this.props.columnOrderManagementNamelist })
    }
  }
  handleChangeStartDate = e => {
    this.setState({ start_datetime: e }, () => this.getDataCalendarTable())


  }
  handleChangeEndDate = e => {
    this.setState({ end_datetime: e }, () => this.getDataCalendarTable())
  }
  async handleChangeYear(year) {
    this._isMounted = true;
    if (this._isMounted) {
      var d = new Date();
      var y = d.getFullYear();
      let Tempyaer = 0
      if (year !== undefined) {
        Tempyaer = year
      } else {
        Tempyaer = y
      }
      let array = []
      let temparray = []
      let temMonth = Tempyaer % 4
      if (Tempyaer) {
        for (let i = 0; i < 12; i++) {
          if (i < 9) {
            if (i === 1) {
              if (temMonth === 0) {
                array.push(`${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${i + 1}-16 - ${Tempyaer}-0${i + 1}-29`)
              } else {
                array.push(`${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${i + 1}-16 - ${Tempyaer}-0${i + 1}-28`)
              }
            } else if (i === 0 || i === 2 || i === 4 || i === 6 || i === 7 || i === 9) {
              array.push(`${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${i + 1}-16 - ${Tempyaer}-0${i + 1}-31`)
            } else if (i === 3 || i === 5 || i === 8) {
              array.push(`${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${i + 1}-16 - ${Tempyaer}-0${i + 1}-30`)
            }
          } else {
            if (i === 9 || i === 11) {
              array.push(`${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${i + 1}-16 - ${Tempyaer}-${i + 1}-31`)
            } else if (i === 10) {
              array.push(`${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${i + 1}-16 - ${Tempyaer}-${i + 1}-30`)
            }
          }
        }
        if (array.length >= 12) {
          for (let i = 0; i < array.length; i++) {
            temparray.push(...array[i].split(','))
          }
        }
      }
      this.setState({ listWeekPayment: temparray, yaerValue: year })
    }
  }
  async SelectDatePyment(el) {
    this.setState({ Tempweek: el })
    let test = el.split(' - ')
    let test1 = test[0]
    let test2 = test[1]
    await this.getDataCalendarTable(test1, test2)

  }

  async setDateForBooking() {

    const d = new Date()
    const n = d.getDate();
    //yaerList
    var nYear = d.getFullYear();
    var num = nYear - 5
    this.setState({ yaerValue: nYear })
    if (num) {
      for (var i = num; i <= nYear; i++) {
        this.state.yearList.push(i)
      }
    }
    if (n <= 15) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 1).format()),
        end_datetime: new Date(moment().set('date', 15).format())
      })
    } else if (n >= 16) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 16).format()),
        end_datetime: new Date(moment(d).endOf('month').format())
      })
    }
    await this.getDataCalendarTable()
    this.handleChangeYear()
    if (this.props.history.location.state !== undefined) {
      this.setState({ Tempweek: `${moment(this.props.history.location.state.start_datetime, 'YYYY-MM-DD').format('YYYY-MM-DD')} - ${moment(this.props.history.location.state.end_datetime, 'YYYY-MM-DD').format('YYYY-MM-DD')}` })
    }
  }
  async getDataProfile() {
    if (this.props.location.state !== undefined) {
      const id = this.props.location.state.id
      const resp = await get(`admin/maid/id/${id}`)
      this.setState({
        dataProfile: {
          picture: resp.user.image_url,
          name: resp.user.first_name + ' ' + resp.user.last_name,
          role: resp.user.role,
          email: resp.user.email,
          phone: resp.user.phone,
          address: resp.user.user_locations.length > 0 ? resp.user.user_locations[0].main_text : ''
        }
      })
    }
  }
  async getDataCalendarTable(start_date, end_date) {
    let { startDate, endDate } = this.state
    if (start_date && end_date) {
      startDate = moment(start_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
      endDate = moment(end_date, 'YYYY-MM-DD').format('YYYY-MM-DD')
    } else {
      startDate = moment(this.state.start_datetime).format('YYYY-MM-DD');
      endDate = moment(this.state.end_datetime).format('YYYY-MM-DD')
    }
    this.setState({ loading: true })
    if (startDate && endDate) {
      if (this.props.location.state !== undefined) {
        const id = this.props.location.state.id
        const resp = await get(`admin/maid/id/${id}/calendar?start_date=${startDate}&end_date=${endDate}`)
        let tempDate = []
        if (resp.calendar.length <= 10) {
          this.setState({ loading: false, PageSize: 10 })
          for (let i = 0; i < resp.calendar.length; i++) {
            tempDate.push({
              Date_service: resp.calendar.length > 0 ? moment(resp.calendar[i].start_datetime).format('YYYY/MM/DD') : '',
              ServiceHrs: resp.calendar.length > 0 ? resp.calendar[i].booking.duration : '',
              Type_service: resp.calendar.length > 0 ? resp.calendar[i].booking.type : '',
              Payment: resp.calendar.length > 0 ? resp.calendar[i].booking.maid_wage : '',
              Rating: resp.calendar[i].maid_review != null ? resp.calendar[i].maid_review.rating : '',
            })

          }
        } else {
          for (let i = 0; i < resp.calendar.length; i++) {
            this.setState({ loading: false, PageSize: resp.calendar.length })
            tempDate.push({
              Date_service: resp.calendar.length > 0 ? moment(resp.calendar[i].start_datetime).format('YYYY/MM/DD') : '',
              ServiceHrs: resp.calendar.length > 0 ? resp.calendar[i].booking.duration : '',
              Type_service: resp.calendar.length > 0 ? resp.calendar[i].booking.type : '',
              Payment: resp.calendar.length > 0 ? resp.calendar[i].booking.maid_wage : '',
              Rating: resp.calendar[i].maid_review != null ? resp.calendar[i].maid_review.rating : '',
            })

          }
        }
        this.setState({ data: tempDate, salary: resp.salary })
      }
    }
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }
  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    const { history } = this.props
    history.push(this.props.history.location.pathname)
  }

  render() {
    const allColumns = [
      {
        Header: () => <h6>Date of service</h6>,
        accessor: 'Date_service', // String-based value accessors!
        minWidth: 100,
        paddingLeft: 10
      },
      {
        Header: () => <h6>Service (Hrs)</h6>,
        accessor: 'ServiceHrs',
        minWidth: 100,
      },
      {
        Header: () => <h6>Type of service</h6>,
        accessor: 'Type_service',
        minWidth: 100,
      },
      {
        Header: () => <h6>Payment</h6>,
        accessor: 'Payment',
        minWidth: 100,
      },
      {
        Header: () => <h6>Rating</h6>,
        accessor: 'Rating',
        minWidth: 100,
      },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor &&
          columnWillShow.push(obj)
      }
      return {}
    })
    const currentLocation = this.props.history.location.pathname
    const { dataProfile, salary } = this.state
    return (
      <Container>
        <div className="container-fluid">
          {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
          {/* Row */}
          <div >
            <div className="row ">
              {/* Column */}
              <Card>
                <div className="card h-vh-80 col-md-12" style={{ width: '330px', height: '620px', borderRadius: '6px', border: ' solid 1px #324755', backgroundColor: '#ffffff' }}>
                  <div className="card-body">
                    <center className="m-t-30">
                      <img
                        src={`${dataProfile.picture}`}
                        alt="profile"
                        className="rounded-circle"
                        width={150}
                      />

                      <h4 className="card-title m-t-10">{dataProfile.picnameture}</h4>
                    </center>
                    <small className="text-muted">Role </small>
                    <h6>{dataProfile.role}</h6>
                    <small className="text-muted">Email address </small>
                    <h6>{dataProfile.email}</h6>
                    <small className="text-muted p-t-30 db">Phone</small>
                    <h6>{dataProfile.phone}</h6>
                    <small className="text-muted p-t-30 db">Address</small>
                    <h6>{dataProfile.address}</h6>
                    {/* <Footer>
                    <h6>Social Profile: </h6>
                    <FooterPicContainer>
                      <img
                        src={`${process.env.PUBLIC_URL}/assets/images/users/6.jpg`}
                        alt="profile2"
                        className="rounded-circle"
                        width={35}
                      />
                      <img
                        src={`${process.env.PUBLIC_URL}/assets/images/users/7.jpg`}
                        alt="profile3"
                        className="rounded-circle"
                        width={35}
                      />
                      <img
                        src={`${process.env.PUBLIC_URL}/assets/images/users/8.jpg`}
                        alt="profile4"
                        className="rounded-circle"
                        width={35}
                      />
                    </FooterPicContainer>
                  </Footer> */}
                  </div>
                </div>
              </Card>
              <div className="col-md-8">
                <div className="btn-container">
                  <Button className="btn" style={{ borderRadius: "11px" }} label="Column setting" onClick={() => this.handleColumn()} />
                  <Button className="btn" style={{ borderRadius: "11px" }} label="Filter by" onClick={() => this.handleFilter()} />
                </div>
                <div className="col-xs-12 col-sm-10 col-md-10 col-xl-8" style={{ lineHeight: "60px" }}>
                  <div className="row form-group">
                    <div className="col-md-6">
                      <div className="row form-group">
                        <div className="col-md-4" >
                          <label style={{ fontSize: "18px", fontWeight: "500px" }}>Year</label>
                        </div>
                        <div className="col-md-6">
                          <select className="OrderFilterInput" id="yaerValue" value={this.state.yaerValue} onChange={(e) => this.handleChangeYear(e.target.value)}>
                            {
                              this.state.yearList.length > 0 ?
                                this.state.yearList.map((el, i) => {
                                  return <option key={i} value={el} >{el}</option>
                                })
                                :
                                <option></option>
                            }
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6" >
                      <div className="row form-group">
                        <div className="col-md-3" >
                          <label style={{ fontSize: "18px", fontWeight: "500px" }}>Date</label>
                        </div>
                        <div className="col-md-6">
                          <select className="OrderFilterInput" id="yaerWeek" value={this.state.Tempweek} onChange={(e) => this.SelectDatePyment(e.target.value)}>
                            {
                              this.state.listWeekPayment.length > 0 ?
                                this.state.listWeekPayment.map((el, i) => {
                                  return <option key={i} value={el} >{el}</option>
                                })
                                :
                                <option></option>
                            }
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xs-12 col-sm-10 col-md-10 col-xl-8" style={{ lineHeight: "40px" }}>
                  <div className="row form-group">
                    <div className="col-md-6">
                      <div className="row form-group">
                        <div className="col-md-4" >
                          <label style={{ fontSize: "18px", fontWeight: "500px" }}>Salary</label>
                        </div>
                        <div className="col-md-6">
                          <input className="OrderFilterInput"
                            type="text" name="salary" value={salary} disabled />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6" >
                      <div className="row form-group">
                        <div className="col-md-3" >
                          <label style={{ fontSize: "18px", fontWeight: "500px" }}>Status</label>
                        </div>
                        <div className="col-md-6">
                          <select className="OrderFilterInput" name="max_type" disabled>
                            <option value="PAID">PAID</option>
                            <option value="UNPAID">UNPAID</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div className="card h-vh-80 OderReactTable">
                  <ReactTable
                    data={this.state.data} //data object
                    columns={columnWillShow}  //column config object
                    loading={this.state.loading}
                    pageSize={this.state.PageSize}
                    showPagination={false}
                    style={{
                      height: "400px"
                    }}
                    className="-striped -highlight"
                  />

                </div>
              </div>
            </div>
          </div>
          <Rightbar data={this.state} currentLocation={currentLocation} />
          {this.props.children}
        </div>
      </Container>
    )
  }
}

const mapState = (state) => ({
  columnOrderManagementNamelist: state.orderManagement.columnOrderManagementNamelist,

})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}

export default connect(
  mapState,
  mapDispatch
)(orderManagementFilter)
