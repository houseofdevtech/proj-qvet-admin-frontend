import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { post, get } from '../../utils/service'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import '../../style/react-table-custom.css'
import Toggle from 'react-toggle'
import './orderPayment.css'
import moment from 'moment'

const Card = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  border-radius: 5px;
  border: 1px solid ${(props) => props.theme.active};
  background: #fff;
  height: max-content;
  width: 100%;
  position: relative;
  top: -1px;
  padding: 10px;
  align-items: flex-start;
  overflow: auto;
`

class OrderManagementPayment extends Component {
  _isMounted = false
  constructor(props) {
    super(props)
    this.state = {
      column: this.props.columnOrderManagementPayment,
      data: [],
      tempdata: [],
      checked: false,
      loading: false,
      paramsDate: {},
      PageSize: 11,
      numberPage: 1,
      totalPages: 0,
      totaldata: 0,
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.start_dateOrder !== undefined &&
      this.props.start_dateOrder !== '' &&
      prevProps.start_dateOrder !== this.props.start_dateOrder &&
      this.props.end_dateOrder !== undefined &&
      this.props.end_dateOrder !== '' &&
      prevProps.end_dateOrder !== this.props.end_dateOrder
    ) {
      if (this.props.start_dateOrder !== '') {
        this.setState({
          PageSize: 11,
          data: [],
        })
        this.getDataPayment()
      }
    }
    if (prevState.column !== this.props.columnOrderManagementPayment) {
      this.setState({
        column: this.props.columnOrderManagementPayment,
      })
    }
  }
  componentDidMount() {
    this.props.setOrderMode('Payment')
    this.setDateForFectData()
  }
  componentWillUnmount() {
    this._isMounted = false
  }
  async setDateForFectData() {
    /// set default Date /// 
    const d = new Date()
    const n = d.getDate()
    if (n <= 15) {
      this.state.paramsDate = await {
        start_date: moment().set('date', 1).format('YYYY-MM-DD'),
        end_date: moment().set('date', 15).format('YYYY-MM-DD'),
      }
    } else if (n >= 16) {
      this.state.paramsDate = await {
        start_date: moment().set('date', 16).format('YYYY-MM-DD'),
        end_date: moment(d).endOf('month').format('YYYY-MM-DD'),
      }
    }
    if (this.state.paramsDate.start_date && this.state.paramsDate.end_date) {
      await this.props.setStartdateOrder('')
      await this.props.setEndateOrder('')
      await this.getDataPayment()
    }
  }
  async getDataPayment() {
    this._isMounted = true
    if (this._isMounted === true) {
      if (this.props.start_dateOrder !== '' && this.props.end_dateOrder !== '') {
        if (this.props.start_dateOrder !== undefined && this.props.end_dateOrder !== undefined) {
          const params = {
            start_date: this.props.start_dateOrder ? moment(this.props.start_dateOrder).format('YYYY-MM-DD') : moment(this.state.paramsDate.start_date).format('YYYY-MM-DD'),
            end_date: this.props.end_dateOrder ? moment(this.props.end_dateOrder).format('YYYY-MM-DD') : moment(this.state.paramsDate.end_date).format('YYYY-MM-DD'),
          }
          if (params) {
            this.setState({
              loading: true,
            })
            await this.props.getOrderPaymentList({
              start_date: params.start_date,
              end_date: params.end_date,
            })
            const resp = await get(`admin/maid/payment/list?start_date=${params.start_date}&end_date=${params.end_date}`)
            let dataPayment = []
            for (let i = 0; i < resp.payment.length; i++) {
              this.setState({
                loading: false,
                totalPages: resp.page_count,
                totaldata: resp.item_count,
                pages: resp.page_count,
              })
              dataPayment.push({
                picture: resp.payment[i].maid.user.image_url,
                Name_maid:
                  resp.payment[i].maid.user.first_name + ' ' + resp.payment[i].maid.user.last_name,
                id: resp.payment[i].maid.id,
                MP_Hours: resp.payment[i].multi_package_hours,
                OT_Hours: resp.payment[i].one_time_hours,
                Salary: resp.payment[i].salary,
                Payment_date: resp.payment[i].payment_datetime,
                Status: resp.payment[i].status,
                isToggle:
                  resp.payment[i].status === 'UNPAID'
                    ? false
                    : resp.payment[i].status === 'PAID'
                      ? true
                      : false,
                income: resp.payment[i].income,
                bonus_five_star: resp.payment[i].five_star_bbonus_five_staronus,
                bonus_per_month: resp.payment[i].bonus_per_month,
                bonus_total_hours: resp.payment[i].bonus_total_hours,
                start_datetime: params.start_date,
                end_datetime: params.end_date,
              })
            }
            await this.setState({
              data: dataPayment,
            })
          }
        }
      } else {
        this.setState({
          loading: true,
        })
        let data = []
        if (this.state.paramsDate.start_date && this.state.paramsDate.end_date) {
          this.props.getOrderPaymentList({
            start_date: this.state.paramsDate.start_date,
            end_date: this.state.paramsDate.end_date,
          })
          const resp = await get(
            `admin/maid/payment/list?start_date=${this.state.paramsDate.start_date}&end_date=${this.state.paramsDate.end_date}`
          )
          if (resp.payment.length > 0) {
            for (let i = 0; i < resp.payment.length; i++) {
              this.setState({
                loading: false,
                totalPages: resp.page_count,
                totaldata: resp.item_count,
                pages: resp.page_count
              })
              data.push({
                picture: resp.payment[i].maid.user.image_url,
                Name_maid:
                  resp.payment[i].maid.user.first_name + ' ' + resp.payment[i].maid.user.last_name,
                id: resp.payment[i].maid.id,
                MP_Hours: resp.payment[i].multi_package_hours,
                OT_Hours: resp.payment[i].one_time_hours,
                Salary: resp.payment[i].salary,
                Payment_date: resp.payment[i].payment_datetime,
                Status: resp.payment[i].status,
                isToggle:
                  resp.payment[i].status === 'UNPAID'
                    ? false
                    : resp.payment[i].status === 'PAID'
                      ? true
                      : false,
                income: resp.payment[i].income,
                bonus_five_star: resp.payment[i].five_star_bbonus_five_staronus,
                bonus_per_month: resp.payment[i].bonus_per_month,
                bonus_total_hours: resp.payment[i].bonus_total_hours,
                start_datetime: this.state.paramsDate.start_date,
                end_datetime: this.state.paramsDate.end_date,
              })
            }
          } else {
            this.setState({
              loading: false,
            })
          }
        }
        this.setState({
          data,
        })
      }
    }
  }
  onScroll = (e) => {
    if (this.state.numberPage === this.state.totalPages) {
      this.setState({ numberPage: this.state.numberPage + 1 })
    }
    if (e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight  && e.target.scrollTop) {
      if (this.state.totalPages > 1 && this.state.data.length < this.state.totaldata) {
        this.setState({
          numberPage: this.state.numberPage + 1,
          loading: true,
        })
        if (this.props.start_dateOrder !== '' && this.props.end_dateOrder !== '') {
          if (this.props.start_dateOrder !== undefined && this.props.end_dateOrder !== undefined) {
            const params = {
              start_date: moment(this.props.start_dateOrder).format('YYYY-MM-DD'),
              end_date: moment(this.props.end_dateOrder).format('YYYY-MM-DD'),
            }
            if (params) {
              this.props.getOrderPaymentList({
                start_date: params.start_date,
                end_date: params.end_date,
              })
              setTimeout(() => {
                get(
                  `admin/maid/payment/list?start_date=${params.start_date}&end_date=${params.end_date}&page=${this.state.numberPage}&limit=10`
                ).then((resp) => {
                  if (resp.payment.length > 0) {
                    for (let i = 0; i < resp.payment.length; i++) {
                      this.state.data.push({
                        picture: resp.payment[i].maid.user.image_url,
                        Name_maid:
                          resp.payment[i].maid.user.first_name +
                          ' ' +
                          resp.payment[i].maid.user.last_name,
                        id: resp.payment[i].maid.id,
                        MP_Hours: resp.payment[i].multi_package_hours,
                        OT_Hours: resp.payment[i].one_time_hours,
                        Salary: resp.payment[i].salary,
                        Payment_date: resp.payment[i].payment_datetime,
                        Status: resp.payment[i].status,
                        isToggle:
                          resp.payment[i].status === 'UNPAID'
                            ? false
                            : resp.payment[i].status === 'PAID'
                              ? true
                              : false,
                        income: resp.payment[i].income,
                        bonus_five_star: resp.payment[i].five_star_bbonus_five_staronus,
                        bonus_per_month: resp.payment[i].bonus_per_month,
                        bonus_total_hours: resp.payment[i].bonus_total_hours,
                        start_datetime: this.state.paramsDate.start_date,
                        end_datetime: this.state.paramsDate.end_date,
                      })
                    }
                    this.setState({
                      loading: false,
                      PageSize: this.state.data.length,
                    })
                  } else {
                    this.setState({
                      loading: false,
                      PageSize: this.state.data.length,
                    })
                  }
                })
              }, 100)
            }
          }
        } else {
          setTimeout(() => {
            get(
              `admin/maid/payment/list?start_date=${this.state.paramsDate.start_date}&end_date=${this.state.paramsDate.end_date}&page=${this.state.numberPage}&limit=10`
            ).then((resp) => {
              if (resp.payment.length > 0) {
                for (let i = 0; i < resp.payment.length; i++) {
                  this.state.data.push({
                    picture: resp.payment[i].maid.user.image_url,
                    Name_maid:
                      resp.payment[i].maid.user.first_name +
                      ' ' +
                      resp.payment[i].maid.user.last_name,
                    id: resp.payment[i].maid.id,
                    MP_Hours: resp.payment[i].multi_package_hours,
                    OT_Hours: resp.payment[i].one_time_hours,
                    Salary: resp.payment[i].salary,
                    Payment_date: resp.payment[i].payment_datetime,
                    Status: resp.payment[i].status,
                    isToggle:
                      resp.payment[i].status === 'UNPAID'
                        ? false
                        : resp.payment[i].status === 'PAID'
                          ? true
                          : false,
                    income: resp.payment[i].income,
                    bonus_five_star: resp.payment[i].five_star_bbonus_five_staronus,
                    bonus_per_month: resp.payment[i].bonus_per_month,
                    bonus_total_hours: resp.payment[i].bonus_total_hours,
                    start_datetime: this.state.paramsDate.start_date,
                    end_datetime: this.state.paramsDate.end_date,
                  })
                }
                this.setState({
                  loading: false,
                  PageSize: this.state.data.length,
                })
              } else {
                this.setState({
                  loading: false,
                  PageSize: this.state.data.length,
                })
              }
            })
          }, 100)
        }
      }
    }
  }
  handleChangeStatus(row) {
    // Change Status date  PAID UNPAID //
    let tempIdx = 0
    const data = this.state.data
    data.forEach((v, i) => {
      if (v.id === row.original.id) {
        v.isToggle = !v.isToggle
        v.status = v.isToggle ? 'PAID' : 'UNPAID'
        tempIdx = i
      }
    })
    this.setState({
      data,
      checked: this.state.data[tempIdx].isToggle,
    })
    const objSalary = {
      salary: row.original.Salary,
      income: row.original.income !== undefined ? row.original.income : 0,
      bonus_five_star:
        row.original.five_star_bbonus_five_staronus !== undefined
          ? row.original.five_star_bbonus_five_staronus
          : 0,
      bonus_per_month:
        row.original.bonus_per_month !== undefined ? row.original.bonus_per_month : 0,
      bonus_total_hours:
        row.original.bonus_total_hours !== undefined ? row.original.bonus_total_hours : 0,
      start_date: this.props.start_dateOrder ? this.props.start_dateOrder : this.state.paramsDate.start_date,
      end_date: this.props.end_dateOrder ? this.props.end_dateOrder : this.state.paramsDate.end_date,
    }
    if (this.state.data[tempIdx].status === 'PAID') {
      if (objSalary) {
        post(objSalary, `admin/maid/id/${row.original.id}/paid`)
      }
    } else if (this.state.data[tempIdx].status === 'UNPAID') {
      if (objSalary) {
        post(objSalary, `admin/maid/id/${row.original.id}/unpaid`)
      }
    }
  }

  render() {
    const allColumns = [
      {
        Header: '',
        accessor: 'picture',
        Cell: (row) => (
          <div style={{ width: '100%' }}>
            <img
              src={`${row.original.picture}`}
              alt="profile3"
              className="rounded-circle"
              width={35}
            />
          </div>
        ),
        minWidth: 40,
      },
      {
        Header: () => <h6> </h6>,
        accessor: 'tooltrip', // String-based value accessors!
        minWidth: 30,
      },
      {
        Header: () => <h6> Name of maid </h6>,
        accessor: 'Name_maid', // String-based value accessors!
        minWidth: 100,
      },
      {
        Header: () => <h6> MP Hours </h6>,
        accessor: 'MP_Hours',
        minWidth: 100,
      },
      {
        Header: () => <h6> OT Hours </h6>,
        accessor: 'OT_Hours',
        minWidth: 100,
      },
      {
        Header: () => <h6> Salary </h6>,
        accessor: 'Salary',
        minWidth: 100,
      },
      {
        Header: () => <h6> Payment date </h6>,
        accessor: 'Payment_date',
        minWidth: 100,
      },
      {
        Header: () => <h6> Status </h6>,
        accessor: 'Status',
        Cell: (row) => (
          <div style={{ width: '100%', cursor: 'pointer', height: '25px' }}>
            <label>
              <Toggle
                onChange={this.handleChangeStatus.bind(this, row)}
                checked={row.original.isToggle}
                value={row.original.Status}
                icons={{
                  checked: (
                    <div
                      style={{
                        display: 'flex',
                        //  justifyContent: "center",
                        alignItems: 'center',
                        height: '100%',
                        fontSize: 15,
                        color: '#FFFFFF',
                        paddingRight: 2,
                      }}>
                      Paid
                    </div>
                  ),
                  unchecked: (
                    <div
                      style={{
                        display: 'flex',
                        float: 'right',
                        alignItems: 'center',
                        height: '100%',
                        fontSize: 15,
                        color: '#324755',
                        paddingRight: 2,
                      }}>
                      Unpaid
                    </div>
                  ),
                }}
              />
            </label>
          </div>
        ),
        minWidth: 100,
      },
    ]

    let columnWillShow = []
    this.state.column.map((colName) => {
      for (var obj of allColumns) {
        colName.isShow && obj.accessor === colName.accessor && columnWillShow.push(obj)
      }
      return {}
    })

    return (
      <div>
        <Card>
          {' '}
          {this.state.data.length >= 0 ? (
            <div onScroll={this.onScroll} style={{ width: '100%' }}>
              <ReactTable
                data={this.state.data}
                columns={columnWillShow}
                loading={this.state.loading}
                pageSize={this.state.PageSize}
                showPagination={false}
                style={{ height: '400px' }}
                getTrGroupProps={(state, rowInfo) => {
                  if (rowInfo !== undefined) {
                    return {
                      onDoubleClick: () => {
                        if (rowInfo.original.isToggle === true) {
                          // this.props.history.push(`/admin/order-management/orderManagementFilter`,rowInfo.original)
                          this.props.history.replace({
                            pathname: '/admin/order-management/orderManagementFilter',
                            state: rowInfo.original,
                          })
                        }
                      },
                    }
                  }
                }}
                className="-striped -highlight"
              />
            </div>
          ) : (
              <ReactTable
                data={this.state.data}
                columns={columnWillShow}
                defaultPageSize={5}
                showPagination={false}
                className=""
              />
            )}
        </Card>
      </div>
    )
  }
}

const mapState = (state) => ({
  start_dateOrder: state.orderManagement.start_dateOrder,
  end_dateOrder: state.orderManagement.end_dateOrder,
  customers: state.customer.customers,
  columnOrderManagementPayment: state.orderManagement.columnOrderManagementPayment,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    getOrderPaymentList: dispatch.orderManagement.getOrderPaymentList,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder
  }
}

export default connect(mapState, mapDispatch)(OrderManagementPayment)
