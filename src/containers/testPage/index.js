import React, { Component } from 'react'
import styled from 'styled-components'

import Chart from 'react-apexcharts'
// import ApexCharts from "apexcharts";

const Container = styled.div`
  background: #eee
  position: absolute;
  // top: 64px;
  // left: 250px;
  height : 100%;
  width: 100%

  // @media (max-width: 1170px) {
  //   left: 69px;
  // }

  @media (max-width: 768px) {
    left: 0px;
  }

`
const GraphContainer = styled.div`
  // background: #d3e7ee;
  // position: relative;
  padding: 35px;
  // width: 1fr;
`
const Grid = styled.div`
  padding: 35px;
  background-color: #f9f9f9;
  /* min-height: calc(100vh - 154px); */
  display: grid;
  grid-gap: 20px;
  grid-template-columns: repeat(auto-fill, minmax(600px, 600px));
  grid-auto-rows: 400px;
  align-self: auto;

  @media (max-width: 768px) {
    grid-row-gap: 15px;
    grid-template-columns: 1fr;
    grid-auto-rows: 400px;
    justify-items: center;
  }
`

class Content extends Component {
  state = {
    options: {
      chart: {
        id: 'apexchart-example',
      },
      xaxis: {
        categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999],
      },

      colors: ['#fea735', '#0077ff', '#fe7235']
      // colors: ['#235d3a', '#397d54', '#73c088', '#a8e0b7', '#c8ead1']
      // colors: ['#F44336', '#E91E63', '#9C27B0'],
      // fill: {
      //   colors: ['#F44336', '#E91E63', '#9C27B0'],
      // },
      // dataLabels: {
      //   style: {
      //     colors: ['#F44336', '#E91E63', '#9C27B0']
      //   }
      // }
      // markers: {
      //   colors: ['#F44336', '#E91E63', '#9C27B0'],
      // },
      // grid: {
      //   row: {
      //     colors: ['#F44336', '#E91E63', '#9C27B0']
      //   },
      //   column: {
      //     colors: ['#F44336', '#E91E63', '#9C27B0']
      //   }
      // }
    },
    series: [
      {
        name: 'sales',
        data: [30, 40, 35, 50, 49, 60, 70, 91, 125],
      },
      {
        name: 'supplies',
        data: [50, 60, 85, 90, 129, 150, 160, 191, 210],
      },
    ],
  }

  render() {
    const types = [
      'line',
      'area',
      'bar',
      'pie',
      'donut',
      'scatter',
      'bubble',
      'heatmap',
      'radialBar',
      'radar',
      'histogram',
      'candlestick',
    ]
    return (
      <Container>
        <Grid>
          {types.map((type) => (
            <GraphContainer>
              <Chart
                options={this.state.options}
                series={this.state.series}
                type={type}
                width={500}
                height={320}
                colors={this.state.colors}
              />
            </GraphContainer>
          ))}
        </Grid>
      </Container>
    )
  }
}

export default Content
