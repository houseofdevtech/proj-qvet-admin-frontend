import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from '../../components/button'
import TabComponent from './../../components/tabComponent'
import styled from 'styled-components'

// import Rightbar from './rightbar'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

class InventoriesLayout extends Component {
  state = {
    showRightbar: false,
    mode: 'column',
    monthNames: [
      { value: '01', label: 'January' },
      { value: '02', label: 'February' },
      { value: '03', label: 'March' },
      { value: '04', label: 'April' },
      { value: '05', label: 'May' },
      { value: '06', label: 'June' },
      { value: '07', label: 'July' },
      { value: '08', label: 'August' },
      { value: '09', label: 'September' },
      { value: '10', label: 'October' },
      { value: '11', label: 'November' },
      { value: '12', label: 'December' },
    ],
  }

  componentDidMount() {
    this.props.setPageTitle('Inventories Calendar List')
    this.props.showBackIcon(true)
    this.props.getMaidList()
    // console.log(this.props)
    // this.state.showRightbar && document.getElementById('layout').classList.add('blur')
  }

  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    // Refresh Redux State to React table
    // by Refresh current Path
    const { history } = this.props
    history.push(this.props.history.location.pathname)
    // console.log(this.state.showRightbar)
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }

  onChangeTab = (path) => {
    // console.log(`onChangeTab : ${match.url + path}`)
    const { history, match } = this.props
    history.push(match.url + path)
  }

  async handleChangeYear(value) {
    console.log(value)
  }

  render() {
    // console.log(`customer : ${JSON.stringify(this.props.customers)}`)
    const currentLocation = this.props.history.location.pathname

    return (
      <div>
        {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
        <div className="page-breadcrumb">
          <div className="btn-container">
            <div className="col-md-6">
              <select
                className="OrderFilterInput"
                id="yaerValue"
                value={this.state.yaerValue}
                onChange={(e) => this.handleChangeYear(e.target.value)}>
                {this.state.monthNames.length > 0 ? (
                  this.state.monthNames.map((el, i) => {
                    return (
                      <option key={i} value={el.value}>
                        {el.label}
                      </option>
                    )
                  })
                ) : (
                  <option></option>
                )}
              </select>
            </div>
          </div>

          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapState = (state) => ({
  maidsLsit: state.maid.maidsLsit,
  maidColumn: state.maid.column,
  customerList: state.customer.customerList,
  customerColumn: state.customer.column,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    getMaidList: dispatch.maid.getMaidList,
    getCustomerList: dispatch.customer.getCustomerList,
  }
}

export default connect(mapState, mapDispatch)(withRouter(InventoriesLayout))
