import React, { Component } from 'react'

// import { SideBarMenu } from './sidebarMenu'
import { connect } from 'react-redux'
import ErrorBoundary from '../errorBoundary'
// import { Content } from './style'
import TopBar from './topbar'
import SideBar from './sidebar'
// import RightBar from './rightbar'


//<Header title={this.props.pageTitle} />

class Layout extends Component {
  render() {
    // const { children } = this.props
    // console.log(this.props.children)
    return (
      <ErrorBoundary>
        {/* ===== Header ===== */}
        <TopBar title={this.props.pageTitle} />

        {/* ===== Side Bar ===== */}
        <SideBar />

        {/* ===== Content ===== */}
        {/* <div class="page-wrapper" style={{display: 'block'}}> */}
        <div className="page-wrapper" style={{display: 'block',overflow:'auto', backgroundColor: '#f8eff6'}}>
        {this.props.children}
        </div>
        
        {/* <RightBar/> */}
      </ErrorBoundary>
    )
  }
}

const mapState = (state) => {
  return { pageTitle: state.Title.pageTitle }
}

const mapDispatch = () => ({})

export default connect(
  mapState,
  mapDispatch
)(Layout)
