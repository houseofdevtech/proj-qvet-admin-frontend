import React, { Component } from 'react'
// import styled from 'styled-components'
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import '../../style/style.custom.css'
import { SideBarMenu as menus } from './sidebarMenu'
import ProfilePane from './profile'

class Sidebar extends Component {
  onClick(e) {
    e.preventDefault()
    console.log('onclick..')
  }

  componentDidMount() {
    // Try to maintain expand menus on sidebar when perform refresh
    // By check current location path against "menus.link"
    // Because the template is from Bootstarp + jQuery so this will adjust by side
    // console.log(`this.props : ${JSON.stringify(this.props)}`)
    // console.log(`tune : ${JSON.stringify(this.props.history.location.pathname)}`)
    const currentLocation = this.props.history.location.pathname
    menus.map((menu) =>
      menu.link
        ? currentLocation.includes(menu.link) && this.handleToggleActive(menu.label)
        : menu.subMenu.map(
            (subMenu) =>
              currentLocation.includes(subMenu.link) && this.handleToggleActive(menu.label)
          )
    )
  }

  handleToggleActive(name) {
    // Add class to expand sidebar menu
    const targetName = document.getElementsByName(name)
    for (let node of targetName) {
      // console.log(node.tagName)
      node.tagName === 'A' && node.classList.add('active')
      node.tagName === 'UL' && node.classList.add('in')
    }
    // console.log(name)
    // console.log(targetName)
  }

  render() {
    // **** <a href={this.props.match} === {undefined}
    // **** linked to nowhere ... this will fix bug href="#/"
    return (
      <aside className="left-sidebar">
        <ProfilePane />
        <div className="scroll-sidebar">
          <nav className="sidebar-nav">
            <ul id="sidebarnav">
              <ul>
                {menus.map((menu) => {
                  return (
                    <li className="sidebar-item" key={menu.label}>
                      {menu.link ? (
                        <NavLink className="sidebar-link has-arrow" to={`/admin${menu.link}`}>
                          <i className={menu.icon}></i>
                          <span className="hide-menu">{menu.label.toUpperCase()} </span>
                        </NavLink>
                      ) : (
                        <a
                          name={menu.label}
                          className="sidebar-link has-arrow waves-effect waves-dark"
                          href={this.props.url}
                          aria-expanded="false">
                          <i className={menu.icon}></i>
                          <span className="hide-menu">{menu.label.toUpperCase()} </span>
                        </a>
                      )}
                      {menu.subMenu && (
                        <ul
                          name={menu.label}
                          aria-expanded="false"
                          className="collapse first-level">
                          {menu.subMenu.map((subMenu) => {
                            return (
                              <li className="sidebar-item" key={`${subMenu.key}`}>
                                <NavLink className="sidebar-link" to={`/admin${subMenu.link}`}>
                                  <i className="mdi mdi-layers" />
                                  {subMenu.label}
                                </NavLink>
                              </li>
                            )
                          })}
                        </ul>
                      )}
                    </li>
                  )
                })}
              </ul>
            </ul>
          </nav>
        </div>
      </aside>
    )
  }
}

const mapState = () => ({})

const mapDispatch = (dispatch) => {
  return { setPageTitle: dispatch.Title.setPageTitle }
}
export default connect(mapState, mapDispatch)(withRouter(Sidebar))

// { menu.subMenu && console.log(menu.subMenu.length)}
