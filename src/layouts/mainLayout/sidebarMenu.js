export const SideBarMenu = [
  {
    key: '1',
    label: 'User Management',
    icon: 'icon fas fa-user',
    subMenu: [
      // {
      //   key: '1',
      //   label: 'Admin Management',
      //   link: '/admin-management',
      // },
      // {
      //   key: '1',
      //   label: 'Admin management',
      //   link: '/maid-management',
      // },
      {
        key: '2',
        label: 'Member management',
        link: '/customer-management',
      },
    ],
  },
  {
    key: '2',
    label: 'Notification',
    icon: 'icon fas fa-bell',
    subMenu: [
      {
        key: '1',
        label: 'Notification',
        link: '/notification-management',
      },
    ],
  },
  {
    key: '3',
    label: 'FINANCE',
    icon: 'icon fas fa-hand-holding-usd',
    subMenu: [
      {
        key: '1',
        label: 'Payment',
        link: '/order-management/list/Parttime',
      },
      {
        key: '2',
        label: 'Receipt',
        link: '/order-management/list/Approve',
      },
    ],

  },
  {
    key: '4',
    label: 'ANALYSIS ',
    icon: 'icon fas fa-chart-line',
    subMenu: [
      {
        key: '1',
        label: 'Dashboard',
        link: '/analysis',
      },
    ],
  },

  // {
  //   key: '3',
  //   label: 'Order Management',
  //   link: '/order-management',
  // },
  // {
  //   key: '5',
  //   label: 'SETTING',
  //   icon: 'icon fas fa-cog',
  //   link: '/chatbox',
  //   subMenu: [
  //     // {
  //     //   key: '1',
  //     //   label: 'Profile Setting',
  //     //   link: '/profile-setting',
  //     // },
  //     // {
  //     //   key: '2',
  //     //   label: 'Roles Setting',
  //     //   link: '/roles-setting',
  //     // },
  //     {
  //       key: '3',
  //       label: 'Group Setting',
  //       link: '/group-setting',
  //     },
  //   ],
  // },
  // {
  //   key: '6',
  //   label: 'Content',
  //   icon: 'icon far fa-edit',
  //   subMenu: [
  //     // {
  //     //   key: '1',
  //     //   label: 'Content',
  //     //   link: '/content',
  //     // },
  //     // {
  //     //   key: '2',
  //     //   label: 'Polls & Forms',
  //     //   link: '/polls-forms',
  //     // },
  //     {
  //       key: '3',
  //       label: 'Broadcasting',
  //       link: '/broadcasting',
  //     },
  //     {
  //       key: '4',
  //       label: 'Promotion',
  //       link: '/promotion',
  //     },
  //   ],
  // },

  // {
  //   key: '7',
  //   label: 'Analysis',
  //   icon: 'icon fas fa-chart-line',
  //   subMenu: [
  //     {
  //       key: '1',
  //       label: 'Dashboard',
  //       link: '/dashboard',
  //     },
  //     {
  //       key: '2',
  //       label: 'Dashboard_Old',
  //       link: '/dashboard0',
  //     },
  //     {
  //       key: '2',
  //       label: 'Database',
  //       link: '/database',
  //     },
  //   ],
  // },
  // {
  //   key: '8',
  //   label: 'Group setting',
  //   icon: 'icon fas fa-cog',
  //   subMenu: [
  //     {
  //       key: '1',
  //       label: 'Profile Setting',
  //       link: '/profile-setting',
  //     },
  //     {
  //       key: '2',
  //       label: 'Roles Setting',
  //       link: '/roles-setting',
  //     },
  //     {
  //       key: '3',
  //       label: 'Group Setting',
  //       link: '/group-setting',
  //     },
  //   ],
  // },
]
