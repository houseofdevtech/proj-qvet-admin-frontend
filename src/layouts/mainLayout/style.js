import styled from 'styled-components'


export const Content = styled.div`
  background:#f0f3f4;
  width:100%;
  height:100%;
  position: absolute;
  left: 250px; 
  top: 60px;
  // left: 0;
  // right: 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    left: 0;
  }
`
