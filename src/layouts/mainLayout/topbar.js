import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Back from '../../images/back_reverse.svg'

import '../../style/style.custom.css'

const LogoContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`

const BackIcon = styled.img`
  margin: 0px 10px 8px 0px;
  cursor: pointer;
`

class Header extends Component {
  state = {
    isBack: true,
  }

  onClick = (e) => {
    e.preventDefault()
    // console.log('onclick..')
  }

  handleGoBack() {
    if (
      this.props.history.location.pathname ===
        '/admin/group-setting/setting-group-Of-user/setting-group-customer' ||
      this.props.history.location.pathname ===
        '/admin/group-setting/setting-group-Of-user/setting-group-maid'
    ) {
      this.props.history.push('/admin/group-setting/setting-group')
    } else if (
      this.props.history.location.pathname === '/admin/broadcasting/announceTemplate' ||
      this.props.history.location.pathname === '/admin/broadcasting/broadcastTemplate'
    ) {
      this.props.history.push('/admin/broadcasting/list/broadcastSent')
    } else {
      this.props.history.goBack()
    }
    this.props.showBackIcon(false)
    this.props.setCheckTypePath(true)
  }

  render() {
    const { title } = this.props
    // console.log(this.props)

    return (
      <div>
        <header className="topbar">
          <nav className={`navbar top-navbar navbar-expand-md navbar-dark 
          ${title === 'Receipt' ? 'bg-green': ''}
          ${title === 'Payment' ? 'bg-red': ''}`}>
            <div id="topbar-expand" className="navbar-header">
              {/* This is for the sidebar toggle which is visible on mobile only */}
              <a
                className="nav-toggler waves-effect waves-light d-block d-md-none"
                href={this.props.content}>
                <i className="ti-menu ti-close text-light" />
              </a>
              {/* ============================================================== */}
              {/* Logo */}
              {/* ============================================================== */}
              <LogoContainer>
                <a className="navbar-brand" href="#">
                  <span className="logo-text">
                    <div>QVETJOB ADMIN</div>
                  </span>
                </a>
              </LogoContainer>
            </div>
            <div
              className="navbar-collapse collapse"
              id="navbarSupportedContent"
              data-navbarbg="skin6">
              {/* <div className="navbar-collapse collapse" id="navbarSupportedContent" > */}
              {/* ============================================================== */}
              {/* toggle and nav items */}
              {/* ============================================================== */}
              <div className={`page-title ${title === 'Receipt' ? 'bg-green': ''} ${title === 'Payment' ? 'bg-red': ''}`}>
                {this.props.appState.isBack && (
                  <BackIcon onClick={() => this.handleGoBack()} alt="back-icon" src={Back} />
                )}
                <h3 className="font-white keep-word">{title}</h3>
              </div>
            </div>
          </nav>
        </header>
      </div>
    )
  }
}

const mapState = (state) => ({
  appState: state.Title,
})

const mapDispatch = (dispatch) => {
  return {
    showBackIcon: dispatch.Title.showBackIcon,
    setCheckTypePath: dispatch.promotion.setCheckTypePath,
  }
}

export default connect(mapState, mapDispatch)(withRouter(Header))
