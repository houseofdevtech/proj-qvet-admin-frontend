import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
import _ from 'lodash'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from '../../components/button'
import TabComponent from './../../components/tabComponent'
import styled from 'styled-components'

import Rightbar from './rightbar'
import './style.css'
import { get } from '../../utils/service'
import moment from 'moment'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

const BtnContainer = styled.div`
display: flex;
width: 100%;
justify-content: space-between;
height: 40px;
margin-bottom: 10px;
`

class MemberManagementLayout extends Component {
  state = {
    showRightbar: false,
    mode: 'column',
    maid:[],
    count: 0,
    resAnnouncement:[],
    cus: [],
    count2:0
  }

  componentDidMount() {
    this.props.showBackIcon(true)
    // this.props.getMaidList()
    // console.log(this.props)
    // this.state.showRightbar && document.getElementById('layout').classList.add('blur')
  }

  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    // Refresh Redux State to React table
    // by Refresh current Path
    const { history } = this.props
    history.push(this.props.history.location.pathname)
    // console.log(this.state.showRightbar)
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }

  onChangeTab = (path) => {
    // console.log(`onChangeTab : ${match.url + path}`)
    const { history, match } = this.props
    history.push(match.url + path)
  }
  searchHandler = async(e) => {
    e.persist()
    if(this.state.count !== e.target?.value?.length) {
      let result = await get('api/alluser')
      this.props.setM(result)
      this.setState({count:  e.target?.value?.length})
    }
    this.setState({maid: this.props.maidList})
    if(e.target.value.length !== 0 ) {
      const result = _.filter(this.props.maidList, (b) => {
        if(b.name.split(" ").join("").toLowerCase().toString().search(e.target.value.split(" ").join("").toLowerCase().toString()) >= 0) {
          return true
        }
      })
      console.log('result', result)
      this.props.setM(result)
    }

    console.log('customersList',this.props.customersList)
    console.log('maidList',this.props.maidList)
  }


  searchHandlerCompany = async(e) => {
    e.persist()
    if(this.state.count2 !== e.target?.value?.length) {
      let resp = await get('api/allcompany')

      this.props.setC(resp)
      this.setState({count2:  e.target?.value?.length})
    }
    console.log('this.props.customersList',this.props.customersList)
    if(e.target.value.length !== 0 ) {
      const result = _.filter(this.props.customersList, (b) => {
        if(b.company_name.split(" ").join("").toLowerCase().toString().search(e.target.value.split(" ").join("").toLowerCase().toString()) >= 0) {
          return true
        }
      })
      console.log('result2', result)
      this.props.setC(result)
    }

  }

  render() {
    // console.log(`customer : ${JSON.stringify(this.props.customers)}`)
    const tabData = [
      {
        label: 'รายชื่อผู้ประกอบการ',
        total: this.props.customerList,
        key: '0',
        path: '/employer',
      },
      {
        label: 'รายชื่อพนักงาน',
        total: this.props.maidsLsit,
        key: '1',
        path: '/customer',
      },
    ]
    const currentLocation = this.props.history.location.pathname
    console.log('window', window.location.pathname.split('/'))
    return (
      <div>
        {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
        <div className="page-breadcrumb">
          {window.location?.pathname?.split('/')[4] !== 'company' &&
          window.location?.pathname?.split('/')[4] !== 'employee' &&
            <BtnContainer>
            <Button
              style={{ width: 'auto' }}
              className="btn"
              label="Column setting"
              onClick={() => this.handleColumn()}
            />

            {/*<Button*/}
            {/*  style={{ width: 'auto' }}*/}
            {/*  className="btn"*/}
            {/*  label="Filter by"*/}
            {/*  onClick={() => this.handleFilter()}*/}
            {/*/>*/}
              {window.location?.pathname?.split('/')[3] !== 'employer' ?
                <input className="search-bar"
                       placeholder="ค้นหาพนักงาน" onChange={this.searchHandler}/> :<input className="search-bar" placeholder="ค้นหาผู้ประกอบการ" onChange={this.searchHandlerCompany}/> }
          </BtnContainer>
          }
          {window.location?.pathname?.split('/')[4] !== 'company' &&
            window.location?.pathname?.split('/')[4] !== 'employee' && (
              <TabComponent
                transparent
                style={{}}
                tabData={tabData}
                onChangeTab={this.onChangeTab}
              />
            )}
          {this.props.children}
        </div>
        <Rightbar data={this.state} currentLocation={currentLocation} />
        {/* {console.log(`current : ${JSON.stringify(this.props.history.location.pathname)}`)} */}
      </div>
    )
  }
}

const mapState = (state) => ({
  maidList: state.Title.maidList,
  customersList: state.Title.customerList,
  maidsLsit: state.maid.maidsLsit,
  maidColumn: state.maid.column,
  customerList: state.customer.customerList,
  customerColumn: state.customer.column,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    setM: dispatch.Title.setM,
    setC: dispatch.Title.setC,
    showBackIcon: dispatch.Title.showBackIcon,
    getMaidList: dispatch.maid.getMaidList,
    getCustomerList: dispatch.customer.getCustomerList,
  }
}

export default connect(mapState, mapDispatch)(withRouter(MemberManagementLayout))
