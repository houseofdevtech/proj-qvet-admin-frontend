import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from '../../components/button'
import TabComponent from './../../components/tabComponent'
import styled from 'styled-components'

import Rightbar from './rightbar'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`

class NotficationLayout extends React.Component {
  state = {
    showRightbar: false,
    mode: 'column',
  }
  componentDidMount() {
    this.props.showBackIcon(true)
    // this.props.getCustomerList()
    // console.log(this.props)
    // this.state.showRightbar && document.getElementById('layout').classList.add('blur')
  }
  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    // Refresh Redux State to React table
    // by Refresh current Path
    const { history } = this.props
    history.push(this.props.history.location.pathname)
    // console.log(this.state.showRightbar)
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }

  onChangeTab = (path) => {
    // console.log(`onChangeTab : ${match.url + path}`)
    const { history, match } = this.props
    history.push(match.url + path)
  }
  render() {
    // console.log(`customer : ${JSON.stringify(this.props.customers)}`)
    const tabData = [
      // {
      //   label: 'รายชื่อผู้ประกอบการ',
      //   total: this.props.customerList,
      //   key: '0',
      //   path: '/maid',
      // },
      {
        label: 'แจ้งเตือนสร้างงานด่วน',
        total: this.props.maidsLsit,
        key: '1',
        path: '/apply-job',
      },
      // {
      //   label: 'แจ้งเตือนพนักงานยกเลิกงาน',
      //   total: this.props.maidsLsit,
      //   key: '2',
      //   path: '/cancle-job',
      // },
    ]
    const currentLocation = this.props.history.location.pathname

    return (
      <div>
        {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
        <div className="page-breadcrumb">
          {/*<div className="btn-container" style={{ height: '40px' }}>*/}
          {/*  <Button*/}
          {/*    style={{ width: 'auto' }}*/}
          {/*    className="btn"*/}
          {/*    label="Column setting"*/}
          {/*    onClick={() => this.handleColumn()}*/}
          {/*  />*/}
          {/*  <Button*/}
          {/*    style={{ width: 'auto' }}*/}
          {/*    className="btn"*/}
          {/*    label="Filter by"*/}
          {/*    onClick={() => this.handleFilter()}*/}
          {/*  />*/}
          {/*</div>*/}

          <TabComponent transparent style={{}} tabData={tabData} onChangeTab={this.onChangeTab} />
          {this.props.children}
        </div>
        <Rightbar data={this.state} currentLocation={currentLocation} />
        {/* {console.log(`current : ${JSON.stringify(this.props.history.location.pathname)}`)} */}
      </div>
    )
  }
}

const mapState = (state) => ({
  maidsLsit: state.maid.maidsLsit,
  maidColumn: state.maid.column,
  customerList: state.customer.customerList,
  customerColumn: state.customer.column,
})

const mapDispatch = (dispatch) => {
  return {
    setPageTitle: dispatch.Title.setPageTitle,
    showBackIcon: dispatch.Title.showBackIcon,
    getMaidList: dispatch.maid.getMaidList,
    getCustomerList: dispatch.customer.getCustomerList,
  }
}

export default connect(mapState, mapDispatch)(withRouter(NotficationLayout))
