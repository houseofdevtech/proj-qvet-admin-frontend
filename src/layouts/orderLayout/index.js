import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '../../components/button'
import { ExportToCsv } from 'export-to-csv'
import TabComponent from './../../components/tabComponent'
import Rightbar from './rightbar'
import styled from 'styled-components'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`
class Orderlayout extends Component {
  constructor() {
    super()
    this.state = {
      totalIN: [],
      totalAC: 0,
      totalEX: 0,
      showRightbar: false,
      mode: 'column',
    }
  }

  componentDidMount() {}

  handleToggle = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
    // Refresh Redux State to React table
    // by Refresh current Path
    const { history } = this.props
    history.push(this.props.history.location.pathname)
    // console.log(this.state.showRightbar)
  }

  handleColumn = () => {
    this.setState({ showRightbar: true, mode: 'column' })
  }
  handleFilter = () => {
    this.setState({ showRightbar: true, mode: 'filter' })
  }

  exportExcel = () => {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      filename: this.props.pageTitle !== 'Receipt' ? 'PaymentList' : 'ReceiptList',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: this.props.pageTitle !== 'Receipt' ? 'Payment' : 'Receipt',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    }

    const csvExporter = new ExportToCsv(options)

    if (this.props.employeeData?.length > 0) {
      csvExporter.generateCsv(this.props.employeeData)
    }
  }
  render() {
    // const tabData = [
    //   {
    //     label: 'รายชื่อผู้ประกอบการ',
    //     total: this.props.customerList,
    //     key: '0',
    //     path: '/maid',
    //   },
    //   {
    //     label: 'รายชื่อพนักงาน',
    //     total: this.props.maidsLsit,
    //     key: '1',
    //     path: '/customer',
    //   },
    // ]
    const currentLocation = this.props.history.location.pathname
    return (
      <div>
        {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}

        <div className="page-breadcrumb">
          <div className="btn-container justify-content-between" style={{ height: '40px' }}>
            <div className="d-flex">
              <Button
                style={{ width: 'auto' }}
                className="btn"
                label="Column setting"
                onClick={() => this.handleColumn()}
              />
              {/*<Button*/}
              {/*  style={{ width: 'auto' }}*/}
              {/*  className="btn"*/}
              {/*  label="Filter by"*/}
              {/*  onClick={() => this.handleFilter()}*/}
              {/*/>*/}
            </div>
            <div className="d-flex">
              <button
                style={{ width: 'auto', backgroundColor: '#8a296d', color: 'white' }}
                className="btn waves-effect waves-light btn-rounded"
                onClick={() => this.exportExcel()}>
                <svg
                  width="1em"
                  height="1em"
                  viewBox="0 0 16 16"
                  className="bi bi-file-earmark-spreadsheet-fill mr-2"
                  fill="currentColor"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    fill-rule="evenodd"
                    d="M2 3a2 2 0 0 1 2-2h5.293a1 1 0 0 1 .707.293L13.707 5a1 1 0 0 1 .293.707V13a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3zm7 2V2l4 4h-3a1 1 0 0 1-1-1zM3 8v1h2v2H3v1h2v2h1v-2h3v2h1v-2h3v-1h-3V9h3V8H3zm3 3V9h3v2H6z"
                  />
                </svg>
                Export to Excel
              </button>
              {/*{  <CSVLin data={this.state.dd}>Download me</CSVLink>}*/}
            </div>
          </div>
          {this.props.children}
        </div>
        <Rightbar data={this.state} currentLocation={currentLocation} />
      </div>
    )
  }
}

const mapState = (state) => ({
  pageTitle: state.Title.pageTitle,
  employerData: state.orderManagement.employerData,
  EmMoney: state.orderManagement.EmMoney,
  employeeData: state.orderManagement.employeeData,
  orderMode: state.orderManagement.orderMode,
  compMoney: state.orderManagement.money,
  compMoneyTotalVat: state.orderManagement.moneyTotalVat,
  dataOrderApprove: state.orderManagement.dataOrderApprove,
  dataOrderPayment: state.orderManagement.dataOrderPayment,
  dataOrderBooking: state.orderManagement.dataOrderBooking,
  customerColumn: state.customer.column,
})

const mapDispatch = (dispatch) => {
  return {
    setCheckTypePath: dispatch.promotion.setCheckTypePath,
    setPageTitle: dispatch.Title.setPageTitle,
    getorDerApprove: dispatch.orderManagement.getOrderApproveList,
    getOrderPaymentList: dispatch.orderManagement.getOrderPaymentList,
    getOrderBookList: dispatch.orderManagement.getOrderBookList,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
  }
}
export default connect(mapState, mapDispatch)(Orderlayout)
