import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ExportToCsv } from 'export-to-csv'
import { json2csv } from 'json2csv'
import Button from '../../components/button'
import TabComponent from './../../components/tabComponent'
import Rightbar from './rightbar'
import styled from 'styled-components'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import './orderManage.css'

const Overlay = styled.div`
  content: ' ';
  z-index: 10;
  display: block;
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  background: rgba(255, 255, 255, 0.8);
`
const Div = styled.div`
  background-image: linear-gradient(to bottom, #8a296d, #b585a5);
`
const Text = styled.p`
  font-size: 16px;
  display: flex;
  color: black;
  align-items: center;
`
const Total = styled.div`
  position: relative;
  display: flex;
  z-index: 999;
  justify-content: end !important;
`

const Box = styled.div`
  display: flex;
  margin-left: 10px;
  background-color: white;
  width: 182px;
  height: 75%;
  border-radius: 4px;
  justify-content: center;
  //padding-left: 20px;
  //padding-right: 20px;
  //padding-top: 20px;
`

const BoxRep = styled.div`
  display: flex;
  margin-left: 10px;
  background-color: white;
  margin-bottom: 10px;
  width: 182px;
  height: 35px;
  border-radius: 4px;
  justify-content: center;
  //padding-left: 20px;
  //padding-right: 20px;
  //padding-top: 20px;
`

class orderManagementLayout extends Component {
  _isMounted = false

  constructor() {
    super()
    this.state = {
      totalIN: [],
      totalAC: 0,
      firstMonday: '',
      totalEX: 0,
      csvData: [
        ['firstname', 'lastname', 'email'],
        ['Ahmed', 'Tomi', 'ah@smthing.co.com'],
        ['Raed', 'Labes', 'rl@smthing.co.com'],
        ['Yezzi', 'Min l3b', 'ymin@cocococo.com'],
      ],
      showRightbar: false,
      start_datetime: new Date(),
      end_datetime: '',
      listWeekPayment: [],
      weekValue: '',
      yearList: [],
      yaerValue: '',
      dd: {},
    }
  }

  componentDidUpdate(prevProps) {
    // console.log('monday', this.state.firstMonday);
    if (
      prevProps.orderMode !== this.props.orderMode &&
      this.props.orderMode !== undefined &&
      this.props.orderMode !== ''
    ) {
      this.setDateLayout()
    }
    // const data = json2csv(this.props.employeeData)
    // console.log('dda')
    // this.setState({dd:data})
  }

  async componentDidMount() {
    this.setState({ yearList: [] })
    console.log('this.props.pageTitle')

    await this.setDateLayout()
  }

  componentWillUnmount() {
    this._isMounted = false
    this.setState({
      listWeekPayment: [],
      weekValue: '',
    })
  }

  handleColumn = () => {
    this._isMounted = true
    if (this._isMounted) {
      this.setState({ showRightbar: true, mode: 'column' })
    }
  }
  exportExcel = () => {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      filename: this.props.pageTitle !== 'Receipt' ? 'PaymentList' : 'ReceiptList',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: this.props.pageTitle !== 'Receipt' ? 'Payment' : 'Receipt',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    }

    const csvExporter = new ExportToCsv(options)

    if (this.props.employeeData?.length > 0) {
      csvExporter.generateCsv(this.props.employeeData)
    }
  }
  handleFilter = () => {
    this._isMounted = true
    if (this._isMounted) {
      this.setState({ showRightbar: true, mode: 'filter' })
    }
  }

  onChangeTab = (path) => {
    const { history, match } = this.props
    const params = {
      start_datetime: this.state.start_datetime,
      end_datetime: this.state.end_datetime,
    }
    history.push(match.url + path, params)
  }

  onChangeTabDetail = (path) => {
    const { history } = this.props
    history.push('/admin/order-management' + path)
    this.props.setCheckTypePath(false)
  }

  async SelectDatePyment(el) {
    this.setState({ weekValue: el })
    let temp = el.split(' - ')
    let temp1 = temp[0]
    let temp2 = temp[1]
    this.props.setStartdateOrder(temp1)
    this.props.setEndateOrder(temp2)
  }

  handleToggle = (key) => {
    this._isMounted = true
    if (this._isMounted) {
      this.setState((prevState) => ({ [key]: !prevState[key] }))
      const { history } = this.props
      history.push(this.props.history.location.pathname)
    }
  }

  handleChangeStartWeekDate = (e) => {
    this.setState({
      firstMonday: moment(e).weekday(1).toDate(),
      end_datetime: moment(e).day(-7).weekday(7).toDate(),
    })
    // console.log('time change: ', moment(e).day(-7).weekday(1).toDate());
    this.props.setStartdateOrder(moment(e).day(-7).weekday(1).toDate())
    this.props.setEndateOrder(moment(e).day(-7).weekday(7).toDate())
  }

  async setDateLayout() {
    const d = new Date()
    const n = d.getDate()
    // yearList
    let date = moment()
      .set('year', moment().year())
      .set('month', moment().month())
      .set('date', moment().startOf('month').format('YYYY-DD-MM'))
      .isoWeekday(1)
    if (date.date() === 8) {
      //
      date = date.isoWeekday(-6)
    }
    date = date.startOf('isoWeek')

    this.setState({ firstMonday: new Date(date) })
    var nYear = await d.getFullYear()
    var num = nYear - 5
    this.setState({ yaerValue: nYear })
    if (num) {
      if (this.state.yearList.length <= 5) {
        for (var i = num; i <= nYear; i++) {
          this.state.yearList.push(i)
        }
      }
    }
    if (n <= 15) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 1).format()),
        end_datetime: new Date(moment().set('date', 15).format()),
      })
      console.log('bbb', this.state.start_datetime, 'bbbb', this.state.firstMonday)
    } else if (n >= 16) {
      await this.setState({
        start_datetime: new Date(moment().set('date', 16).format()),
        end_datetime: new Date(moment(d).endOf('month').format()),
      })
    }

    this.handleChangeYear()
  }

  async handleChangeYear(year) {
    this.setState({ yaerValue: year })
    this._isMounted = true
    if (this._isMounted) {
      var d = new Date()
      var y = d.getFullYear()
      let Tempyaer = 0
      if (year !== undefined) {
        Tempyaer = year
      } else {
        Tempyaer = y
      }
      let array = []
      let temparray = []
      let temMonth = Tempyaer % 4
      if (Tempyaer) {
        for (let i = 0; i < 12; i++) {
          if (i < 9) {
            if (i === 1) {
              if (temMonth === 0) {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-29`
                )
              } else {
                array.push(
                  `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                    i + 1
                  }-16 - ${Tempyaer}-0${i + 1}-28`
                )
              }
            } else if (i === 0 || i === 2 || i === 4 || i === 6 || i === 7 || i === 9) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-31`
              )
            } else if (i === 3 || i === 5 || i === 8) {
              array.push(
                `${Tempyaer}-0${i + 1}-01 - ${Tempyaer}-0${i + 1}-15,${Tempyaer}-0${
                  i + 1
                }-16 - ${Tempyaer}-0${i + 1}-30`
              )
            }
          } else {
            if (i === 9 || i === 11) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-31`
              )
            } else if (i === 10) {
              array.push(
                `${Tempyaer}-${i + 1}-01 - ${Tempyaer}-${i + 1}-15,${Tempyaer}-${
                  i + 1
                }-16 - ${Tempyaer}-${i + 1}-30`
              )
            }
          }
        }
        if (array.length >= 12) {
          for (let i = 0; i < array.length; i++) {
            temparray.push(...array[i].split(','))
          }
        }
      }
      this.setState({
        listWeekPayment: temparray,
        weekValue: `${moment(this.state.start_datetime).format('YYYY-MM-DD')} - ${moment(
          this.state.end_datetime
        ).format('YYYY-MM-DD')}`,
      })
    }
  }

  handleChangeMonth = e => {
    // console.log('date', moment(e));
    // console.log('date start: ', moment(e).format('YYYY-MM-DD'));
    // console.log('date end', moment(e).endOf('month').format('YYYY-MM-DD'));
    this.setState({
      start_datetime: e,
      end_datetime: moment(e).endOf('month').toDate()
    })
    // console.log('time change: ', moment(e).day(-7).weekday(1).toDate());
    this.props.setStartdateOrder(e)
    this.props.setEndateOrder(moment(e).endOf('month').toDate())
  }

  handleChangeStartDate = (e) => {
    console.log('hhh')
    console.log(e)
    this.setState({ start_datetime: e })
    this.props.setStartdateOrder(e)
  }

  handleChangeEndDate = (e) => {
    // console.log('time check',e);
    this.setState({ end_datetime: e })
    this.props.setEndateOrder(e)
  }

  render() {
    const tabData = [
      {
        label: 'Full Time',
        total: this.props.dataOrderBooking,
        key: '0',
        path: '/Fulltime',
      },
      {
        label: 'Part Time',
        total: this.props.dataOrderApprove,
        key: '1',
        path: '/Parttime',
      },
    ]

    const isWeekday = (date) => {
      const day = date.getDay()
      return day === 1
    }

    const currentLocation = this.props.history.location.pathname
    return (
      <div>
        {this.state.showRightbar && <Overlay onClick={() => this.handleToggle('showRightbar')} />}
        <div>
          <div className="col-md-12 mb-3" style={{ lineHeight: '60px' }}>
            {this.props.orderMode === 'Payment' ? (
              <Div className="d-flex">
                <div className="d-flex justify-content-between col-md-6 row">
                  <div className="col-md-4">
                    <label
                      style={{
                        fontSize: '13px',
                        fontWeight: '500px',
                        color: 'white',
                        lineHeight: '2.4',
                      }}>
                      วันสรุปรอบการชำระเงิน
                    </label>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8 col-xl-8" style={{ zIndex: 9 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={moment(this.state.start_datetime)}
                      // showTimeSelect
                      name="start_datetime"
                      selected={this.state.start_datetime ? this.state.start_datetime : null}
                      // onChange={this.handleChangeStartDate}
                      onChange={this.handleChangeMonth}
                      // timeFormat="p"
                      // timeIntervals={15}
                      dateFormat="MM/yyyy"
                      showMonthYearPicker
                      showFullMonthYearPicker
                    />
                  </div>

                  <div className="col-md-4">
                    <label style={{ fontSize: '13px', fontWeight: '500px', color: 'white' }}>
                      สรุปจากวันที่
                    </label>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8 col-xl-8" style={{ zIndex: 2 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={
                        // moment(this.state.firstMonday).format('YYYY/MM/DD') +
                        moment(this.state.start_datetime).format('YYYY/MM/DD') +
                        ' - ' +
                        moment(this.state.end_datetime).format('YYYY/MM/DD')
                      }
                      // showTimeSelect
                      name="end_datetime"
                      selected={this.state.end_datetime ? this.state.end_datetime : null}
                      onChange={this.handleChangeEndDate}
                      disabled
                      // timeFormat="p"
                      timeIntervals={15}
                      dateFormat=" yyyy-MM-dd "
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex align-items-center justify-content-end">
                  <div className="d-flex justify-content-end ">
                    <div className="d-flex justify-content-around">
                      <p style={{ color: 'white' }}>ยอดรวมทั้งหมด</p>
                      <Box>
                        <Text className="mt-3">
                          {' '}
                          {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                            this.props.EmMoney.EmMoney
                          )}{' '}
                          บาท
                        </Text>
                      </Box>
                    </div>
                  </div>
                </div>
              </Div>
            ) : this.props.orderMode === 'Approve' ? (
              <Div className="p-2 d-flex align-items-center">
                <div>
                  <div className="d-flex flex-column" style={{ flexGrow: '.4' }}>
                    <label style={{ fontSize: '13px', fontWeight: '500px', color: 'white' }}>
                      วันสรุปรอบการชำระเงิน (ทุกวันที่ 1)
                    </label>
                    <label style={{ fontSize: '13px', fontWeight: '500px', color: 'white' }}>
                      สรุปจากวันที่
                    </label>
                  </div>
                </div>
                <div className="d-flex flex-column" style={{ flexGrow: '.4' }}>
                  <div className="col-xs-12 col-sm-12 col-md col-xl-8" style={{ zIndex: 10 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={this.state.start_datetime}
                      // showTimeSelect
                      name="start_datetime"
                      selected={this.state.start_datetime ? this.state.start_datetime : null}
                      onChange={this.handleChangeStartDate}
                      // timeFormat="p"
                      // timeIntervals={15}
                      dateFormat="MM/yyyy"
                      showMonthYearPicker
                      showFullMonthYearPicker
                      // showMonthDropdown
                      // dateFormat=" yyyy-MM-dd "
                    />
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8 col-xl-8" style={{ zIndex: 9 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={this.state.end_datetime}
                      // showTimeSelect
                      name="end_datetime"
                      selected={this.state.end_datetime ? this.state.end_datetime : null}
                      onChange={this.handleChangeEndDate}
                      // timeFormat="p"
                      timeIntervals={15}
                      dateFormat=" yyyy-MM-dd "
                    />
                  </div>
                </div>
                <div
                  className="d-flex flex-column justify-content-between"
                  style={{ flexGrow: '.4' }}>
                  <label
                    className="d-flex"
                    style={{
                      fontSize: '13px',
                      fontWeight: '500px',
                      color: 'white',
                      marginBottom: '-20px',
                    }}>
                    สรุปรายได้ทั้งหมด
                  </label>
                  <label
                    style={{
                      fontSize: '13px',
                      fontWeight: '500px',
                      color: 'white',
                      marginBottom: '-20px',
                    }}>
                    ภาษีมูลค่าเพิ่ม 7%
                  </label>
                  <label style={{ fontSize: '13px', fontWeight: '500px', color: 'white' }}>
                    จำนวนเงินรวมภาษีมูลค่าเพิ่ม 7%
                  </label>
                </div>
                <div className="d-flex flex-column justify-content-between">
                  <BoxRep>
                    <label className="d-flex align-items-center mb-0">
                      {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                        this.props.compMoney?.money
                      )}
                    </label>
                  </BoxRep>
                  <BoxRep>
                    <label className="d-flex align-items-center mb-0">
                      {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                        this.props.compMoney?.moneyVx
                      )}
                    </label>
                  </BoxRep>
                  <BoxRep>
                    <label className="d-flex align-items-center mb-0">
                      {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                        this.props.compMoney?.moneyVat
                      )}
                    </label>
                  </BoxRep>
                </div>
              </Div>
            ) : (
              <Div className="d-flex">
                <div className="d-flex justify-content-between col-md-6 row">
                  <div className="col-md-4">
                    <label
                      style={{
                        fontSize: '13px',
                        fontWeight: '500px',
                        color: 'white',
                        lineHeight: '2.4',
                      }}>
                      วันสรุปรอบการชำระเงิน (ทุกวันจันทร์)
                    </label>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8 col-xl-8" style={{ zIndex: 9 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={this.state.firstMonday}
                      // showTimeSelect
                      name="start_datetime"
                      selected={this.state.firstMonday ? this.state.firstMonday : null}
                      // onChange={this.handleChangeStartDate}
                      onChange={this.handleChangeStartWeekDate}
                      // timeFormat="p"
                      filterDate={isWeekday}
                      timeIntervals={15}
                      dateFormat=" yyyy-MM-dd "
                    />
                  </div>

                  <div className="col-md-4">
                    <label style={{ fontSize: '13px', fontWeight: '500px', color: 'white' }}>
                      สรุปจากวันที่
                    </label>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8 col-xl-8" style={{ zIndex: 2 }}>
                    <DatePicker
                      className="Rectangle-Datepicker-promotion"
                      value={
                        // moment(this.state.firstMonday).format('YYYY/MM/DD') +
                        moment(this.state.firstMonday).day(-6).format('YYYY/MM/DD') +
                        ' - ' +
                        moment(this.state.end_datetime).format('YYYY/MM/DD')
                      }
                      // showTimeSelect
                      name="end_datetime"
                      selected={this.state.end_datetime ? this.state.end_datetime : null}
                      onChange={this.handleChangeEndDate}
                      disabled
                      // timeFormat="p"
                      timeIntervals={15}
                      dateFormat=" yyyy-MM-dd "
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex align-items-center justify-content-end">
                  <div className="d-flex justify-content-end ">
                    <div className="d-flex justify-content-around">
                      <p style={{ color: 'white' }}>ยอดรวมทั้งหมด</p>
                      <Box>
                        <Text className="mt-3">
                          {' '}
                          {Intl.NumberFormat('th-TH', { currency: 'THB' }).format(
                            this.props.EmMoney.EmMoney
                          )}{' '}
                          บาท
                        </Text>
                      </Box>
                    </div>
                  </div>
                </div>
              </Div>
            )}
          </div>

          {this.props.pageTitle !== 'Receipt' && (
            <TabComponent transparent tabData={tabData} onChangeTab={this.onChangeTab} />
          )}
          <Rightbar data={this.state} currentLocation={currentLocation} />

          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapState = (state) => ({
  pageTitle: state.Title.pageTitle,
  employerData: state.orderManagement.employerData,
  EmMoney: state.orderManagement.EmMoney,
  employeeData: state.orderManagement.employeeData,
  orderMode: state.orderManagement.orderMode,
  compMoney: state.orderManagement.money,
  compMoneyTotalVat: state.orderManagement.moneyTotalVat,
  dataOrderApprove: state.orderManagement.dataOrderApprove,
  dataOrderPayment: state.orderManagement.dataOrderPayment,
  dataOrderBooking: state.orderManagement.dataOrderBooking,
})

const mapDispatch = (dispatch) => {
  return {
    setCheckTypePath: dispatch.promotion.setCheckTypePath,
    setPageTitle: dispatch.Title.setPageTitle,
    getorDerApprove: dispatch.orderManagement.getOrderApproveList,
    getOrderPaymentList: dispatch.orderManagement.getOrderPaymentList,
    getOrderBookList: dispatch.orderManagement.getOrderBookList,
    setOrderMode: dispatch.orderManagement.setOrderMode,
    setStartdateOrder: dispatch.orderManagement.setStartdateOrder,
    setEndateOrder: dispatch.orderManagement.setEndateOrder,
    setWorkTime: dispatch.orderManagement.setWorkTime,
  }
}
export default connect(mapState, mapDispatch)(orderManagementLayout)
