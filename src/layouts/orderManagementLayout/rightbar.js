import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import RightbarCollumSetting from './rightbarCollumApprove'
import RightbarCollumPayment from './rightbarCollumPayment'
import RightbarCollumBooking from './rightbarCollumBooking'
import RightbarCollumPaymentFillter from './rightbarCollumPaymentFillter'
import RightbarFilter from './rightbarFilter'

const Container = styled.div`
  display: flex;
  flex-direction: column;
`
const HeaderText = styled.h3`
  padding: 10px 15px;
  color: white;
  font-weight: 200;
`

class RightbarOrderManagement extends Component {
  render() {
    return (
      <div>
        <div
          id="rightbar"
          className={this.props.data.showRightbar ? 'rightbar show-service-panel' : 'rightbar'}>
          <div className="rightbar-body">
            <Container>
              <HeaderText>
                {this.props.data.mode === 'column' ? 'Column Setting' : 'Filter By'}
              </HeaderText>
              {this.props.currentLocation.includes('Approve') ? (
                <RightbarCollumSetting mode={this.props.data.mode} />
              ) : 
              this.props.currentLocation.includes('Payment') ? (
                <RightbarCollumPayment mode={this.props.data.mode} />
              ) :
              this.props.currentLocation.includes('Booking') ? (
                <RightbarCollumBooking mode={this.props.data.mode} />
              ) : 
              this.props.currentLocation.includes('orderManagementFilter') ? (
                <RightbarCollumPaymentFillter mode={this.props.data.mode} />
              ) :(
                <RightbarFilter mode={this.props.data.mode} />
              )}
            </Container>
          </div>
        </div>
      </div>
    )
  }
}
// container={() => (useContainer ? this._container : document.body)}

const mapState = (state) => ({
  maidColumn: state.maid.column,
  customerColumn: state.customer.column,
})

const mapDispatch = (dispatch) => {
  return {
    setMaidColumnList: dispatch.maid.setMaidColumnList,
  }
}

export default connect(
  mapState,
  mapDispatch
)(RightbarOrderManagement)
