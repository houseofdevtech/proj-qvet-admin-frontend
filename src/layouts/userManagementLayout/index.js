import React, { Component } from 'react'

export default class UserManagementLayout extends Component {
  render() {
    return (
      <div>
      {this.props.children}
      </div>
    )
  }
}
