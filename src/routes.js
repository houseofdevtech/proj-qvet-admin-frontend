import React from 'react'
import {
  BrowserRouter,
  // HashRouter,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom'

import PrivateRoute from './components/privateRoute/loadable'
import NotFoundPage from './views/notFoundPage/loadable'

import AdminMainLayout from './layouts/mainLayout'
// import HomePage from './containers/homePage/index'

// import UserManagementLayout from './layout/userManagementLayout'
import AdminManagementPage from './containers/adminManagementPage'

// import MemberManagementPage from './containers/memberManagementPage'
import MemberManagementLayout from './layouts/memberManagementLayout'
import MemberCustomerPage from './containers/memberCustomerPage'

import maidLayout from './layouts/maidLayout'
import maidManagementLayout from './layouts/maidManagementLayout'
import MemberMaidPage from './containers/memberMaidPage'
import maidProfile from './containers/maidProfile'
import customerProfile from './containers/customerProfile'
// import MemberMaidHardPage from './containers/memberMaidPage/indexHard'

import inventoriesLayout from './layouts/inventoriesLayout'
import InvencalendarInventory from './containers/InvencalendarInventory'

import orderLayout from './layouts/orderLayout/index'
import orderManagementLayout from './layouts/orderManagementLayout/index'
import OrderManagementApprove from './containers/orderManagementApprove/index'
import orderManagementPayment from './containers/orderManagementPayment/index'
import OrderManagementBooking2 from './containers/OrdermanagementBooking2/index'
import OrderManagementBooking from './containers/orderManagementBooking/index'
import orderManagementFilter from './containers/orderManagementFilter/index'
import orderBookingProfile from './containers/orderBookingProfile/index'

import ContentManagementLayout from './layouts/contentManagementLayout'
import ContentPublishedPage from './containers/contentPublishedPage'

import broadcastLayout from './layouts/broadcastLayout/index'
import broadcastManagementLayout from './layouts/broadcastManagementLayout'
import broadcastSent from './containers/broadcastSent/index'
import broadcastError from './containers/broadcastError/index'
import broadcastTemplate from './containers/broadcastTemplate/index'
import broadcastAnnounceTemplate from './containers/broadcastAnnounceTemplate/index'

import promotionLayout from './layouts/promotionLayout/index'
import promotionManagementLayout from './layouts/promotionManagementLayout/index'
import promotionAdd from './containers/promotionAdd/index'
import promotionInactive from './containers/promotionInactive/index'
import PromotionActived from './containers/promotionActived/index'
import promotionExpired from './containers/promotionExpired/index'

import ChatBoxPage from './containers/chatBoxPage'
import DashboardPage from './containers/dashboardPage'

import DashboardLayout from './layouts/dashboardLayout'
import DashboardOverallPage from './containers/dashboardOverallPage'
import DashboardCustomerPage from './containers/dashboardCustomerPage'

import DatabaseLayout from './layouts/databaseLayout'
import DatabaseMaidPage from './containers/databaseMaidPage'

import NotificationLayout from './layouts/notificationLayout'
import NotificationPage from './containers/notificationPage'
import NotificationCancle from './containers/notificationCancle'

import AnalysisLayout from './layouts/analysisLayout'
import AnalysisPage from './containers/analysisPage'

// import SettingLayout from './layouts/settingLayout'
import SettingPage from './containers/settingPage'
import SettingProfilePage from './containers/settingProfilePage'
import SettingRolesPage from './containers/settingRolesPage'

import SettingGroupLayout from './layouts/settingGroupLayout/index'
import SettingGroupPage from './containers/settingGroupPage/index'
import SettingGroupOfUser from './containers/settingGroupOfUser/index'
import settingGroupCustomer from './containers/settingGroupOfUserCustomer/index'
import settingGroupMaid from './containers/settingGroupOfUsermaid/index'

import ReceiptProfile from './containers/ReceiptProfile'

const adminRoutes = {
  path: '/admin',
  layout: AdminMainLayout,
  routes: [
    // ##### Here is Redirect Pattern Route #####
    // {
    //   exact: true,
    //   subpath: '/',
    //   render: ({ match }) => <Redirect to={`${match.url}/login`} />,
    // },
    {
      // ##### Here is set to Lannding ... Can be set to another page for testing ######
      exact: true,
      subpath: '/',
      render: ({ match }) => <Redirect to={`${match.url}/customer-management`} />,
      // component: HomePage,
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/admin-management',
      component: AdminManagementPage,
    },
    // {
    //   isPrivate: false,
    //   exact: true,
    //   subpath: '/member-management',
    //   component: MemberManagementPage,
    // },
    {
      path: '/admin/customer-management',
      layout: MemberManagementLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/customer`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/customer',
          component: MemberCustomerPage,
        },
        {
          subpath: '/profile/employee/:id/:uId',
          component: maidProfile,
        },
        {
          subpath: '/profile/company/:id',
          component: customerProfile,
        },
        {
          subpath: '/employer',
          component: MemberMaidPage,
        },
      ],
    },
    {
      path: '/admin/notification-management',
      layout: NotificationLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/apply-job`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/apply-job',
          component: NotificationPage,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/cancle-job',
          component: NotificationCancle,
        },
      ],
    },
    {
      path: '/admin/analysis',
      layout: AnalysisLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/dashboard`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/dashboard',
          component: AnalysisPage,
        },
      ],
    },
    {
      path: '/admin/maid-management',
      layout: maidLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/list`} />,
        },
        {
          path: '/admin/maid-management/list',
          layout: maidManagementLayout,
          routes: [
            {
              exact: true,
              subpath: '/',
              render: ({ match }) => <Redirect to={`${match.url}/maid`} />,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/maid',
              component: MemberMaidPage,
            },
          ],
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/maidProfile',
          component: maidProfile,
        },
      ],
    },
    {
      path: '/admin/inventories-management',
      layout: inventoriesLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/inventories`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/inventories',
          component: InvencalendarInventory,
        },
      ],
    },
    {
      //
      //
      path: '/admin/order-management',
      layout: orderLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/list`} />,
        },
        {
          path: '/admin/order-management/list',
          layout: orderManagementLayout,
          routes: [
            {
              exact: true,
              subpath: '/',
              render: ({ match }) => <Redirect to={`${match.url}/Parttime`} />,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/Parttime',
              component: OrderManagementBooking2,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/Fulltime',
              component: OrderManagementBooking,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/Approve',
              component: OrderManagementApprove,
            },
            {
              subpath: '/profile/:id',
              component: ReceiptProfile,
            },
          ],
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/orderManagementFilter',
          component: orderManagementFilter,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/orderBookingProfile',
          component: orderBookingProfile,
        },
      ],
    },

    {
      path: '/admin/content',
      layout: ContentManagementLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/published`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/published',
          component: ContentPublishedPage,
        },
        // {
        //   isPrivate: false,
        //   exact: true,
        //   subpath: '/draft',
        //   component: ContentDraftPage,
        // },
        // {
        //   isPrivate: false,
        //   exact: true,
        //   subpath: '/add-new-content',
        //   component: ContentAddPage,
        // },
      ],
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/polls-forms',
      component: SettingPage,
    },
    {
      path: '/admin/broadcasting',
      layout: broadcastLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/list`} />,
        },
        {
          path: '/admin/broadcasting/list',
          layout: broadcastManagementLayout,
          routes: [
            {
              exact: true,
              subpath: '/',
              render: ({ match }) => <Redirect to={`${match.url}/broadcastSent`} />,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/broadcastSent',
              component: broadcastSent,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/broadcastError',
              component: broadcastError,
            },
          ],
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/announceTemplate',
          component: broadcastAnnounceTemplate,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/broadcastTemplate',
          component: broadcastTemplate,
        },
      ],
    },
    {
      // promotionManagementLayout
      path: '/admin/promotion',
      layout: promotionLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/list`} />,
        },
        {
          path: '/admin/promotion/list',
          layout: promotionManagementLayout,
          routes: [
            {
              exact: true,
              subpath: '/',
              render: ({ match }) => <Redirect to={`${match.url}/promotionInactive`} />,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/promotionInactive',
              component: promotionInactive,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/promotionActived',
              component: PromotionActived,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/promotionExpired',
              component: promotionExpired,
            },
          ],
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/promotionAdd',
          component: promotionAdd,
        },
      ],
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/chatbox',
      component: ChatBoxPage,
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/dashboard0',
      component: DashboardPage,
    },
    {
      path: '/admin/dashboard',
      layout: DashboardLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/overall`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/overall',
          component: DashboardOverallPage,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/customer',
          component: DashboardCustomerPage,
        },
        // {
        //   isPrivate: false,
        //   exact: true,
        //   subpath: '/maid',
        //   component: DashboardMaidPage,
        // },
      ],
    },
    {
      path: '/admin/database',
      layout: DatabaseLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/maid`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/maid',
          component: DatabaseMaidPage,
        },
      ],
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/profile-setting',
      component: SettingProfilePage,
    },
    {
      isPrivate: false,
      exact: true,
      subpath: '/roles-setting',
      component: SettingRolesPage,
    },

    ///////////////////
    //////
    {
      path: '/admin/group-setting',
      layout: SettingGroupLayout,
      routes: [
        {
          exact: true,
          subpath: '/',
          render: ({ match }) => <Redirect to={`${match.url}/setting-group`} />,
        },
        {
          isPrivate: false,
          exact: true,
          subpath: '/setting-group',
          component: SettingGroupPage,
        },

        {
          path: '/admin/group-setting/setting-group-Of-user',
          layout: SettingGroupOfUser,
          routes: [
            {
              exact: true,
              subpath: '/',
              render: ({ match }) => <Redirect to={`${match.url}/setting-group-customer`} />,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/setting-group-customer',
              component: settingGroupCustomer,
            },
            {
              isPrivate: false,
              exact: true,
              subpath: '/setting-group-maid',
              component: settingGroupMaid,
            },
          ],
        },
      ],
    },
  ],
}

// Guide Line for all route
// guestRoute / unAuthenthicationRoute [ Login, Authentication, Forgot Password]
// adminRoute       [ All feature ]
// adminCustomRoute [ May have config file to custom route from SuperAdmin permission given]

const rootRoutes = [
  {
    exact: true,
    path: '/',
    render: () => <Redirect to="/admin" />,
  },
  adminRoutes,
]

const generateRoutes = (routes, parentProps) => {
  return (
    <Switch>
      {routes.map(({ routes: childRoutes, layout: Layout, subpath, isPrivate, ...props }) => {
        const CrossRoutes = isPrivate ? PrivateRoute : Route
        const path = subpath ? `${parentProps.path}${subpath}` : props.path
        if (childRoutes) {
          return (
            <CrossRoutes
              key={JSON.stringify(routes)}
              {...props}
              render={(layoutProps) => (
                <Layout {...layoutProps}>{generateRoutes(childRoutes, props)}</Layout>
              )}
            />
          )
        }
        //console.log(`process.env.PUBLIC_URL : ${process.env.PUBLIC_URL + path}`)
        // return <CrossRoutes key={JSON.stringify(routes)} {...props} path={process.env.PUBLIC_URL + path} />
        return <CrossRoutes key={JSON.stringify(routes)} {...props} path={path} />
      })}
      <PrivateRoute path="**" component={NotFoundPage} />
    </Switch>
  )
}
// {/* <BrowserRouter basename="/proj-bluuu-admin-frontend"></BrowserRouter> */}

const Routes = () => {
  return <BrowserRouter>{generateRoutes(rootRoutes)}</BrowserRouter>
}

export default Routes
