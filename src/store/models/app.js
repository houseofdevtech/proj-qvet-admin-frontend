export const Title = {
  state: {
    isBack: false,
    pageTitle: '',
    maidList: [],
    customerList:[]
  },
  reducers: {
    setPageTitle(state, payload) {
      return {
        ...state,
        pageTitle: payload,
      }
    },
    setM(state,payload){
      console.log('pM',payload)
      return {
        ...state,
        maidList: payload,
      }
    },
    setC(state,payload){
      console.log('pM',payload)
      return {
        ...state,
        customerList: payload,
      }
    },
    showBackIcon(state, payload) {
      return {
        ...state,
        isBack: payload,
      }
    },
  },
}