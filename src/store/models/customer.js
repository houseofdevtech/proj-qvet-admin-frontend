import request from '../../utils/request'
import moment from 'moment'
export const customer = {
  state: {
    customers: [],
    selectedCustomer: null,
    column: [
      { label: 'ลำดับ', accessor: 'member', isShow: true },
      { label: 'ตำแหน่งงาน', accessor: 'position', isShow: true },
      { label: 'ชื่อพนักงาน', accessor: 'name', isShow: true },
      { label: 'อีเมล', accessor: 'email', isShow: true },
      { label: 'เบอร์โทร', accessor: 'phone', isShow: true },
      { label: 'Level All', accessor: 'level', isShow: true },
      { label: 'Id Line', accessor: 'idLine', isShow: true },
      {label: 'สถานะการตรวจสอบประวัติและข้อมูลส่วนตัว', accessor: 'approve_status', isShow: true},

      { label: 'วัน และ เวลา ที่สร้างข้อมูล', accessor: 'createdAt', isShow: true },
      { label: 'วัน และ เวลาที่อัพเดตข้อมูลล่าสุด', accessor: 'updatedAt', isShow: true },
      { label: 'สถานะ', accessor: 'updatedAt', isShow: true },
      // {
      //   label: 'สถานะการ ยืนยันอีเมล',
      //   accessor: 'Email Verify',
      //   isShow: true,
      // },
      // {
      //   label: '  สถานะการตรวจสอบ ประวัติและข้อมูลส่วนตัว',
      //   accessor: 'Personal Verify',
      //   isShow: true,
      // },
      // {
      //   label: 'ดูรายละเอียด',
      //   accessor: 'Detail',
      //   isShow: true,
      // },
      // {
      //   label: 'ลบผู้ใช้งาน',
      //   accessor: 'Delete User',
      //   isShow: true,
      // },

      // { label: 'Joining Date', accessor: 'joinDate', isShow: false },
      // { label: 'Role', accessor: 'role', isShow: true },
    ],
    column1: [
      { label: 'Name', accessor: 'name', isShow: true },
      { label: 'Id', accessor: 'id', isShow: true },
      { label: 'Code', accessor: 'code', isShow: true },
      { label: 'Create BY', accessor: 'createBy', isShow: true },
      { label: 'Start Date', accessor: 'startDate', isShow: true },
      { label: 'End Date', accessor: 'endDate', isShow: true },
    ],

    customerList: 0,
  },
  reducers: {
    setCustomerList(state, payload) {
      console.log('payloadxx',payload)
      return { ...state, customers: payload }
    },
    setCustomerTotalList(state, payload) {
      return { ...state, customerList: payload }
    },
    setSelectedCustomer(state, payload) {
      return { ...state, selectedCustomer: payload }
    },
    setCustomer(state, payload) {
      return {
        ...state,
        customers: payload,
      }
    },
    setCustomerColumnList(state, payload) {
      // console.log(`payload: ${JSON.stringify(payload)}`)
      return { ...state, column: payload }
    },
    toggleShowCustomerColumn(state, payload) {
      console.log(`payload: ${payload}`)
      // console.log(`target: ${JSON.stringify(state.column[6].accessor)}`)
      var newColumn = state.column
      const result = state.column.map((obj, index) => {
        if (obj.accessor === payload) {
          // console.log(`match: ${obj.accessor} === ${payload} `)
          const editObj = { ...obj, isShow: !obj.isShow }
          // console.log(`editObj: ${JSON.stringify(editObj)}`)
          newColumn[index] = editObj
          return newColumn
        }
        return newColumn
        // return {...state}
      })
      console.log(`result: ${JSON.stringify(result[0])}`)
      // return result from map method
      // which is array with 1 member
      return { ...state }
      // return result[0]
    },
  },
  // effects: (dispatch) => ({
  //   async getCustomerList() {
  //     const res = await request.get('/api/alluser')
  //     console.log(res)

  //     const cleanData = res.data.map((customer) => {
  //       return {
  //         id: customer.id,
  //         member: customer.member,
  //         title: customer.title,
  //         name: customer.name,
  //         email: customer.email,
  //         phone: customer.phone,
  //         position: customer.position,
  //         idLine: customer.idLine,
  //         createdAt: customer.createdAt,
  //         updatedAt: customer.updatedAt,
  //       }
  //     })

  //     // const Data = res.map((customer) => {
  //     //    return {
  //     //      id: customer.id,
  //     //      member: customer.member,
  //     //      title: customer.title,
  //     //      name: customer.name,
  //     //      email: customer.email,
  //     //      phone: customer.phone,
  //     //      position: customer.position,
  //     //      idLine: customer.idLine,
  //     //    }
  //     //  })
  //     const totalData = res.data.item_count
  //     console.log(cleanData)
  //     dispatch.customer.setCustomerList(cleanData)
  //     dispatch.customer.setCustomerTotalList(totalData)
  //   },
  // }),
}
