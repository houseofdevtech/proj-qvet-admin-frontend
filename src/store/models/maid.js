import request from '../../utils/request'

export const maid = {
  state: {
    maidsList: [],
    column: [
      { label: 'Picture', value: 1, accessor: 'picture', isShow: true },
      { label: 'Maid ID', value: 1, accessor: 'member', isShow: true },
      { label: 'Name', value: 2, accessor: 'position', isShow: true },
      { label: 'Maid Tier', value: 1, accessor: 'company_name', isShow: true },
      { label: 'Phone', value: 4, accessor: 'phone', isShow: true },
      { label: 'Email', value: 3, accessor: 'email', isShow: true },
      { label: 'Average Ratings', value: 1, accessor: 'Average_Ratings', isShow: true },
      { label: '# of ratings', value: 1, accessor: 'updatedAt', isShow: true },
      { label: 'Repeat Rate', value: 1, accessor: 'Repeat_Rate', isShow: true },
      { label: 'status', value: 1, accessor: 'approve_status', isShow: true },

      { label: 'Start Date', value: 1, accessor: 'Start_Date', isShow: true },
      { label: 'District of Pin', value: 1, accessor: 'District_of_Pin', isShow: true },
      { label: 'Interviewer', value: 1, accessor: 'Interviewer', isShow: true },
      { label: 'Trainer', value: 1, accessor: 'Trainer', isShow: true },
      { label: 'Status', value: 5, accessor: 'status', isShow: true },
      { label: 'Role', value: 6, accessor: 'role', isShow: true },
      { label: 'Joining Date', value: 7, accessor: 'joinDate', isShow: false },
    ],
    columnMaidProfile: [
      { label: 'Cumulative', accessor: 'Cumulative', isShow: true },
      { label: 'Mounth', accessor: 'Mounth', isShow: true },
      { label: 'Work Completed', accessor: 'Work_completed', isShow: true },
      { label: 'Hrs Completed', accessor: 'Hrs_completed', isShow: true },
      { label: 'Salary(THB)', accessor: 'salary', isShow: true },
      { label: '5 Star Bonus', accessor: 'star_bonus', isShow: true },
      { label: 'Monthly Bonus', accessor: 'Monthly_bonus', isShow: true },
      { label: 'Milestone Bonus', accessor: 'Milestone_bonus', isShow: true },
    ],
  },
  reducers: {
    setMaidList(state, payload) {
      console.log('payload11', payload)
      return { ...state, maidsList: payload }
    },
    setMaidColumnList(state, payload) {
      // console.log(`payload: ${JSON.stringify(payload)}`)
      return { ...state, column: payload }
    },
    toggleShowMaidColumn(state, payload) {
      // console.log(`payload: ${payload}`)
      // console.log(`target: ${JSON.stringify(this.state)}`)
      return { ...state, column: { ...state.column } }
    },
  },
  // effects: (dispatch) => ({
  //   async getMaidList() {
  //     const res = await request.get('/maid/list')
  //     const cleanData = res.data.item_count
  //     dispatch.maid.setMaidList(cleanData)
  //   },
  // }),
}
