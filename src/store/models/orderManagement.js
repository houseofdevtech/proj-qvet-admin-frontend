import request from '../../utils/request'

export const orderManagement = {
  state: {
    dataOrderApprove: 0,
    dataOrderPayment: 0,
    dataOrderBooking: 0,
    start_dateOrder: '',
    money: {},
    EmMoney: {},
    moneyTotalVat: 0,
    end_dateOrder: '',
    columnOrderManagementApprove: [
      { label: 'ลำดับ', accessor: 'Order_ID', isShow: true },
      // { label: 'Payment date', accessor: 'Payment_date', isShow: true },
      // { label: 'Payment Method', accessor: 'name', isShow: true },
      // { label: 'User ID', accessor: 'position', isShow: true },
      // { label: 'User Name', accessor: 'level', isShow: true },
      // { label: 'Order Type', accessor: 'jobCode', isShow: true },
      { label: 'ชื่อผู้ประกอบการ', accessor: 'EmployerName', isShow: true },
      // { label: 'Status', accessor: 'JobType', isShow: true },
      { label: 'วันที่ปฏิบัติงาน', accessor: 'WorkTime', isShow: true },
      { label: 'จำนวนวัน', accessor: 'amountDay', isShow: true },
      { label: 'จำนวนผู้ปฏิบัติงาน', accessor: 'empAmount', isShow: true },
      { label: 'จำนวนเงิน', accessor: 'moneyPerson', isShow: true },
      // { label: 'Maid Name', accessor: 'score', isShow: true },
      { label: 'ภาษีมูลค่าเพิ่ม 7%', accessor: 'moneyVat', isShow: true },
      { label: 'จำนวนเงินรวมภาษีมูลค่าเพิ่ม 7%', accessor: 'moneyVatTotal', isShow: true },
      // { label: 'Maid Phone', accessor: 'listConfirm', isShow: true },
      // { label: 'Booked Date', accessor: 'paymentStatus', isShow: true },
      { label: 'ดูรายละเอียด', accessor: 'Details', isShow: true },
    ],
    columnOrderManagementPayment: [
      { label: 'Picture', accessor: 'picture', isShow: true },
      { label: 'Name of maid', accessor: 'Name_maid', isShow: true },
      { label: 'MP Hours', accessor: 'MP_Hours', isShow: true },
      { label: 'OT Hours', accessor: 'OT_Hours', isShow: true },
      { label: 'Salary', accessor: 'Salary', isShow: true },
      { label: 'Payment date', accessor: 'Payment_date', isShow: true },
      { label: 'Status', accessor: 'Status', isShow: true },
    ],
    columnOrderManagementBooking: [
      { label: 'ลำดับ', accessor: 'Order_ID', isShow: true },
      // { label: 'Payment date', accessor: 'Payment_date', isShow: true },
      { label: 'ชื่อพนักงาน', accessor: 'name', isShow: true },
      { label: 'ตำแหน่ง', accessor: 'position', isShow: true },
      { label: 'Level', accessor: 'level', isShow: true },
      { label: 'อัตราจ้าง', accessor: 'pricing_person', isShow: true },
      { label: 'ชื่อผู้ประกอบการ', accessor: 'EmployerName', isShow: true },
      { label: 'ประเภทงาน', accessor: 'JobType', isShow: true },
      { label: 'วันที่ปฏิบัติงาน', accessor: 'WorkTime', isShow: true },
      { label: 'จำนวนวัน', accessor: 'amountDay', isShow: true },
      { label: 'จำนวนเงิน', accessor: 'moneyPerson', isShow: true },
      { label: 'คะแนนประเมิน', accessor: 'score', isShow: true },
      { label: 'หมายเหตุ', accessor: 'note', isShow: true },

      { label: 'สถานะรายการ', accessor: 'listConfirm', isShow: true },
    ],
    columnBookingProfile: [
      { label: 'Maid Change', accessor: 'Maid_change', isShow: true },
      { label: 'Maid ID', accessor: 'Maid_ID', isShow: true },
      { label: 'Maid Name', accessor: 'Maid_Name', isShow: true },
      { label: 'Maid Phone', accessor: 'Maid_Phone', isShow: true },
      { label: 'Credit', accessor: 'Credit', isShow: true },
      { label: 'Book Date', accessor: 'Book_Date', isShow: true },
      { label: 'Start Time', accessor: 'Start_Time', isShow: true },
      { label: 'Credit Status', accessor: 'Credit_Status', isShow: true },
      { label: 'Purchase Date', accessor: 'Purchase_Date', isShow: true },
      { label: 'Expiration Date', accessor: 'Expiration_Date', isShow: true },
    ],
    columnOrderManagementNamelist: [
      { label: 'Date of service', accessor: 'Date_service', isShow: true },
      { label: 'Service (Hrs)', accessor: 'ServiceHrs', isShow: true },
      { label: 'Type of service', accessor: 'Type_service', isShow: true },
      { label: 'Payment', accessor: 'Payment', isShow: true },
      { label: 'Rating', accessor: 'Rating', isShow: true },
    ],
    showBtnAdd: true,
    orderMode: 'Approve',
    updateSatatusApprove: '',
    weekDatePayment: {},
    employeeData: {},
    employerData: {},
  },
  reducers: {
    satatusApprove(state, payload) {
      return {
        ...state,
        updateSatatusApprove: payload,
      }
    },
    setEmployee(state, payload) {
      return {
        ...state,
        employeeData: payload,
      }
    },
    setEmployer(state, payload) {
      return {
        ...state,
        employerData: payload,
      }
    },
    setEmployerTotal(state, payload) {
      return {
        ...state,
        EmMoney: payload,
      }
    },
    setCompanyMoney(state, payload) {
      return {
        ...state,
        money: payload,
      }
    },
    setCompanyMoneyVatTotal(state, payload) {
      return {
        ...state,
        moneyTotalVat: payload,
      }
    },
    setStartdateOrder(state, payload) {
      return {
        ...state,
        start_dateOrder: payload,
      }
    },
    setEndateOrder(state, payload) {
      return {
        ...state,
        end_dateOrder: payload,
      }
    },
    setOrderApproveColumnList(state, payload) {
      return { ...state, columnOrderManagementApprove: payload }
    },
    setOrderPaymenColumnList(state, payload) {
      return { ...state, columnOrderManagementPayment: payload }
    },
    setOrderBookingColumnList(state, payload) {
      return { ...state, columnOrderManagementBooking: payload }
    },
    setOrderManagementNameList(state, payload) {
      return { ...state, columnOrderManagementNamelist: payload }
    },
    getOrderApproveList(state, payload) {
      return {
        ...state,
        dataOrderApprove: payload,
      }
    },
    getorDerPayment(state, payload) {
      return {
        ...state,
        dataOrderPayment: payload,
      }
    },
    getorDerBooking(state, payload) {
      return {
        ...state,
        dataOrderBooking: payload,
      }
    },
    setCheckTypePath(state, payload) {
      return {
        ...state,
        showBtnAdd: payload,
      }
    },
    setOrderMode(state, payload) {
      return {
        ...state,
        orderMode: payload,
      }
    },
  },
}
