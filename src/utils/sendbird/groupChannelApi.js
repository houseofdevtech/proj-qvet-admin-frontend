import sendbird from './sendbird'

export default {
    getChannelList (params) {
        return sendbird.get(`/group_channels`, { params }).then((resp) => {
            return resp.data
        })
    },
    viewChannel (channel_url, params) {
        return sendbird.get(`/group_channels/${channel_url}`, { params }).then((resp) => {
            return resp.data
        })
    },
    createChannel (data) {
        return sendbird.post(`/group_channels`, data).then((resp) => {
            return resp.data
        })
    },
    updateChannel (channel_url, data) {
        return sendbird.put(`/group_channels/${channel_url}`, data).then((resp) => {
            return resp.data
        })
    }
}