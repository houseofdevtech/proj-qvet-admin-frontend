import sendbird from './sendbird'

export default {
    messageList (channel_url, params) {
        return sendbird.get(`group_channels/${channel_url}/messages`, { params }).then((resp) => {
            return resp.data
        })
    },
    sendMessage (channel_url, data) {
        return sendbird.post(`group_channels/${channel_url}/messages`, data).then((resp) => {
            return resp.data
        })
    },
    viewUnreadMessage (user_id, params) {
        return sendbird.get(`/users/${user_id}/unread_message_count`, params).then((resp) => {
            return resp.data
        })
        // VIEW UNREAD users_id[]
        // return sendbird.get(`/group_channels/${channel_url}/messages/unread_count`, { params }).then((resp) => {
        //     return resp.data
        // })
    }
}