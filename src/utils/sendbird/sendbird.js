import axios from 'axios'

const APP_ID = 'AC287AD4-E4DC-4CD2-B9F5-9E5AB205C294'
const TOKEN = 'e4224011e0a78d002c63e296ae13dfd3c7e222f1'

const sendbird = axios.create({
    baseURL: `https://api-${APP_ID}.sendbird.com/v3/`,
    headers: { 
        'content-type': 'application/json, charset=utf8',
        'Api-Token': TOKEN
    }
})

export default sendbird