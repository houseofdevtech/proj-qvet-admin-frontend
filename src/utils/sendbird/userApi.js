import sendbird from './sendbird'

export default {
    getUerList (params) {
        return sendbird.get(`/users`, { params }).then((resp) => {
            return resp.data
        })
    }
}
