export const ip = process.env.REACT_APP_URL
export const post = (object, path, token) =>
  new Promise((resolve, reject) => {
    fetch(ip + path, {
      method: 'POST',
      headers: {
        Authorization: 'Basic Ymx1dXU6dXV1Ymw=',
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(object),
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.json()
      })
      .then((json) => {
        resolve(json)
      })
      .catch((err) => reject(err))
  })
export const get = async (path, token) =>
  await fetch(ip + path, {
    method: 'GET',
    headers: {
      Authorization: 'Basic Ymx1dXU6dXV1Ymw=',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .catch((err) => console.log(err))
export const get_other = (path, token) =>
  new Promise((resolve, reject) => {
    fetch(path, {
      method: 'GET',
      mode: 'no-cors',
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.blob()
      })
      .then((blob) => {
        console.log(blob)
        resolve(blob)
      })
      .catch((err) => reject(err))
  })



export const put = ( path,object, token) =>
  new Promise((resolve, reject) => {
    fetch(ip + path, {
      method: 'PUT',
      headers: {
        Authorization: 'Basic Ymx1dXU6dXV1Ymw=',
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(object)
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.json()
      })
      .then((json) => {
        resolve(json)
      })
      .catch((err) => reject(err))
  })
