import React, { Component } from 'react'
import styled from 'styled-components'
import ErrorBoundary from '../errorBoundary'

const Container = styled.div`
  display: flex;
  // justify-content: center;
  // align-items: center;
  height: 100%;
`

class NotFoundPage extends Component {
  static propTypes = {}

  render() {
    return (
      <ErrorBoundary>
        <Container>
          <h1>404 Not Found</h1>
        </Container>
      </ErrorBoundary>
    )
  }
}

export default NotFoundPage
